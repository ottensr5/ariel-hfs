#include <cppunit/config/SourcePrefix.h>
#include "../src/fcu_algorithms.h"
#include "FCUTest.hpp"
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <experimental/filesystem>
#include <math.h>

CPPUNIT_TEST_SUITE_REGISTRATION(FCUTest);

void FCUTest::setUp()
{
    unsigned int i;
    const char noStarName[128] = "./test/noStar.txt";
    const char acqImgName[128] = "./test/acquisitionTest.txt";
    const char starImgName[128] = "./test/trackingTest.txt";
    sum_star = 0;
    sum_acq = 0;
    std::ifstream noStarIf, starImgIf, acqImgIf;

    acqImgIf.open(acqImgName, std::ifstream::in);
    if (acqImgIf.is_open())
    {
        for (i = 0; i < 200*200; i++)
        {
            acqImgIf >> acqImg[i];
            sum_acq = sum_acq + acqImg[i];
        }
        acqImgIf.close();
    }


    for(i = 0; i < 64*64; i++){
        noSignal[i] = 0.;
    }

    noStarIf.open(noStarName, std::ifstream::in);

    if (noStarIf.is_open())
    {
        for (i = 0; i < 64*64; i++)
        {
            noStarIf >> noStar[i];
        }
        noStarIf.close();
    }
    
    starImgIf.open(starImgName, std::ifstream::in);
    if (starImgIf.is_open())
    {
        for (i = 0; i < 64*64; i++)
        {
            starImgIf >> starImg[i];
            sum_star = sum_star + starImg[i];
        }
        starImgIf.close();
    }

}

void FCUTest::tearDown()
{
}

void FCUTest::testMinMean()
{
    unsigned int testSet[10] = {3, 6, 0, 4, 9, 7, 5, 2, 8, 1};
    unsigned int testSetMean[3] = {3, 6, 0};
    unsigned int res;
    int resMean;
    float mean, sig;

    res = GetMin(testSet, 10);

    CPPUNIT_ASSERT(res == 0);

    resMean = MeanSigma(testSetMean, 0, &mean, &sig);
    CPPUNIT_ASSERT(resMean == -1);

    resMean = 0;
    resMean = MeanSigma(testSetMean, 1, &mean, &sig);
    CPPUNIT_ASSERT(resMean == -1);

}

void FCUTest::testMedianFilter()
{
    unsigned int input[64*64];
    unsigned int output[64*64];
    unsigned int i;
    int flag;

    flag = 0;

    for(i = 0; i < 64*64; i++){
        input[i] = noStar[i];
    }
    std::cout << "Check dimension checks of median filter.\n";
    input[120] = 100000.0;
    flag = MedFilter3x3(input, 2, 64, 200, output);
    CPPUNIT_ASSERT(flag == -1);

    flag = 0;
    flag = MedFilter3x3(input, 64, 2, 200, output);
    CPPUNIT_ASSERT(flag == -1);
    std::cout << "Check function of median filter.\n";
    flag = 0;
    flag = MedFilter3x3(input, 64, 64, 200, output);
    CPPUNIT_ASSERT(flag == 0);

    CPPUNIT_ASSERT(output[120] < 500);

}

void FCUTest::testValidation()
{
    struct valpack package;
        
         
    std::cout << "Check Dimension check.\n";
    package = CheckRoiForStar(noSignal, 0, 64, 100, 0.6, 1, 32., 32., 20, sum_star, 0);
    std::cout << package.flag << "\n";
    CPPUNIT_ASSERT(package.flag == 111);

    package.flag = 0;

    package = CheckRoiForStar(noSignal, 64, 0, 100, 0.6, 1, 32., 32., 20, sum_star, 0);
    CPPUNIT_ASSERT(package.flag == 111);
    std::cout << package.flag << "\n";
    
    package.flag = 0;

    package = CheckRoiForStar(noSignal, 3, 3, 100, 0.6, 1, 32., 32., 20, sum_star, 0);
    CPPUNIT_ASSERT(package.flag == 111);
    std::cout << package.flag << "\n";

    package.flag = 0;
    std::cout << "Check constant image check.\n";
    
    package = CheckRoiForStar(noSignal, 64, 64, 100, 0.6, 1, 32., 32., 20, sum_star, 0);
    std::cout << package.flag << "\n";
    CPPUNIT_ASSERT(package.flag == 101);

    package.flag = 0;
    std::cout << "Check no star check.\n";
    
    package = CheckRoiForStar(noStar, 64, 64, 100, 0.6, 1, 32., 32., 20, sum_star, 0);
    std::cout << package.flag << "\n";
    CPPUNIT_ASSERT(package.flag == 102);
    
    package.flag = 0;
    std::cout << "Check too low signal check.\n";
    
    package = CheckRoiForStar(starImg, 64, 64, 10000, 0.6, 1, 32., 32., 20, sum_star, 0);
    std::cout << package.flag << "\n";
    CPPUNIT_ASSERT(package.flag == 104);
    
    package.flag = 0;
    std::cout << "Check too low pearson check.\n";

    std_threshold(starImg, 64, 64);
    package = CheckRoiForStar(starImg, 64, 64, 100, 1.0, 1, 32., 32., 50, 18000, 0);
    std::cout << package.flag << "\n";
    CPPUNIT_ASSERT(package.flag == 106);

    package.flag = 0;
    std::cout << "Check valid output FGS1.\n";
    
    package.index = 2.0;
    package = CheckRoiForStar(starImg, 64, 64, 100, 0.0, 1, 32., 32., 50, 18000, 0);
    std::cout << package.index << "\n";
    CPPUNIT_ASSERT(package.index < 1.0);
    
    package.index = 2.0;
    std::cout << "Check valid output FGS2.\n";
    
    package = CheckRoiForStar(starImg, 64, 64, 100, 0.0, 2, 32., 32., 50, 18000, 0);
    std::cout << package.index << "\n";
    CPPUNIT_ASSERT(package.index < 1.0);
}

void FCUTest::testAcquisition()
{
    struct coord output;
    
    std::cout << "Check valid output.\n";
    std_threshold(acqImg, 200, 200);
    output = SourceDetection(acqImg, 200, 200, 1, 4, 22000, 50, 20, 3, 5., 5., 100, 0.3);
    CPPUNIT_ASSERT(output.x < 100.0);
    CPPUNIT_ASSERT(output.x > 99.0);
    CPPUNIT_ASSERT(output.y < 100.0);
    CPPUNIT_ASSERT(output.y > 99.0);
    std::cout << output.validity.index << "\n";
    CPPUNIT_ASSERT(output.validity.flag == 1);
    CPPUNIT_ASSERT(output.validity.index < 1.0);

    std::cout << "Check no valid target.\n";

    output = SourceDetection(acqImg, 200, 200, 1, 4, 200000, 50, 20, 3, 5., 5., 100, 0.3);
    std::cout << output.validity.flag << "\n";
    CPPUNIT_ASSERT(output.validity.flag == 107); 
    
    std::cout << "Check dimension check.\n";

    output = SourceDetection(acqImg, 2, 2, 1, 4, sum_acq, 50, 20, 3, 5., 5., 100, 0.3);
    std::cout << output.validity.flag << "\n";

    CPPUNIT_ASSERT(output.validity.flag == 111); 
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output.x, 0.0, 1e-7);    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output.y, 0.0, 1e-7);
}

void FCUTest::testCentroid()
{
    struct coord output;

    std::cout << "Check valid output.\n";
    std_threshold(starImg, 64, 64);
    output = ArielCoG(starImg, 64, 64, 5, 5., 5., 1, 100, 0.3, 50, 18000, 0);
    CPPUNIT_ASSERT(output.x < 32.0);
    CPPUNIT_ASSERT(output.x > 31.0);
    CPPUNIT_ASSERT(output.y < 32.0);
    CPPUNIT_ASSERT(output.y > 31.0);
    CPPUNIT_ASSERT(output.validity.flag == 1);
    CPPUNIT_ASSERT(output.validity.index < 1.0);

    std::cout << "Check invalid output.\n";

    output = ArielCoG(noStar, 64, 64, 5, 5., 5., 1, 100, 0.3, 50, sum_star, 0);
    std::cout << output.validity.flag << "\n";
    CPPUNIT_ASSERT(output.validity.flag == 102); 

}

void FCUTest::testThreshold()
{
    unsigned int i;
    unsigned int sum, mean, std, sumSquare, min, max, maxOg;

    sum = 0;
    sumSquare = 0;
    maxOg = 0;
    for(i = 0; i < 64*64; i++){
        sum = sum + starImg[i];
        sumSquare = sumSquare + (starImg[i] * starImg[i]);
        if(starImg[i] > maxOg)
            maxOg = starImg[i];
    }
    mean = (unsigned int) (sum / (64 * 64));
    std = (unsigned int) sqrt((sumSquare / (64 * 64)) - (mean*mean));

    std_threshold(starImg, 64, 64);

    min = starImg[0];
    max = starImg[0];

    for(i = 0; i < 64*64; i++){
        if(starImg[i] < min)
            min = starImg[i];
        if(starImg[i] > max)
            max = starImg[i];
    }

    CPPUNIT_ASSERT(min == 0);
    CPPUNIT_ASSERT(max == (maxOg - mean - std));

}

