#ifndef DETECTORFEATURESTEST_H
#define DETECTORFEATURESTEST_H
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

class DetectorFeaturesTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(DetectorFeaturesTest);
  CPPUNIT_TEST(testBias);
  CPPUNIT_TEST(testDark);
  CPPUNIT_TEST(testHotPixels);
  CPPUNIT_TEST(testFlat);
  CPPUNIT_TEST(testStar);
  CPPUNIT_TEST(testConvolve);
  CPPUNIT_TEST(testBkgr);
  CPPUNIT_TEST(testSmear);
  CPPUNIT_TEST_SUITE_END();

  double bias[200*200];
  double rnSample[200];
  double dark[200*200];
  double hp[200*200];
  double flat[200*200];
  double mask[200*200];
  double psf[40*40];
  double image[1000*1000];
  double bkgr[200*200];
  double kernel[125*125];


public:
  
  void setUp();
  void tearDown();

  void testBias();

  void testDark();

  void testHotPixels();

  void testFlat();

  void testStar();

  void testConvolve();

  void testBkgr();

  void testSmear();
};

#endif //DETECTORFEATURESTEST_H
