#include <cppunit/config/SourcePrefix.h>
#include "../src/detector_features.h"
#include "DetectorFeatureTest.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <experimental/filesystem>
#include <math.h>

CPPUNIT_TEST_SUITE_REGISTRATION(DetectorFeaturesTest);

void DetectorFeaturesTest::setUp()
{
    unsigned int i;

    for(i = 0; i < 200*200; i++){
        bias[i] = 0.;
        dark[i] = 0.;
        hp[i] = 0.;
        flat[i] = 0.;
        mask[i] = 0.;
        psf[i] = 0.;
        bkgr[i] = 0.;
    }

    
    for(i = 0; i < 125*125; i++){
        kernel[i] = 0.;
    }
}

void DetectorFeaturesTest::tearDown()
{
}

void DetectorFeaturesTest::testBias()
{
    unsigned int i;

    for(i = 0; i < 200; i++){
        rnSample[i] = 0.0;
    }

    generate_bias(bias, rnSample, 123., 200, 200, 200);
    std::cout << "Check if all pixels have the same bias values.\n";
    for(i = 0; i < 200*200; i++){
        CPPUNIT_ASSERT_DOUBLES_EQUAL(bias[i], 123., 0.001);  
    }
}

void DetectorFeaturesTest::testDark()
{
    unsigned int i;
    double mean, std;

    

    mean = 0.;
    std = 0.;
    generate_dark(dark, 10., 1., 200, 200);
    std::cout << "Check if dark values are not 0\n";
    for(i = 0; i < 200*200; i++)
    {
        CPPUNIT_ASSERT(dark[i] > 0.0);
        mean = mean + dark[i];
    }

    mean = mean / (200*200);
    for(i = 0; i < 200*200; i++)
    {
        std = std + (dark[i] - mean) * (dark[i] - mean); 
    }
    std = sqrt(std/(200*200));

    std::cout << "Check dark mean = 0\n";

    generate_dark(dark, 0, 1, 200, 200);

    mean = 0;
    for(i = 0; i < 200*200; i++)
    {
        CPPUNIT_ASSERT_DOUBLES_EQUAL(dark[i], 0.0, 1e-7);
    }
}

void DetectorFeaturesTest::testHotPixels()
{
    unsigned int i;
    double mean;
    mean = 0;

    for(i = 0; i < 200*200; i++){
        hp[i] = 0.0;
    }
    
    generate_hotpixels(hp, 0.5, 100, 10000, 200, 200);
    std::cout << "Check if the hot pixel values are not 0\n";
    for(i = 0; i < 200*200; i++){
       CPPUNIT_ASSERT(hp[i] > 0.0);
       mean = mean + hp[i];
    }

    mean = mean / (200*200);
    std::cout << "Check if hot pixels where generated.\n";
    CPPUNIT_ASSERT(mean > 1.0);
}

void DetectorFeaturesTest::testFlat()
{
    double flat[200*200];
    unsigned int i;

    for(i = 0; i < 200*200; i++){
        flat[i] = 0.0;
    }

    generate_flat(flat, 0.9, 0.08, 200, 200);
    std::cout << "Check if flat values are below 1.\n";

    for(i = 0; i < 200*200; i++){
        CPPUNIT_ASSERT(flat[i] <= 1.0);
    }
    generate_flat(flat, 0.05, 0.08, 200, 200);
    std::cout << "Check if flat values are not smaller than 0.\n";

    for(i = 0; i < 200*200; i++){
        CPPUNIT_ASSERT(flat[i] >= 0.0);
    }
}

void DetectorFeaturesTest::testStar()
{
    struct stars catalogue;
    unsigned int i;
    double sum;
    sum = 0;

    for(i = 0; i < 2500; i++){
        catalogue.signalFGS1[i] = 0.;
        catalogue.signalFGS2[i] = 0.;
        catalogue.x[i] = 0.;
        catalogue.y[i] = 0.;
        catalogue.visible[i] = 0;
    }
    catalogue.number = 4;
    
    catalogue.signalFGS1[0] = 500;
    catalogue.signalFGS2[0] = 1000;

    catalogue.signalFGS1[1] = 2000;
    catalogue.signalFGS2[1] = 4000;

    catalogue.signalFGS1[2] = 6000;
    catalogue.signalFGS2[2] = 8000;

    catalogue.signalFGS1[3] = 10000;
    catalogue.signalFGS2[3] = 12000;

    catalogue.visible[0] = 1;
    catalogue.visible[1] = 1;
    catalogue.visible[2] = 1;
    catalogue.visible[3] = 1;

    catalogue.x[0] = -10.0;
    catalogue.y[0] = 30.0;

    catalogue.x[1] = 100.5;
    catalogue.y[1] = 100.0;

    catalogue.x[2] = 30.0;
    catalogue.y[2] = 60.0;

    catalogue.x[3] = 300.0;
    catalogue.y[3] = 10.0;

    for(i = 0; i < 200*200; i++){
        mask[i] = 0.;
    }

    generate_starmask(mask, catalogue, 1.0, 200, 200, 1);

    for(i = 0; i < 200*200; i++){
        sum = sum + mask[i];
    }
    std::cout << "Check for correct mask generation with default config.\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(sum, 8000., 0.0001);

    catalogue.visible[2] = 0;
    
    for(i = 0; i < 200*200; i++){
        mask[i] = 0.;
    }

    generate_starmask(mask, catalogue, 1.0, 200, 200, 1);
    sum = 0.;
    for(i = 0; i < 200*200; i++){
        sum = sum + mask[i];
    }
    std::cout << "Check for correct use of visible flag.\n";
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(sum, 2000., 0.0001);
    
    for(i = 0; i < 200*200; i++){
        mask[i] = 0.;
    }

    generate_starmask(mask, catalogue, 1.0, 200, 200, 2);
    sum = 0.;
    for(i = 0; i < 200*200; i++){
        sum = sum + mask[i];
    }
    std::cout << "Check for correct use of channel config.\n";
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(sum, 4000., 0.0001);

}

void DetectorFeaturesTest::testConvolve()
{ 
    struct stars catalogue;
    const char psfName[128] = "./FGS1_270nm.txt";
    unsigned int i;
    double sumMask, sumImage;
    std::ifstream psfIf;

    sumMask = 0.;
    sumImage = 0.;
    
    psfIf.open(psfName, std::ifstream::in);
    if (psfIf.is_open())
    {
        for (i = 0; i < 40*40; i++)
        {
            psfIf >> psf[i];
        }
        psfIf.close();
    }

    
    catalogue.number = 1;
    
    catalogue.signalFGS1[0] = 2000;
    catalogue.signalFGS2[0] = 2000;

    
    catalogue.visible[0] = 1;
    
    catalogue.x[0] = 100.0;
    catalogue.y[0] = 100.0;
    
    for(i = 0; i < 200*200; i++){
        mask[i] = 0.;
    }
    
    
    
    generate_starmask(mask, catalogue, 1.0, 200, 200, 1);
    
    for(i = 0; i < 200*200; i++){
        sumMask = sumMask + mask[i];
    }
    
    generate_star_image(image, psf, mask, 200, 200, 40, 40);

    for(i = 0; i < 200*200; i++){
        sumImage = sumImage + image[i];
    }

    std::cout << "Check if signal is conserved in convolution.\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(sumMask, sumImage, 0.0001);
}

void DetectorFeaturesTest::testBkgr()
{
    unsigned int i;

    for(i = 0; i < 200*200; i++){
        bkgr[i] = 0.0;
    }

    generate_background(bkgr, 10, 1.0, 1.0, 200, 200);
    std::cout << "Check if background is not 0.\n";
    
    for(i = 0; i < 200*200; i++){
        CPPUNIT_ASSERT(bkgr[i] > 0.);
    }

}

void DetectorFeaturesTest::testSmear()
{
    unsigned int i;
    int warning;

    for(i = 0; i < 125*125; i++){
        kernel[i] = 0.;
    }

    generate_linear_smearing_kernel(kernel, 0, 0, 10, 10, 125);
    std::cout << "Check endpoints of smear in positive smear direction.\n";
    CPPUNIT_ASSERT(kernel[62 + 125*62] > 0.0);
    CPPUNIT_ASSERT(kernel[72 + 125*72] > 0.0);

    for(i = 0; i < 125*125; i++){
        kernel[i] = 0.;
    }

    generate_linear_smearing_kernel(kernel, 0, 0, -10, -10, 125);
    std::cout << "Check endpoints of smear in negative smear direction.\n";
    CPPUNIT_ASSERT(kernel[62 + 125*62] > 0.0);
    CPPUNIT_ASSERT(kernel[52 + 125*52] > 0.0);
    
    for(i = 0; i < 125*125; i++){
        kernel[i] = 0.;
    }

    warning = generate_linear_smearing_kernel(kernel, -70, -70, 130, 130, 125);
    std::cout << "Check limit failsave.\n";
    CPPUNIT_ASSERT(kernel[0] > 0.0);
    CPPUNIT_ASSERT(kernel[122 + 122*125] > 0.0);
    CPPUNIT_ASSERT(warning == 1);
    
    for(i = 0; i < 125*125; i++){
        kernel[i] = 0.;
    }

    warning = generate_linear_smearing_kernel(kernel, 0, 0, 0, 0, 125);
    std::cout << "Check 0 lenght smear.\n";
    CPPUNIT_ASSERT(warning == 0);
    CPPUNIT_ASSERT(kernel[62 + 62*125] > 0.0);

    for(i = 0; i < 125*125; i++){
        kernel[i] = 0.;
    }

    generate_linear_smearing_kernel(kernel, 0, 0, 10, 0, 125);
    std::cout << "Check linear x smear.\n";
    CPPUNIT_ASSERT(kernel[62 + 125*62] > 0.0);
    CPPUNIT_ASSERT(kernel[70 + 125*62] > 0.0);
    
    for(i = 0; i < 125*125; i++){
        kernel[i] = 0.;
    }

    generate_linear_smearing_kernel(kernel, 0, 0, 0, 10, 125);
    std::cout << "Check linear y smear.\n";
    CPPUNIT_ASSERT(kernel[62 + 125*62] > 0.0);
    CPPUNIT_ASSERT(kernel[62 + 125*70] > 0.0);

}
