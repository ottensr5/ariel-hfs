#ifndef UTILITYTEST_H
#define UTILITYTEST_H
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

class UtilityTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(UtilityTest);
  CPPUNIT_TEST(testPoisson);
  CPPUNIT_TEST(testNormal);
  CPPUNIT_TEST(testUpsampling);
  CPPUNIT_TEST(testDownsampling);
  CPPUNIT_TEST(testMatrixOp);
  CPPUNIT_TEST_SUITE_END();

public:
  

  void setUp();
  void tearDown();

  void testPoisson();
  void testNormal();
  void testUpsampling();
  void testDownsampling();
  void testMatrixOp();

  };

#endif //UTILITYTEST_H
