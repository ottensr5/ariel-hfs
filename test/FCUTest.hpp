#ifndef FCUTEST_H
#define FCUTEST_H
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

class FCUTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(FCUTest);
  CPPUNIT_TEST(testMinMean);
  CPPUNIT_TEST(testMedianFilter);
  CPPUNIT_TEST(testValidation);
  CPPUNIT_TEST(testAcquisition);
  CPPUNIT_TEST(testCentroid);
  CPPUNIT_TEST(testThreshold);
  CPPUNIT_TEST_SUITE_END();
  unsigned int noSignal[64*64];
  unsigned int noStar[64*64];
  unsigned int starImg[64*64];
  unsigned int acqImg[200*200];
  unsigned int sum_star, sum_acq;


public:
  

  void setUp();
  void tearDown();

  void testMinMean();
  void testMedianFilter();
  void testValidation();
  void testAcquisition();
  void testCentroid();
  void testThreshold();
  };

#endif //FCUTEST_H
