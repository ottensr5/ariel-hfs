#include <cppunit/config/SourcePrefix.h>
#include "../src/HFS_API.hpp"
#include "HFSTest.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <experimental/filesystem>
#include <math.h>

#define TWO_PI 6.28318531f

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(HFSTest);


void HFSTest::setUp()
{
}

void HFSTest::tearDown()
{
}

void HFSTest::testConstructor()
{
    struct hfs_state state;
    char path[] = "./test/HFS_config_test.xml";
    char path_alt[] = "./test/HFS_config_test_alt.xml";
    char* real_path = realpath(path, NULL);
    char* real_path_alt = realpath(path_alt, NULL);
    FGS fgs(real_path);
    FGS fgs_alt(real_path_alt);

    state = fgs.get_hfs_state();

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.timestep, 0.0125, 0.001);
  
    CPPUNIT_ASSERT_EQUAL((int) state.rand_seed, 135412);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.start_exposure, 0.0, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.flat_mean, 0.97, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.flat_sigma, 0.03, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.bias_value, 2000, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.read_noise, 20, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.bkgr_noise, 1.1, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.dark_mean, 1, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.gain, 1.0, 0.001);

    CPPUNIT_ASSERT_EQUAL((int) state.full_well_cap, 100000);

    CPPUNIT_ASSERT_EQUAL((int) state.med_threshold, 1000);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.jitter_error, 5.0, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.reset_duration, 0.5, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.reset_end, 0.0, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.timing_tolerance, 0.005, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.ps_fgs1, 175.0, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.ps_fgs2, 137.0, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.fl_fgs1, 21.21, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.fl_fgs2, 27.08, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.qe_fgs1, 0.58, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.qe_fgs2, 0.70, 0.001);
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.offset_FGS1[0], 0, 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.offset_FGS1[1], 0, 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.offset_FGS2[0], 0, 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.offset_FGS2[1], 0, 0.001);

    CPPUNIT_ASSERT_EQUAL((int)state.channel, 1);

    CPPUNIT_ASSERT_EQUAL(state.mode, 2);

    CPPUNIT_ASSERT_EQUAL((int) state.fwhm1_x, 5);
    CPPUNIT_ASSERT_EQUAL((int) state.fwhm1_y, 5);

    CPPUNIT_ASSERT_EQUAL((int) state.fwhm2_x, 6);
    CPPUNIT_ASSERT_EQUAL((int) state.fwhm2_y, 6);
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.transition_delay_channel, 0.5, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.transition_delay_mode, 0.5, 0.001);
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.transition_end, 0.0, 0.001);

    CPPUNIT_ASSERT_EQUAL((int) state.target_signal, 60000);
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.lower_sig_lim, 1.0, 0.001);
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.pearson_limit, 0.6, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.qe, 0.58, 0.001);

    CPPUNIT_ASSERT_EQUAL((int)state.iter_track, 5);

    CPPUNIT_ASSERT_EQUAL((int)state.med_track, 1);

    CPPUNIT_ASSERT_EQUAL((int)state.tolerance_track, 30);

    CPPUNIT_ASSERT_EQUAL((int)state.threshold_track, 2);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.delay_track, 0.02, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.exp_track, 0.1, 0.001);

    CPPUNIT_ASSERT_EQUAL((int) state.track_dim, 64);

    CPPUNIT_ASSERT_EQUAL((int)state.iter_acq, 3);

    CPPUNIT_ASSERT_EQUAL((int)state.med_acq, 1);

    CPPUNIT_ASSERT_EQUAL((int)state.threshold_acq, 2);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.delay_acq, 0.1, 0.001);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.exp_acq, 0.5, 0.001);

    CPPUNIT_ASSERT_EQUAL((int) state.tolerance_acq, 30);
    
    CPPUNIT_ASSERT_EQUAL((int) state.acq_dim_fgs1, 128);
    
    CPPUNIT_ASSERT_EQUAL((int) state.acq_dim_fgs2, 162);

    CPPUNIT_ASSERT_EQUAL((int) state.max_targets, 25);

    CPPUNIT_ASSERT_EQUAL((int) state.extension_sigma, 1000);

    CPPUNIT_ASSERT_EQUAL((int) state.ramp_length, 100);

    CPPUNIT_ASSERT_EQUAL((int) state.ramp_counter, 0);

    CPPUNIT_ASSERT_EQUAL((int) state.sat_limit, 5);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.x_smear_0, 0.0, 0.001);
    
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.y_smear_0, 0.0, 0.001);

    state = fgs_alt.get_hfs_state();

    CPPUNIT_ASSERT_EQUAL((int)state.channel, 2);
}

void HFSTest::testChannelChange()
{
    struct hfs_state state;
    char path[] = "./test/HFS_config_test.xml";
    char* real_path = realpath(path, NULL);
    FGS fgs(real_path);

    fgs.set_channel(2);
    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_EQUAL((int)state.channel, 2);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.qe, 0.70, 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.plate_scale, 137.0, 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.focal_length, 27.08, 0.001);

    fgs.set_channel(1);
    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_EQUAL((int)state.channel, 1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.qe, 0.58, 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.plate_scale, 175.0, 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.focal_length, 21.21, 0.001);

}

void HFSTest::testModeChange()
{
    struct hfs_state state;
    char path[] = "./test/HFS_config_test.xml";
    char* real_path = realpath(path, NULL);
    FGS fgs(real_path);

    fgs.set_mode("Acquisition");

    state = fgs.get_hfs_state();

    CPPUNIT_ASSERT_EQUAL(state.mode, 1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.delay, 0.1, 0.001);
    CPPUNIT_ASSERT_EQUAL((int)state.dim_x, 128);
    CPPUNIT_ASSERT_EQUAL((int)state.dim_y, 128);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.exposure_time, 0.5, 0.001);

    fgs.set_mode("Tracking");

    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_EQUAL(state.mode, 2);
}

void HFSTest::testTransformation()
{

    char path[] = "./test/HFS_config_test.xml";
    char* real_path = realpath(path, NULL);
    FGS fgs(real_path);
    double vecx[3] = {1e-5, 0, 1};
    double vecy[3] = {0, 1e-5, 1};
    double vecz[3] = {0, 0, 1};
    double vecVel[3] = {0, 0, 0};
    double output[2];
    double velRes;

    fgs.transform_to_detector(vecx, vecVel, output);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 1e-5*(360/TWO_PI)*3600000/175.00, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 0, 1e-7);

    fgs.transform_to_detector(vecy, vecVel, output);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 0, 1e-7);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 1e-5*(360/TWO_PI)*3600000/175.0, 1e-5);

    fgs.transform_to_detector(vecz, vecVel, output);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 0, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 0, 1e-5);

    vecVel[0] = 5;

    fgs.transform_to_detector(vecx, vecVel, output);

    velRes = (1e-5 + 5/CVEL)*(360/TWO_PI)*3600000/175.0;

    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], velRes, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 0, 1e-5);

    vecVel[0] = 0;
    vecVel[1] = 5;

    fgs.transform_to_detector(vecy, vecVel, output);
    velRes = (1e-5 + 5/CVEL)*(360/TWO_PI)*3600000/175.0;
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 0, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], velRes, 1e-5);

    vecVel[1] = 0;
    vecVel[0] = 0;
    fgs.set_mode("Acquisition");
    
    vecx[0] = 1e-5;
    vecx[1] = 0;
    vecx[2] = 1;
    fgs.transform_to_detector(vecx, vecVel, output);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 1e-5*(360/TWO_PI)*3600000/175.00, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 0, 1e-7);
    vecy[0] = 0;
    vecy[1] = 1e-5;
    vecy[2] = 1;
    fgs.transform_to_detector(vecy, vecVel, output);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 0, 1e-7);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 1e-5*(360/TWO_PI)*3600000/175.0, 1e-5);

    fgs.transform_to_detector(vecz, vecVel, output);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 0, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 0, 1e-5);

    vecVel[0] = 5;

    fgs.transform_to_detector(vecx, vecVel, output);
    velRes = (1e-5 + 5/CVEL)*(360/TWO_PI)*3600000/175.0;
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], velRes, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], 0, 1e-5);

    vecVel[0] = 0;
    vecVel[1] = 5;

    fgs.transform_to_detector(vecy, vecVel, output);
    velRes = (1e-5 + 5/CVEL)*(360/TWO_PI)*3600000/175.0;
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[0], 0, 1e-5);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(output[1], velRes, 1e-5);
}

void HFSTest::testMainLoop()
{
    char path[] = "./test/HFS_config_test.xml";
    char* real_path = realpath(path, NULL);
    FGS fgs(real_path);
    struct hfs_state state;
    hfs_parameters update;
    outputHfs outPacket;
    double quat[4] = {0.8689485897529942 , 0.36411458356110155 , -0.3351849018145181, 0.0};
    double vel[3] = {0.0, 0.0, 0.0};
    double time;
    double dt = 0.0125;
    unsigned int i;
    unsigned int ctr = 0;

    outPacket.time = 0.;
    time = 0.;
    update.reset = 0;
    /*set FGS channel*/
    update.channel = 1;
    update.mode = 2;

    /*create xml file with current state*/
    update.save = 0;
    update.set_invalid = 0;

    update.set_error = 0;
    update.time = time;

    /*set shifts*/
    update.add_shift_x = 0;
    update.add_shift_y = 0;
    update.mult_shift_x = 1;
    update.mult_shift_y = 1;

    update.target_pos_x = 0;
    update.target_pos_y = 0;
      
    /*Set signal for target identification in ADU/s*/
    update.validation_signal = 80000;

    /*Unit quaternion of the current SC attitude in reference to J2000*/
    update.position_quat[0] = quat[0];
    update.position_quat[1] = quat[1];
    update.position_quat[2] = quat[2];
    update.position_quat[3] = quat[3];

    update.scVelocity[0] = vel[0];
    update.scVelocity[1] = vel[1];
    update.scVelocity[2] = vel[2];

    /*Angular rate in the SC reference frame in arcsec/s*/
    update.ang_rate[0] = 0;
    update.ang_rate[1] = 1;
    update.ang_rate[2] = 1;

    fgs.set_params(update, &outPacket);
    ctr++;
    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.sim_time, 0.0, 0.00001);
    std::cout << "Check exposure start\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.start_exposure, 0.0, 0.00001);

    update.target_pos_x = 1;
    update.target_pos_y = 2;

    update.time = update.time + dt;    
    fgs.set_params(update, &outPacket);
    ctr++;
    state = fgs.get_hfs_state();
    std::cout << "Check time propagation\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.sim_time, dt, 0.00001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.start_exposure, 0.0, 0.00001);
    
    update.target_pos_x = 0;
    update.target_pos_y = 0;
    std::cout << "Check Drift\n";
    for(i = 0; i < 10; i++)
    {
        update.time = update.time + dt;    
        fgs.set_params(update, &outPacket);
        ctr++;
        state = fgs.get_hfs_state();
    }
    state = fgs.get_hfs_state();

    std::cout << "Check new integration start\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.start_exposure, 0.1 + ctr * 1.5e-5, 0.0001);

    std::cout << "Check generated centroid packet\n";
    CPPUNIT_ASSERT(outPacket.x > 1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(outPacket.time, 0.12 + ctr * 1.5e-5, 0.01);

    std::cout << "Check Channel transition\n";
    
    update.channel = 2;
    update.time = update.time + dt;
    fgs.set_params(update, &outPacket);
    ctr++;

    state = fgs.get_hfs_state();
    std::cout << "Trandition start internal time: " << state.internal_time << "\n";
    std::cout << "Trandition start sim time: " << state.sim_time << "\n";

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.transition_end, update.time + 0.5 + ctr * 1.5e-5, 0.0002);
    /* Advance time past transtion end*/
    for(i = 0; i < 50; i++)
    {
        update.time = update.time + dt;    
        fgs.set_params(update, &outPacket);
        ctr++;
    }
    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_EQUAL((int )state.channel, 2);

    std::cout << "Check Mode Transition\n";
    update.mode = 1;
    update.time = update.time + dt;
    fgs.set_params(update, &outPacket);
    ctr++;
    update.target_pos_x = 1;
    update.target_pos_y = 2;

    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.transition_end, update.time + 0.5 + ctr * 1.5e-5, 0.0003);
    for(i = 0; i < 100; i++)
    {
        update.time = update.time + dt;    
        fgs.set_params(update, &outPacket);
        ctr++;
    }
    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_EQUAL((int)state.mode, 1);

    std::cout << "Check borders for Tracking\n";

    update.mode = 2;
    update.target_pos_x = 80;
    update.target_pos_y = 80;

    for(i = 0; i < 100; i++)
    {
        update.time = update.time + dt;    
        fgs.set_params(update, &outPacket);
        ctr++;
    }
    state = fgs.get_hfs_state();

    CPPUNIT_ASSERT_EQUAL(state.target_pos_x, 80);
    CPPUNIT_ASSERT_EQUAL(state.target_pos_y, 80);

    update.target_pos_x = 0;
    update.target_pos_y = 0;
    std::cout << "Check Reset of FGS\n";
    update.reset = 1;
    update.time = update.time + dt;    
    fgs.set_params(update, &outPacket);
    ctr++;
    
    update.reset = 0;
    update.channel = 1;
    state = fgs.get_hfs_state();
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.reset_end, update.time + 0.5 + ctr * 1.5e-5, 0.0004);
    /* Advance time past reset end*/
    for(i = 0; i < 50; i++)
    {
        update.time = update.time + dt;    
        fgs.set_params(update, &outPacket);
        ctr++;
    }
    state = fgs.get_hfs_state();

    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.reset_end, 0.0, 0.0001);
    CPPUNIT_ASSERT_EQUAL((int )state.channel, 1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.sim_time, update.time, 0.0001);
    

}

void HFSTest::testSave()
{
    char path[] = "./test/HFS_config_test.xml";
    char pathNew[] = "./HFS_config_1.xml";
    char* realPathNew = realpath(pathNew, NULL);
    char* realPath = realpath(path, NULL);
    FGS fgs(realPath);
    struct hfs_state state;
    struct hfs_state stateNew;    
    hfs_parameters update;
    outputHfs outPacket;
    double quat[4] = {0.8892834993148474, -0.0006038143244162956, 0.2873837014059442, -0.3557880006700909};
    double vel[3] = {0.0, 0.0, 0.0};
    double time;
    double dt = 0.0125;
    unsigned int i;

    outPacket.time = 0.;
    time = 0.;
    update.reset = 0;
    /*set FGS channel*/
    update.channel = 1;
    update.mode = 2;

    /*create xml file with current state*/
    update.save = 0;
    update.set_invalid = 0;

    update.set_error = 0;
    update.time = time;

    /*set shifts*/
    update.add_shift_x = 0;
    update.add_shift_y = 0;
    update.mult_shift_x = 1;
    update.mult_shift_y = 1;

    update.target_pos_x = 0;
    update.target_pos_y = 0;
      
    /*Set signal for target identification in ADU/s*/
    update.validation_signal = 80000;

    /*Unit quaternion of the current SC attitude in reference to J2000*/
    update.position_quat[0] = quat[0];
    update.position_quat[1] = quat[1];
    update.position_quat[2] = quat[2];
    update.position_quat[3] = quat[3];

    update.scVelocity[0] = vel[0];
    update.scVelocity[1] = vel[1];
    update.scVelocity[2] = vel[2];

    /*Angular rate in the SC reference frame in arcsec/s*/
    update.ang_rate[0] = 0;
    update.ang_rate[1] = 1;
    update.ang_rate[2] = 1;

    fgs.set_params(update, &outPacket);

    update.channel = 2;
    for(i = 0; i < 50; i++)
    {
        update.time = update.time + dt;    
        fgs.set_params(update, &outPacket);
    }
    update.save = 1;
    fgs.set_params(update, &outPacket);
    update.save = 0;

    FGS fgsSave(realPathNew);
    
    state = fgs.get_hfs_state();
    stateNew = fgsSave.get_hfs_state();

    CPPUNIT_ASSERT_EQUAL((int)stateNew.channel, 2);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(state.start_exposure, stateNew.start_exposure, 0.000001);
    CPPUNIT_ASSERT_EQUAL((int) state.rand_seed, (int) stateNew.rand_seed);
}
