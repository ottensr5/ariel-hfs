<HFS_MetaInfo>

  <simulation_time>0.0</simulation_time>

  <internal_time>0.0</internal_time>


  <!--  Global timestep of the matlab simulink model.
        Default: 0.0125 (80Hz)-->
  <timestep>0.0125</timestep>

  <!-- 	Random seed used in the noise generation -->
  <random_seed>135412</random_seed>

  <!-- 	Starting time of last integration, used for saving the current
      	simulator state. Default for new runs: 0 -->
  <integration_start>0</integration_start>

  <!--  The mean value for pixel sensitivities. Allowed
        value range: 0.0-1.0. Note that the simulated flat
        field is clipped at 1.0 per pixel. -->
  <flat_mean>0.97</flat_mean>

  <!--  The standard deviation of the pixel sensitivities. Al-
        lowed value range: 0.0-1.0. Note that the simulated
        flat field is clipped at 1.0 per pixel.  -->
  <flat_sigma>0.03</flat_sigma>

  <!-- Fixed bias value that is included in the final image. Default: 2000 -->
  <bias>2000.0</bias>

  <!--  The mean read noise of a pixel in ADU. Default: 20 -->
  <readout_noise>20.0</readout_noise>

  <!-- The mean bkgr noise in photons per second. -->
  <bkgr_noise>1.1</bkgr_noise>

  <!--  The mean dark current in a pixel in electrons per
        second. Deefault: 1 -->
  <dark_average>1</dark_average>

  <!--  The gain to the detectors. Default: 1 -->
  <gain>1.0</gain>
  
  <!--  The full well capacity of the detector in ADU  -->
  <full_well_capacity>100000</full_well_capacity>

  <!--  The difference threshold in ADU at which the median filter will replace
        the image value with the local median -->
  <median_filter_threshold>1000</median_filter_threshold>

  <!--  Standard deviation of the centroid error induced by the spacecraft
        jitter during the integration period in mas. -->
  <jitter_error_std>5.0</jitter_error_std>

  <!--  Duration of the simulated FCU reset in s -->
  <reset_duration>0.5</reset_duration>

  <!--  State variable denoting end of ongoing reset, Default: 0-->
  <reset_end>0</reset_end>

  <!--  Tolerance for the timing related checks for centroid generation, reset 
        and transitions, Default: 0.005-->
  <timing_tolerance>0.005</timing_tolerance>

  <timing_jitter>1e-6</timing_jitter>

  <timing_drift>1.5e-5</timing_drift>

  <!--  relative path of the Psf input files. The psfs are given as 40x40px
        arrays that are stored in the C array format -->

  <FGS1_Psf>./FGS1_270nm.txt</FGS1_Psf>

  <FGS2_Psf>./FGS2_270nm.txt</FGS2_Psf>

  <!-- Plate scale of FGS1 in mas/px, Default: 175 -->
  <FGS1_PS>175.0</FGS1_PS>

  <!-- Plate scale of FGS2 in mas/px, Default: 136 -->
  <FGS2_PS>137.0</FGS2_PS>
  
  <!-- Focal length of FGS1 in m, Default: 21.21 -->
  <FGS1_FL>21.21</FGS1_FL>

  <!-- Focal length of FGS2 in m, Default: 27.08 -->
  <FGS2_FL>27.08</FGS2_FL>

  <!-- Quantum efficiency of FGS1, Default: 0.58 -->
  <FGS1_QE>0.58</FGS1_QE>

  <!-- Quantum efficiency of FGS2, Default: 0.70 -->
  <FGS2_QE>0.70</FGS2_QE>

  <!-- Angular missalignment of the FGS channels from the optical axis in the the detector plane in mas, No default values available -->
  <FGS1_Offset_X>0</FGS1_Offset_X>
  <FGS1_Offset_Y>0</FGS1_Offset_Y>

  <FGS2_Offset_X>0</FGS2_Offset_X>
  <FGS2_Offset_Y>0</FGS2_Offset_Y>

  <!--  Starting channel of the Simulator: 1 or 2 -->
  <FGS_channel>1</FGS_channel>

  <!--  Starting operational mode of the Simulator. Possible entries are:
        - Tracking
        - Acquisition
        - Standby
  -->
  <FGS_mode>Tracking</FGS_mode>

  <!-- FWHM of the gaussian used as a weighting function in the centroid calculation in px.
      Default: FGS1: x = 5, y = 5; FGS2: x = 6, y = 6
  -->
  <FWHM_FGS1_x>5</FWHM_FGS1_x>
  <FWHM_FGS1_y>5</FWHM_FGS1_y>
  <FWHM_FGS2_x>6</FWHM_FGS2_x>
  <FWHM_FGS2_y>6</FWHM_FGS2_y>

  <!-- Time delay causes by the channel switch in s. -->
  <transition_delay_channel>0.5</transition_delay_channel>

  <!-- Time delay causes by the mode switch in s. -->
  <transition_delay_mode>0.5</transition_delay_mode>

  <!--  End time of an ongoing transition. Used for State saving. 0 denotes no transition. Default: 0 -->
  <transition_end>0.0</transition_end>

  <!--  Target signal in ADU per second used for validation -->
  <target_signal>60000</target_signal>

  <!--  lower signal limit ("There is a signal" check) for centroid validation in % of target signal. defalt 1%-->
  <lower_signal_limit>1</lower_signal_limit>

  <!--  Threshold for the Pearson correlation used as the validity index.
        Index can range from -1 to 1. A value of 0.5 to 0.6 serves as a
        decent cutoff for invalid measurements-->
  <validation_pearson_limit>0.6</validation_pearson_limit>

  <!-- default failed status of the HFS -->
  <failed_status>0</failed_status>

  <!--  Ramp length in samples. Default: 100 -->
  <ramp_length>100</ramp_length>

  <!-- current position in the ramp. Default: 0 -->
  <ramp_counter>0</ramp_counter>

  <!-- Maximum ramp length. Default: 150 -->
  <max_ramp>150</max_ramp>
  
  <!-- Limit for staturated pixels in an image. At this limit, the centroid will be set invalid. 
  Used for the Snowball detection. Default: 5 -->
  <saturation_limit>5</saturation_limit>
  
  <!-- Calibration and hot pixel map that replaces the first sample of a ramp for FGS1 -->
  <reset_calibration_FGS1>./Calibration_FGS1.txt</reset_calibration_FGS1>

  <!-- Calibration and hot pixel map that replaces the first sample of a ramp for FGS2 -->
  <reset_calibration_FGS2>./Calibration_FGS2.txt</reset_calibration_FGS2>
  
  <!--  Name of the Hot and Dead Pixel input map to be used by the HFS for FGS2 -->
  <hp_map_FGS1>./hotPixels_FGS1.txt</hp_map_FGS1>

  <!--  Name of the Hot and Dead Pixel input map to be used by the HFS for FGS2 --> 
  <hp_map_FGS2>./hotPixels_FGS2.txt</hp_map_FGS2>

  <!-- Noise of the reset anomaly -->
  <reset_noise>10</reset_noise>

  <!--  The radius of the circular field of view in arcsec. set to zero for full FoV. 
   Default: 22 -->
  <circular_fov_radius>22</circular_fov_radius>

  <!--  Configuration of the Tracking procedure -->
  <Tracking>
    <!--  Iterations of the algorithm. Standard is 5 -->
    <iterations>5</iterations>
    <!--  Toogle optional median filter -->
    <median_filter>1</median_filter>
    <!--  Toogle thresholding -->
    <thresholding>2</thresholding>
    <!--  Delay of the centroid packet in seconds.
          Note: must be lower than the integration time (0.125 s) -->
    <delay_mean>0.02</delay_mean>

    <delay_std>0.01</delay_std>

    <delay_min>0.01</delay_min>

    <delay_max>0.07</delay_max>
    <!-- Exposure time for mode Tracking in s, default: 0.1 -->
    <exposure_time>0.1</exposure_time>
    <!--  Brightness tolerance for Validation default is 30% -->
    <tolerance>30</tolerance>
    <!-- Dimension of the Tracking window in px, default: 64 -->
    <dim>64</dim>
  </Tracking>

  <!--  Configuration of the Acquisition procedure -->
  <Acquisition>
    <!--  Iterations of the algorithm. Standard is 3 -->
    <iterations>3</iterations>
    <!--  Toogle optional median filter -->
    <median_filter>1</median_filter>
    <!--  Toogle thresholding -->
    <thresholding>2</thresholding>
    <!--  Delay of the centroid packet in seconds.
          Note: must be lower than the integration time (0.5 s) -->
    <delay_mean>0.1</delay_mean>

    <delay_std>0.01</delay_std>

    <delay_min>0.03</delay_min>

    <delay_max>0.07</delay_max>
    <!-- Exposure time for mode Acquisition in s, default: 0.5 -->
    <exposure_time>0.5</exposure_time>
    <!--  Brightness tolerance for target identification default is 30% -->
    <tolerance>30</tolerance>
    <!-- Dimensiton of the Acquisition window for FGS1, default: 128px -->
    <FGS1_dim>128</FGS1_dim>
    <!-- Dimensiton of the Acquisition window for FGS2, default: 162px -->
    <FGS2_dim>162</FGS2_dim>
    <!-- Limit of brightest sources to be considered in the source extraction. Default: 25-->
    <target_limit>25</target_limit>
    <!-- Signal Sigma Limit for Source extent check. Default: 1000. Possible cause for 107 error if set too high-->
    <extension_sigma>1000</extension_sigma>
  </Acquisition>

  <!--  Relative path to the star catalogue -->
  <Star_catalogue>./src/Star_catalogue.csv</Star_catalogue>

  <!-- 	Flag indicating if images features shall be generated from existing files. 
  	Only applicable for saved states-->
  <use_saved_files>0</use_saved_files>

  <flat>./flat_field.txt</flat>

  <smear>./smear_kernel.txt</smear>

  <ramp>./ramp_input.txt</ramp>

  <previous_image>./previous_image.txt</previous_image>
  
  <centroid_queue>./centroid_queue.txt</centroid_queue>

  <smear_x>0.0</smear_x>
  <smear_y>0.0</smear_y>

  <trf_x>0</trf_x>
  <trf_y>0</trf_y>

</HFS_MetaInfo>
