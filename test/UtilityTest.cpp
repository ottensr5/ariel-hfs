#include <cppunit/config/SourcePrefix.h>
#include "../src/utilities.h"
#include "UtilityTest.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <experimental/filesystem>
#include <math.h>

CPPUNIT_TEST_SUITE_REGISTRATION(UtilityTest);

void UtilityTest::setUp()
{
}

void UtilityTest::tearDown()
{
}

void UtilityTest::testPoisson()
{
    double array[200*200];
    unsigned int i;
    double mean;

    for(i = 0; i < 200*200; i++){
        array[i] = 0.0;
    }

    random_poisson_trm(array, 10., 200*200);
    mean = 0.;

    for(i = 0; i < 200*200; i++){
        mean = mean + array[i];
    }

    mean = mean / (200*200);

    std::cout << "Check if array has been populated with values.\n";
    CPPUNIT_ASSERT(mean != 0.0);
}

void UtilityTest::testNormal()
{
    double array[200*200];
    unsigned int i;
    double mean;

    for(i = 0; i < 200*200; i++){
        array[i] = 0.0;
    }

    random_normal_trm(array, 10., 2.5, 200*200);
    mean = 0.;

    for(i = 0; i < 200*200; i++){
        mean = mean + array[i];
    }

    mean = mean / (200*200);

    std::cout << "Check if array has been populated with values.\n";
    CPPUNIT_ASSERT(mean != 0.0);

    std::cout << "Check if mean is within margin.\n";
    CPPUNIT_ASSERT(mean > 9.5);
    CPPUNIT_ASSERT(mean < 10.5);
}

void UtilityTest::testUpsampling()
{
    double array[200*200], upArray[1000*1000];
    unsigned int i;
    double sum, sumUp;

    for(i = 0; i < 200*200; i++){
        array[i] = 0.0;
    }

    for(i = 0; i < 1000*1000; i++){
        upArray[i] = 0.0;
    }
    
    array[0] = 10.0;

    upsample_image(array, upArray, 200, 200, 1000, 1000, 5, 0);

    std::cout << "Check if array has been upsampled.\n";
    for(i = 0; i < 5; i++){
        CPPUNIT_ASSERT(upArray[i] != 0);
    }

    sum = 0.0;
    sumUp = 0.0;
    for(i = 0; i < 1000*1000; i++){
        if(i < 200*200){
            sum = sum + array[i];
        }
        sumUp = sumUp  + upArray[i];
    }

    std::cout << "Check sum of upsampled array without copy.\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(sum, sumUp, 0.0001);

    for(i = 0; i < 1000*1000; i++){
        upArray[i] = 0.0;
    }

    upsample_image(array, upArray, 200, 200, 1000, 1000, 5, 1);
    sum = 0.0;
    sumUp = 0.0;
    for(i = 0; i < 1000*1000; i++){
        if(i < 200*200){
            sum = sum + array[i];
        }
        sumUp = sumUp  + upArray[i];
    }

    std::cout << "Check sum of upsampled array with copy.\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(sum * 25, sumUp, 0.0001);

}

void UtilityTest::testDownsampling()
{
    double array[1000*1000], dsArray[200*200];
    unsigned int i;
    double sum, sumDown;

    for(i = 0; i < 1000*1000; i++){
        array[i] = 0.0;
    }

    for(i = 0; i < 200*200; i++){
        dsArray[i] = 0.0;
    }
    
    for(i = 0; i < 5; i++){
        array[i] = 10.;
    } 

    downsample_image(array, dsArray, 1000, 1000, 5);
    
    std::cout << "Check if array has been downsampled.\n"; 
    CPPUNIT_ASSERT(dsArray[0] != 0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dsArray[1], 0.0, 0.0001);
    sum = 0.0;
    sumDown = 0.0;
    for(i = 0; i < 1000*1000; i++){
        if(i < 200*200){
            sumDown = sumDown + dsArray[i];
        }
        sum = sum  + array[i];
    }

    std::cout << "Check sum of downsampled array.\n";
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, sumDown, 0.0001);
    for(i = 0; i < 1000*1000; i++){
        array[i] = 0.0;
    }

    downsample_image(array, dsArray, 1000, 1000, 5);
    std::cout << "Check for division by zero error.\n";
    
    for(i = 0; i < 200*200; i++){
        CPPUNIT_ASSERT_DOUBLES_EQUAL(dsArray[0], 0, 0.0001);
    }
}

void UtilityTest::testMatrixOp()
{
    double x[3] = {1., 0., 0.};
    double y[3] = {0., 1., 0.};
    double mxData[9] = {1. ,0., 0., 0., 1., 0., 0., 0., 1.};
    double resData[3];
    struct dmatrix inMatrix1, inMatrix2, resMatrix;
    double resVec[3];
    double res;
    int flag;
    
    std::cout << "Check Dot Product.\n";
    res = dot(x, y, 3);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(res, 0.0, 0.0001);
    
    res = dot(x, x, 3);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(res, 1.0, 0.0001);

    std::cout << "Check Cross Product.\n";
    
    cross3(x, y, resVec);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(resVec[0], 0.0, 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(resVec[1], 0.0, 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(resVec[2], 1.0, 0.0001);

    inMatrix1.data = mxData;
    inMatrix1.xdim = 3;
    inMatrix1.ydim = 3;

    inMatrix2.data = x;
    inMatrix2.xdim = 1;
    inMatrix2.ydim = 3;

    resMatrix.data = resData;


    std::cout << "Check Matrix Multiplication.\n";
    flag = dmatmult(inMatrix1, inMatrix2, &resMatrix);

    CPPUNIT_ASSERT(flag == 0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(resMatrix.data[0], 1.0, 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(resMatrix.data[1], 0.0, 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(resMatrix.data[2], 0.0, 0.0001);

    inMatrix2.ydim = 4;
    flag = dmatmult(inMatrix1, inMatrix2, &resMatrix);
        
    CPPUNIT_ASSERT(flag == -1);
   
}
