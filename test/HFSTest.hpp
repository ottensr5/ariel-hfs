#ifndef HFSTEST_H
#define HFSTEST_H
#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>

class HFSTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(HFSTest);
  CPPUNIT_TEST(testConstructor);
  CPPUNIT_TEST(testChannelChange);
  CPPUNIT_TEST(testModeChange);
  CPPUNIT_TEST(testTransformation);
  CPPUNIT_TEST(testMainLoop);
  CPPUNIT_TEST(testSave);
  CPPUNIT_TEST_SUITE_END();

public:
  void setUp();
  void tearDown();

  void testConstructor();

  void testChannelChange();

  void testModeChange();

  void testTransformation();

  void testMainLoop();

  void testSave();

};

#endif //HFSTEST_H
