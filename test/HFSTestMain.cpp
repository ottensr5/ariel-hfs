#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "HFSTest.hpp"
#include "DetectorFeatureTest.hpp"
#include "UtilityTest.hpp"
#include "FCUTest.hpp"

int main(int argc, char* argv[])
{
    // Get the top level suite from the registry

    // Adds the test to the list of test to run
    CppUnit::TextUi::TestRunner runner;
    runner.addTest(HFSTest::suite());
    runner.addTest(DetectorFeaturesTest::suite());
    runner.addTest(UtilityTest::suite());
    runner.addTest(FCUTest::suite());

   
    // Change the default outputter to a compiler error format outputter
    runner.setOutputter( new CppUnit::CompilerOutputter(&runner.result(), std::cerr));
    
    // Run the tests.
    bool wasSucessful = runner.run();

    // Return error code 1 if the one of test failed.
    return wasSucessful ? 0 : 1;
}
