ifeq ($(OS), Windows_NT)
	DLLEXT := .dll
else
	DLLEXT := .so
endif

CC = gcc
CFLAGS += -fPIC -shared -lm -lfftw3 -fopenmp -O3 -lfftw3_omp -fPIC -Wall -Werror
SRC_DIR = $(shell pwd)/src
TEST_DIR = $(shell pwd)/test
DEST_DIR = $(shell pwd)/src
INCLUDE_DIR = $(shell pwd)/src

TESTS = $(TEST_DIR)/HFSTest.o $(TEST_DIR)/DetectorFeatureTest.o $(TEST_DIR)/UtilityTest.o $(TEST_DIR)/FCUTest.o $(TEST_DIR)/HFSTestMain.o

LIBRARIES = $(DEST_DIR)/libutilities$(DLLEXT) $(DEST_DIR)/libdetector_features$(DLLEXT) $(DEST_DIR)/libfcu_algorithms$(DLLEXT)
TARGET_API = $(DEST_DIR)/libHFS_API$(DLLEXT)

$(LIBRARIES): $(DEST_DIR)/lib%$(DLLEXT): $(SRC_DIR)/%.c
	$(CC) $< -shared -o $@ -fPIC $(CFLAGS)

$(TARGET_API): $(DEST_DIR)/lib%$(DLLEXT): $(SRC_DIR)/%.cpp
	$(CC) $< -shared -o $@ -L$(SRC_DIR) -lfcu_algorithms -lutilities -ldetector_features -ltinyxml2 -fopenmp -O3 -lstdc++ -xc++ -fPIC -Wall -Werror -Wl,-rpath=$(SRC_DIR)

$(TESTS): $(TEST_DIR)/%.o: $(TEST_DIR)/%.cpp
	g++ -c $< -o $@ -fPIC -L$(SRC_DIR) -lfcu_algorithms -lutilities -ldetector_features -lHFS_API -lcppunit -ltinyxml2 -lstdc++ -Wall -Wl,-rpath=$(SRC_DIR)

API_Test: $(TEST_DIR)/HFSTest.o $(TEST_DIR)/DetectorFeatureTest.o $(TEST_DIR)/UtilityTest.o $(TEST_DIR)/FCUTest.o $(TEST_DIR)/HFSTestMain.o
	g++ $(TEST_DIR)/HFSTestMain.o $(TEST_DIR)/HFSTest.o $(TEST_DIR)/DetectorFeatureTest.o $(TEST_DIR)/UtilityTest.o $(TEST_DIR)/FCUTest.o -o $(TEST_DIR)/API_Test -L$(SRC_DIR) -lfcu_algorithms -lutilities -ldetector_features -lHFS_API -lcppunit -ltinyxml2 -lstdc++ -fPIC -Wall -Werror -Wl,-rpath=$(SRC_DIR)

example: $(TEST_DIR)/example.cpp
	g++ $(TEST_DIR)/example.cpp -o ./example.out -L$(SRC_DIR) -lHFS_API -ltinyxml2 -lstdc++ -Wall -Werror -Wl,-rpath=$(SRC_DIR)

scan:
	@echo "+-----------------------+"
	@echo "| running static checks |"
	@echo "+-----------------------+"
	@echo

	@echo "1. CLANG"
	scan-build -V make all

	@echo "+--------------------------------+"
	@echo "| finished running static checks |"
	@echo "+--------------------------------+"
	@echo

	@echo "+--------------------+"
	@echo "| running unit tests |"
	@echo "+--------------------+"
	@echo

	make API_Test
	$(TEST_DIR)/API_Test

	@echo "+---------------------+"
	@echo "| finished unit tests |"
	@echo "+---------------------+"
	@echo


all: $(LIBRARIES) $(TARGET_API)
	@echo "finished building all targets"

.PHONY: clean
clean:
	-${RM} $(SRC_DIR)/*.so 
	-${RM} $(TEST_DIR)/*.o 
	-${RM} example.out
