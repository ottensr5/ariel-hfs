*******************************************************************
* ARIEL FGS HFS Repository, ARIEL-UVIE-PL-ML-001 1.3.2, CHANGELOG *
*******************************************************************

################################################################################
V0.1:
4.May, 2022   Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

  Known open issues:

    - HFS does not support centroid delays larger than the integration period of
      the image. If a larger delay is configured, no centroid packet will be
      sent, as the centroid is overwritten before it is sent, reseting the
      delay.

    - The error induced by random spacecraft jitter is only implemented as a
      gaussian error term that is added at the end, which is configurable. This
      will be adressed in a future version where the jitter error shall be based
      of given power spectra.

    - The transition between operating modes and channels is currently
      implemented as a delay of a single integration period of the old mode
      before the new mode/channel is applied. This will be changed once more
      information on the detectors operation is available.

    - The centroids are given with respect to the FBA and FBT reference frame
      which are both centered on the center of the Detector reference frame.
      Future version shall allow for configurable offsets between the frames.
################################################################################
V0.2:
15.June, 2022   Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed conversion from Spacecraft reference frame to detector reference
      frame.

    - Fixed typo in conversion matrix.

    - Fixed typo in quaternion multiplication.

    - Added validation signal to input parameters of hfs_parameter struct.
      This should prevent wrong or invalid identification when changing targets.
################################################################################
V0.3:
27.June, 2022   Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed integer overflow in matlab simulink bus definition
    
    - Fixed sampling rate of mode Acquisition
################################################################################
V0.4:
15.March, 2023 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Changed timekeeping to remove need for sync-flag

    - Exposed all configurable Parameters to the configuration file

    - updated HFS interface to contain all parameters used in S196,2 (shifts and FBT origin configuration)

    - added function to set FGS error to HFS

    - Extended Star catalouge to include neighbouring stars and load all catalouge stars during operation

    - Improved saving function to allow saving of current smearing and constant image components
################################################################################
V0.5:
13.September, 2023 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed error with smearing kernel generation

    - Fixed errors in random seed saving

    - Fixed shot noise model

    - Fixed Simulink Bus data types

    - Fixed errors in centroiding algorithm implementation

    - Added Timing tolerance parameter
    
    - Fixed State preservation

    - Fixed uninitialized Parameters

    - Fixed of minor errors

    - Added Relativistic Aberration
################################################################################
V1.0:
1.November, 2023 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed smearing kernel border check

    - Fixed shot noise model

    - Fixed Simulink Bus data types of output

    - Added Timing tolerance parameter to centroid generation 

    - Fixed uninitialized Parameters if default mode is standby

    - Fixed error in Relativistic Abberation

    - Added aggressive thresholding method to help with tracking close to the 
      image border
    
    - Reworked Matlab function block, added Error and failed state outputs

    - Added Simulink examples

    - Fixed typos and Macro definitions

  Open Issues:

    - No change to close neighbour confusion, a correction will be provided in 
      a future update
################################################################################
V1.1:
31.January, 2024 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed bug in smearing kernel generation

    - moved Spacecraft - optical transformation outside of the HFS

    - Added bound warning for Smearing

    - Added validation for Tracking to avoid tracking wrong target

    - Removed gaia source id from Star Catalogue

    - Updated Star Validation Signal Table

    - Implemented full relativistic aberration model

    - updated Simulink examples

    - Added testplan and report

    - fixed CoG position update in Tracking algorithm

  Open Issues:
    - State Preservation: still discrepency on the first centroid.

################################################################################
V1.2:
03.May, 2024 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed saving of state

    - added circular FoV

    - added separate delays for mode and channel transitions

    - updated noise model to NDR

    - updated Hot Pixel implementation

    - Fixed shift intruduced by smeaing kernel

    - fixed issues in Target aquisition 

    - fixed error in Centroid validation

    - added check for saturated images

    - added compliance to test report

    - added output files for tests

    - updated Star validation table

    - updated gain modeling

    - switched HFS to 80 Hz

################################################################################
V1.3:
08.July, 2024 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed issue with bias at start of ramp

    - improved performance on of-axis targets by changing weighted images to floats
      and updating weighting function size

    - updated 108 check to trigger when stars have a smaller extent

    - removed 109 saturation check from target acquisition

    - updated Hot Pixel implementation

    - Fixed valgrind error in target acquistion

    - fixed issues in Target aquisition 

    - updated simulink test

    - removed redundant code

    - removed debug outputs

    - cleaned up uptdated code left over from previous generations

    - updated Star validation table

    - updated Background sources

    - fixed typos

################################################################################
V1.3.1:
25.July, 2024 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed issue with normalisation of Weighting function in WCoG

    - Changed TRF switch to only update when changing to Tracking

    - Added bounds check on star positioning

    - Updated Reset noise generation to be more performant

################################################################################
V1.3.2:
5.August, 2024 Gerald Mösenlechner   <gerald.moesenlechner@univie.ac.at>

   Fixed Issues:

    - Fixed issue with state preservation when Target Position is set in Tracking

    - Update Reset and Save Test to reflect fixed behaviour
#################################################################################

