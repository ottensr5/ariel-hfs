/**
* @file    HFS_Wrapper.cpp
* @author  Gerald Mösenlechner (gerald.moesenlechner@univie.ac.at)
* @date    November, 2024
* @version 1.3.3
*
* @brief Code for the Matlab Simulink S-function implementation example
*
*
* ## Overview
* This file contains functions to be used for the implementation of the HFS
* using the Matlab legacy code tool
*
*/

#include "./HFS_API.hpp"

FGS* fgsSim;

/**
 * @brief Method to be used in legacy code tool of Matlab for creating FGS obj.
 *
 * @param config: Configuration file
 *
 */

void createFGS()
{
  fgsSim = new FGS("./HFS_config.xml");
}

/**
 * @brief Method to be used in legacy code tool of Matlab for deleting FGS obj.
 *
 */

void deleteFGS()
{
  delete fgsSim;
}

/**
 * @brief Method to be used in legacy code tool of Matlab
 *
 * @param fgs: FGS object to store state of simulation
 * @param update: hfs parameters for update of state
 *
 * @return output packet containing the measurement, failed status and error
 */

void updateFGS(hfs_parameters *update, outputHfs *outPacket)
{
  fgsSim -> set_params(*update, outPacket);
}
