#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <string.h>
#include <complex.h>
#include <fftw3.h>
#include <stdbool.h>

#ifndef UTILITIES_H
#define UTILITIES_H
#ifdef __cplusplus
extern "C" {
#endif

struct dmatrix {
    double *data;
    int xdim;
    int ydim;
};

double random_poisson(double);

void random_poisson_trm(double *, double, unsigned int);

void random_normal_trm(double *, double, double, unsigned int);

double random_normal_number(double, double);

int upsample_image(double *, double *, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, int);

int downsample_image(double *, double *, unsigned int, unsigned int, unsigned int);

double *tile_array(double *, unsigned int, unsigned int, unsigned int, unsigned int);

double *rotate_image(double *, double, unsigned int, unsigned int, int);

double dot(double *, double *, unsigned int);

int cross3(double *, double *, double *);

int dmatmult (struct dmatrix, struct dmatrix, struct dmatrix *);

#ifdef __cplusplus
}
#endif
#endif //UTILITIES_H
