#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#ifndef FCU_ALGORITHMS_H
#define FCU_ALGORITHMS_H
#ifdef __cplusplus
extern "C" {
#endif

#define SWAP_SORTU(a,b) { if ((a) > (b)) { a = a ^ b; b = b ^ a; a = a ^ b; } }

struct valpack{
    int flag;
    float index;
    unsigned int magnitude;
};

struct coord{
  float x;
  float y;
  float time;
  float integration_start;
  struct valpack validity;
};

void MinMaxU32(unsigned int *, unsigned int, unsigned int *, unsigned int *);

void std_threshold(unsigned int *, unsigned int, unsigned int);

int MedFilter3x3 (unsigned int *, unsigned int, unsigned int, unsigned int, unsigned int *);

struct valpack CheckRoiForStar(unsigned int *, unsigned int, unsigned int, unsigned int, float, unsigned int, double, double, unsigned int, unsigned int, short);

struct coord SourceDetection(unsigned int *, unsigned int , unsigned int, int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, float, float, unsigned int, float);

struct coord ArielCoG (unsigned int *, unsigned int, unsigned int, unsigned int, float, float, int, unsigned int, float, unsigned int, unsigned int, short);

void CenterOfGravity2D(unsigned int *, unsigned int, unsigned int, float *, float *);

void WeightedCenterOfGravity2D(unsigned int *, float *, unsigned int, unsigned int, float *, float *);

void IntensityWeightedCenterOfGravity2D(unsigned int *, unsigned int, unsigned int, float *, float *);

int MeanSigma (unsigned int *, int, float *, float *);

unsigned int GetMin(unsigned int*, unsigned int);

int hotPixelCorrection(unsigned int *, unsigned short *, unsigned int, unsigned int, unsigned int);

#ifdef __cplusplus
}
#endif

#endif //FCU_ALGORITHMS_H
