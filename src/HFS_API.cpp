/**
* @file     HFS_API.cpp
* @author   Gerald Mösenlechner (gerald.moesenlechner@univie.ac.at)
* @date     November, 2024
* @version  1.3.3
*
* @brief Functions for the generation of detector features, such as bias, dark and flats.
*
*
* ## Overview
* C++ library containing the interface for the FGS Hight Fidelity Simulator
*
*/


#include "HFS_API.hpp"
#include "utilities.h"
#include "detector_features.h"
#include "fcu_algorithms.h"
#include <iomanip>
#include <tinyxml2.h>

const char* MODE_ACQU = "Acquisition";
const char* MODE_TRAC = "Tracking";
const char* MODE_STBY = "Standby";

double smear_kernel_os[XDIM_KERNEL*5*YDIM_KERNEL*5];
double smear_kernel[XDIM_KERNEL*YDIM_KERNEL];
double bias_sample[128];
double reset_sample[128];
double bias[XDIM*YDIM];
double dark[XDIM*YDIM];
double flat[XDIM*YDIM];
double bkgr[XDIM*YDIM];
double shot[XDIM*YDIM];
double hot_pixel_master_FGS1[XDIM*YDIM];
double hot_pixel_master_FGS2[XDIM*YDIM];
double hot_pixels[XDIM*YDIM];
double FGS1_Psf[XDIM_PSF*YDIM_PSF];
double FGS2_Psf[XDIM_PSF*YDIM_PSF];
double starmask[XDIM*YDIM];
double smeared_mask[XDIM*YDIM];
double starimage[XDIM*YDIM];
double fov_mask_FGS1[XDIM*YDIM];
double fov_mask_FGS2[XDIM*YDIM];
unsigned int image[XDIM*YDIM];
unsigned int image_ramp[XDIM*YDIM];
unsigned int previous_image[XDIM*YDIM];
unsigned int tmp_image[XDIM*YDIM];
unsigned int filtered_image[XDIM*YDIM];
unsigned short reference_cal_master_FGS1[XDIM*YDIM];
unsigned short reference_cal_master_FGS2[XDIM*YDIM];
unsigned short reference_cal[XDIM*YDIM];

/**
 * @brief resets all HFS parameters based on the provided configuration file
 *
 * The function resets all buffers of the FGS class and sets them to the values
 * given in the xml configuration file. The flat and the hot/defective pixels
 * are also simulated at this stage.
 *
 * @param config_file: name and path of the configuration file
 * @param hard_reset: flag to denote that the simulation time shall be reset
 *
 * @returns 0 on success
 */

int FGS :: reset_fgs(const char* config_file, int hard_reset)
{
    unsigned int i;
    std::ifstream psf1_if, psf2_if, hp_if, flat_if, smear_if, ramp_if, img_if, reference_if, queue_if;
    tinyxml2::XMLDocument config;
    config.LoadFile(config_file);
    tinyxml2::XMLElement *cRootElement = config.RootElement();
    unsigned int use_save;
    const char* hp_buffer1;
    const char* hp_buffer2;
    const char* flat_buffer;
    const char* smear_buffer;
    const char* ramp_buffer;
    const char* img_buffer;
    const char* queue_buffer;
    const char* psf1_buffer;
    const char* psf2_buffer;
    const char* catalogue_buffer;
    const char* modeInput;
    const char* reference_buffer1;
    const char* reference_buffer2;
    struct coord centroid_buffer;

    targets -> number = 0;
    /* Initialise smearing points and counters*/
    x_smear_0 = 0.;
    y_smear_0 = 0.;
    sync_ctr = 0;
    sim_time = 0.;
    trf_x = 0;
    trf_y = 0;
    sat_counter = 0;
    input_file = config_file;

    /*parse xml file*/
    if (NULL != cRootElement){
        if(hard_reset == 1){
            cRootElement -> FirstChildElement("simulation_time") -> QueryDoubleText(&sim_time);
            cRootElement -> FirstChildElement("internal_time") -> QueryDoubleText(&internal_time);
            cRootElement -> FirstChildElement("timestep") -> QueryDoubleText(&timestep);
            cRootElement -> FirstChildElement("random_seed") -> QueryUnsignedText(&rand_seed);
            cRootElement -> FirstChildElement("integration_start") -> QueryDoubleText(&start_exposure);
        }
        else{
            start_exposure = sim_time;
        }
        cRootElement -> FirstChildElement("flat_mean") -> QueryDoubleText(&flat_mean);
        cRootElement -> FirstChildElement("flat_sigma") -> QueryDoubleText(&flat_sigma);
        cRootElement -> FirstChildElement("bias") -> QueryDoubleText(&bias_value);
        cRootElement -> FirstChildElement("bkgr_noise") -> QueryDoubleText(&bkgr_noise);
        cRootElement -> FirstChildElement("readout_noise") -> QueryDoubleText(&read_noise);
        cRootElement -> FirstChildElement("dark_average") -> QueryDoubleText(&dark_mean);
        cRootElement -> FirstChildElement("gain") -> QueryDoubleText(&gain);
        cRootElement -> FirstChildElement("full_well_capacity") -> QueryUnsignedText(&full_well_cap);
        cRootElement -> FirstChildElement("median_filter_threshold") -> QueryUnsignedText(&med_threshold);
        cRootElement -> FirstChildElement("jitter_error_std") -> QueryDoubleText(&jitter_error);

        cRootElement -> FirstChildElement("reset_duration") -> QueryDoubleText(&reset_duration);
        cRootElement -> FirstChildElement("reset_end") -> QueryDoubleText(&reset_end);
        cRootElement -> FirstChildElement("timing_tolerance") -> QueryDoubleText(&timing_tolerance);
        cRootElement -> FirstChildElement("timing_jitter") -> QueryDoubleText(&timing_jitter);
        cRootElement -> FirstChildElement("timing_drift") -> QueryDoubleText(&timing_drift);
        /*Set random seed*/
        srand(rand_seed);

        psf1_buffer = cRootElement -> FirstChildElement("FGS1_Psf") -> GetText();
        psf_fgs1.assign(psf1_buffer);     
        /*check if Psf inputs exist*/
        psf1_if.open(psf1_buffer, std::ifstream::in);
        if (psf1_if.is_open()){
            if(is_empty(psf1_if)){
                std::cerr << "The PSF file for FGS1 is empty!" << std::endl;
                return -1;
            }
            for (i = 0; i < XDIM_PSF*YDIM_PSF; i++){
                psf1_if >> FGS1_Psf[i];
            }
            psf1_if.close();  
        }
        else {
            std::cerr << "Can't find input file " << psf_fgs1 << std::endl;
            return -1;
        }
        psf2_buffer = cRootElement -> FirstChildElement("FGS2_Psf") -> GetText();
        psf_fgs2.assign(psf2_buffer);

        psf2_if.open(psf2_buffer, std::ifstream::in);
        if (psf2_if.is_open()){
            if(is_empty(psf2_if)){
                std::cerr << "The PSF file for FGS2 is empty!" << std::endl;
                return -1;
            }
            for (i = 0; i < XDIM_PSF*YDIM_PSF; i++){
                psf2_if >> FGS2_Psf[i];
            }
            psf2_if.close();
        }
        else {
            std::cerr << "Can't find input file " << psf_fgs2 << std::endl;
            return -1;
        }
        cRootElement -> FirstChildElement("FGS1_PS") -> QueryDoubleText(&ps_fgs1);
        cRootElement -> FirstChildElement("FGS2_PS") -> QueryDoubleText(&ps_fgs2);
        cRootElement -> FirstChildElement("FGS1_FL") -> QueryDoubleText(&fl_fgs1);
        cRootElement -> FirstChildElement("FGS2_FL") -> QueryDoubleText(&fl_fgs2);
        cRootElement -> FirstChildElement("FGS1_QE") -> QueryDoubleText(&qe_fgs1);
        cRootElement -> FirstChildElement("FGS2_QE") -> QueryDoubleText(&qe_fgs2);
        cRootElement -> FirstChildElement("FGS1_Offset_X") -> QueryDoubleText(&offset_FGS1[0]);
        cRootElement -> FirstChildElement("FGS1_Offset_Y") -> QueryDoubleText(&offset_FGS1[1]);
        cRootElement -> FirstChildElement("FGS2_Offset_X") -> QueryDoubleText(&offset_FGS2[0]);
        cRootElement -> FirstChildElement("FGS2_Offset_Y") -> QueryDoubleText(&offset_FGS2[1]);

        cRootElement -> FirstChildElement("FGS_channel") -> QueryUnsignedText(&channel);
        modeInput = cRootElement -> FirstChildElement("FGS_mode") -> GetText();
        if (strcmp(modeInput, MODE_ACQU) == 0){
	        mode = MODE_ACQU;
        }
        else if (strcmp(modeInput, MODE_TRAC) == 0){
	        mode = MODE_TRAC;
        }
        else {
	        mode = MODE_STBY;
        }
        cRootElement -> FirstChildElement("FWHM_FGS1_x") -> QueryUnsignedText(&fwhm1_x);
        cRootElement -> FirstChildElement("FWHM_FGS1_y") -> QueryUnsignedText(&fwhm1_y);
        cRootElement -> FirstChildElement("FWHM_FGS2_x") -> QueryUnsignedText(&fwhm2_x);
        cRootElement -> FirstChildElement("FWHM_FGS2_y") -> QueryUnsignedText(&fwhm2_y);
        cRootElement -> FirstChildElement("transition_delay_channel") -> QueryDoubleText(&transition_delay_channel);
        cRootElement -> FirstChildElement("transition_delay_mode") -> QueryDoubleText(&transition_delay_mode);

        cRootElement -> FirstChildElement("transition_end") -> QueryDoubleText(&transition_end);
        cRootElement -> FirstChildElement("target_signal") -> QueryUnsignedText(&target_signal);
        cRootElement -> FirstChildElement("lower_signal_limit") -> QueryDoubleText(&lower_sig_lim);
        cRootElement -> FirstChildElement("validation_pearson_limit") -> QueryDoubleText(&pearson_limit);
        cRootElement -> FirstChildElement("failed_status") -> QueryUnsignedText(&failed);
        cRootElement -> FirstChildElement("ramp_length") -> QueryUnsignedText(&ramp_length);
        cRootElement -> FirstChildElement("ramp_counter") -> QueryUnsignedText(&ramp_counter);
        cRootElement -> FirstChildElement("max_ramp") -> QueryUnsignedText(&max_ramp);
        cRootElement -> FirstChildElement("saturation_limit") -> QueryUnsignedText(&sat_limit);

        reference_buffer1 = cRootElement -> FirstChildElement("reset_calibration_FGS1") -> GetText();
        reference_data_fgs1.assign(reference_buffer1);     
        reference_if.open(reference_buffer1, std::ifstream::in);

        if (reference_if.is_open()){
            if(is_empty(reference_if)){
                std::cerr << "The calibration file for FGS1 is empty!" << std::endl;
                return -1;
            }
            for (i = 0; i < XDIM*YDIM; i++){
                reference_if >> reference_cal_master_FGS1[i];
            }
            reference_if.close();  
        }
        else {
            std::cerr << "Can't find input file " << reference_data_fgs1 << std::endl;
            return -1;
        }

        reference_buffer2 = cRootElement -> FirstChildElement("reset_calibration_FGS2") -> GetText();
        reference_data_fgs2.assign(reference_buffer2);     
        reference_if.open(reference_buffer2, std::ifstream::in);

        if (reference_if.is_open()){
            if(is_empty(reference_if)){
                std::cerr << "The calibration file for FGS2 is empty!" << std::endl;
                return -1;
            }
            for (i = 0; i < XDIM*YDIM; i++){
                reference_if >> reference_cal_master_FGS2[i];
            }
            reference_if.close();  
        }
        else {
            std::cerr << "Can't find input file " << reference_data_fgs2 << std::endl;
            return -1;
        }

        hp_buffer1 = cRootElement -> FirstChildElement("hp_map_FGS1") -> GetText();
        hp_map_fgs1.assign(hp_buffer1);
	    hp_if.open(hp_buffer1, std::ifstream::in);

	    if (hp_if.is_open()){
            if(is_empty(hp_if)){
                std::cerr << "The hot pixel map for FGS1 is empty!" << std::endl;
                return -1;
            }
            for (i = 0; i < XDIM*YDIM; i++){
                hp_if >> hot_pixel_master_FGS1[i];
            }
            hp_if.close();

        }
	    else {
            std::cerr << "Can't find input file " << hp_buffer1 << std::endl;
            return -1;
        }

        hp_buffer2 = cRootElement -> FirstChildElement("hp_map_FGS2") -> GetText();
        hp_map_fgs2.assign(hp_buffer2);
	    hp_if.open(hp_buffer2, std::ifstream::in);

	    if (hp_if.is_open()){
            if(is_empty(hp_if)){
                std::cerr << "The hot pixel map for FGS2 is empty!" << std::endl;
                return -1;
            }
            for (i = 0; i < XDIM*YDIM; i++){
                hp_if >> hot_pixel_master_FGS2[i];
            }
            hp_if.close();

        }
	    else {
            std::cerr << "Can't find input file " << hp_buffer2 << std::endl;
            return -1;
        }

        cRootElement -> FirstChildElement("reset_noise") -> QueryDoubleText(&reset_noise);
        cRootElement -> FirstChildElement("circular_fov_radius") -> QueryDoubleText(&fov_radius);

        if(fov_radius > 0){
            generate_fov_mask(fov_mask_FGS1, fov_radius, ps_fgs1/1000, XDIM, YDIM);
            generate_fov_mask(fov_mask_FGS2, fov_radius, ps_fgs2/1000, XDIM, YDIM);
        }
        else{
            for(i = 0; i < XDIM*YDIM; i++){
                fov_mask_FGS1[i] = 1;
                fov_mask_FGS2[i] = 1;
            }
        }

        tinyxml2::XMLElement *cTracking = cRootElement -> FirstChildElement("Tracking");
        cTracking -> FirstChildElement("iterations") -> QueryUnsignedText(&iter_track);
        cTracking -> FirstChildElement("median_filter") -> QueryUnsignedText(&med_track);
        cTracking -> FirstChildElement("thresholding") -> QueryUnsignedText(&threshold_track);
        cTracking -> FirstChildElement("delay_mean") -> QueryDoubleText(&delay_mean_track);
        cTracking -> FirstChildElement("delay_std") -> QueryDoubleText(&delay_std_track);
        cTracking -> FirstChildElement("delay_max") -> QueryDoubleText(&delay_max_track);
        cTracking -> FirstChildElement("delay_min") -> QueryDoubleText(&delay_min_track);
        cTracking -> FirstChildElement("exposure_time") -> QueryDoubleText(&exp_track);
        cTracking -> FirstChildElement("tolerance") -> QueryUnsignedText(&tolerance_track);
        cTracking -> FirstChildElement("dim") -> QueryUnsignedText(&track_dim);
        //TDOD add delay jitter
        tinyxml2::XMLElement *cAcquisition = cRootElement -> FirstChildElement("Acquisition");
        cAcquisition -> FirstChildElement("iterations") -> QueryUnsignedText(&iter_acq);
        cAcquisition -> FirstChildElement("median_filter") -> QueryUnsignedText(&med_acq);
        cAcquisition -> FirstChildElement("thresholding") -> QueryUnsignedText(&threshold_acq);
        cAcquisition -> FirstChildElement("delay_mean") -> QueryDoubleText(&delay_mean_acq);
        cAcquisition -> FirstChildElement("delay_std") -> QueryDoubleText(&delay_std_acq);
        cAcquisition -> FirstChildElement("delay_max") -> QueryDoubleText(&delay_max_acq);
        cAcquisition -> FirstChildElement("delay_min") -> QueryDoubleText(&delay_min_acq);
        cAcquisition -> FirstChildElement("exposure_time") -> QueryDoubleText(&exp_acq);
        cAcquisition -> FirstChildElement("tolerance") -> QueryUnsignedText(&tolerance_acq);
        cAcquisition -> FirstChildElement("FGS1_dim") -> QueryUnsignedText(&acq_dim_fgs1);
        cAcquisition -> FirstChildElement("FGS2_dim") -> QueryUnsignedText(&acq_dim_fgs2);
        cAcquisition -> FirstChildElement("target_limit") -> QueryUnsignedText(&max_targets);
        cAcquisition -> FirstChildElement("extension_sigma") -> QueryUnsignedText(&extension_sigma);
        /*Parse and read stars from catalogue*/
        catalogue_buffer = cRootElement -> FirstChildElement("Star_catalogue") -> GetText();
        catalogue.assign(catalogue_buffer);

        search_star_id(targets, catalogue_buffer);

        cRootElement -> FirstChildElement("use_saved_files") -> QueryUnsignedText(&use_save);
        flat_buffer = cRootElement -> FirstChildElement("flat") -> GetText();
        smear_buffer = cRootElement -> FirstChildElement("smear") -> GetText();
        ramp_buffer = cRootElement -> FirstChildElement("ramp") -> GetText();
        img_buffer = cRootElement -> FirstChildElement("previous_image") -> GetText();
        queue_buffer = cRootElement -> FirstChildElement("centroid_queue") -> GetText();
        cRootElement -> FirstChildElement("smear_x") -> QueryDoubleText(&x_smear_0);
        cRootElement -> FirstChildElement("smear_y") -> QueryDoubleText(&y_smear_0);
        cRootElement -> FirstChildElement("trf_x") -> QueryIntText(&trf_x);
        cRootElement -> FirstChildElement("trf_y") -> QueryIntText(&trf_y);
        /*reset all image buffers*/
        if(hard_reset){
            reset_arrays(1);
        }
        else{
            transition_end = 0.0;
        }
            
        error.xErr = 0.;
        error.yErr = 0.;
       
        /*Set starting conditions for dim, qe, delay and exposure*/
        if(channel == 1){
            qe = qe_fgs1;
            focal_length = fl_fgs1;
            plate_scale = ps_fgs1;
	        offset[0] = offset_FGS1[0];
	        offset[1] = offset_FGS1[1];
	        fwhm_x = fwhm1_x;
	        fwhm_y = fwhm1_y;
        }
        else if(channel == 2){
            qe = qe_fgs2;
            focal_length = fl_fgs2;
            plate_scale = ps_fgs2;
	        offset[0] = offset_FGS2[0];
	        offset[1] = offset_FGS2[1];
	        fwhm_x = fwhm2_x;
	        fwhm_y = fwhm2_y;
        }

        if(strcmp(mode, MODE_ACQU) == 0){
            exposure_time = exp_acq;
            delay_mean = delay_mean_acq;
            delay_std = delay_std_acq;
            delay_max = delay_max_acq;
            delay_min = delay_min_acq;
            trf_x = 0;
            trf_y = 0;
            if(channel == 1){
                dim_x = acq_dim_fgs1;
                dim_y = acq_dim_fgs1;
            }
            else if(channel == 2){
                dim_x = acq_dim_fgs2;
                dim_y = acq_dim_fgs2;
            }   
        }
        else if(strcmp(mode, MODE_TRAC) == 0){
            exposure_time = exp_track;
            delay_mean = delay_mean_track;
            delay_std = delay_std_track;
            delay_max = delay_max_track;
            delay_min = delay_min_track;
            /*Tracking uses 64x64 for both channels*/
            dim_x = track_dim;
            dim_y = track_dim;
        }
        else{
            /*Initialize values for Tracking if Standby is configured as default*/
            exposure_time = exp_track;
            delay_mean = delay_mean_track;
            delay_std = delay_std_track;
            delay_max = delay_max_track;
            delay_min = delay_min_track;
            /*Tracking uses 64x64 for both channels*/
            dim_x = track_dim;
            dim_y = track_dim;
        }
        if(channel == 1){
            get_hp(hot_pixels, hot_pixel_master_FGS1, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
            get_calibration(reference_cal, reference_cal_master_FGS1, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
        }
        else{ 
            get_hp(hot_pixels, hot_pixel_master_FGS2, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
            get_calibration(reference_cal, reference_cal_master_FGS2, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
        }
        /*Create static attributes of the image*/
        if(hard_reset == 0){
            return 0;
        }
        else if(use_save == 0){
      	    generate_flat(flat, flat_mean, flat_sigma, XDIM, YDIM);
            reset_ramp();
        }
        else{
	        flat_if.open(flat_buffer, std::ifstream::in);
	        if (flat_if.is_open()){
                if(is_empty(flat_if)){
                    std::cerr << "The flat file is empty!" << std::endl;
                    return -1;
                }
                for (i = 0; i < XDIM*YDIM; i++){
                    flat_if >> flat[i];
                }
	        }
	        else {
                std::cerr << "Can't find input file " << flat_buffer << std::endl;
                return -1;
            }

            flat_if.close();
        
	        smear_if.open(smear_buffer, std::ifstream::in);
	        if (smear_if.is_open()){
                if(is_empty(smear_if)){
                    std::cerr << "The smear file is empty!" << std::endl;
                    return -1;
                }
                for (i = 0; i < XDIM_KERNEL*5*YDIM_KERNEL*5; i++){
                    smear_if >> smear_kernel_os[i];
                }
	        }
	        else {
                std::cerr << "Can't find input file " << smear_buffer << std::endl;
                return -1;
            }

            smear_if.close();

	        ramp_if.open(ramp_buffer, std::ifstream::in);
	        if (ramp_if.is_open()){
                if(is_empty(ramp_if)){
                    std::cerr << "The ramp file is empty!" << std::endl;
                    return -1;
                }
                for (i = 0; i < XDIM*YDIM; i++){
                    ramp_if >> image_ramp[i];
                }
	        }
	        else {
                std::cerr << "Can't find input file " << ramp_buffer << std::endl;
                return -1;
            }

            ramp_if.close();

	        img_if.open(img_buffer, std::ifstream::in);
	        if (img_if.is_open()){
                if(is_empty(img_if)){
                    std::cerr << "The previous image file is empty!" << std::endl;
                    return -1;
                }
                for (i = 0; i < XDIM*YDIM; i++){
                    img_if >> previous_image[i];
                }
	        }
	        else {
                std::cerr << "Can't find input file " << img_buffer << std::endl;
                return -1;
            }

            img_if.close();

            queue_if.open(queue_buffer, std::ifstream::in);
            if (queue_if.is_open()){ 
                while(queue_if >> centroid_buffer.x >> centroid_buffer.y >> centroid_buffer.time >> centroid_buffer.integration_start >> centroid_buffer.validity.flag >> centroid_buffer.validity.index >> centroid_buffer.validity.magnitude){
                    centroid_queue.push_front(centroid_buffer);
                }
            }
            
            queue_if.close();
        }
    }
    else{
        std::cerr << "The config file appeares to be empty!" << std::endl;
        return -1;
    }
    return 0;
}


double angular_rate_drf[3];

/**
 * @brief Creates a smearing kernel based on the given angular rate and time
 * step
 *
 * The function adds a linear smear to the smearing kernel. The position and
 * lenght of this smear is defined by the angular rate, the sampling time and
 * the end of the previous smear. The smearing kernel is reset on the calculation
 * of a centroid.

 * @param angular_rate: array containg the current angular rate
 * @param dt: time step used for angle determination
 *
 * @returns 0 on success
 */

int FGS :: generate_smear(double (&angular_rate)[3], double dt)
{
    double unit_vec[3] = {0., 0., 1.};
    double ar[3];
    double x_smear, y_smear;
    unsigned int i;
    int status;

    for (i = 0; i < 3; i++){
        ar[i] = angular_rate[i]/3600; /*convert to degrees/s*/
    }

    cross3(ar, unit_vec, angular_rate_drf);

    x_smear = - angular_rate_drf[0]*dt*3600000/plate_scale;
    y_smear = - angular_rate_drf[1]*dt*3600000/plate_scale;

    status = generate_linear_smearing_kernel(smear_kernel_os, x_smear_0 * 5, y_smear_0 * 5, x_smear * 5, y_smear * 5, XDIM_KERNEL*5);

    x_smear_0 += x_smear;
    y_smear_0 += y_smear;
    
    if(status == 1){
      std::cerr << "WARNING: angular rate exeeds smearing kernel borders at t = " << sim_time << "s!\n";
    }

  return 0;
}

/**
 * @brief Creates image based on current status
 *
 * The function creates an image containg detector features and the smeared
 * stars based on the configured parameters at the given time stamp
 *
 */

int FGS :: generate_image()
{
    unsigned int i;
    sat_counter = 0;

    shift_smear(smear_kernel_os, x_smear_0*5, y_smear_0*5, XDIM_KERNEL*5);
 
    downsample_image(smear_kernel_os, smear_kernel, XDIM_KERNEL*5, YDIM_KERNEL*5, 5);
    
    generate_bias(bias, bias_sample, 0, 128, dim_x, dim_y);

    generate_dark(dark, dark_mean, exposure_time, dim_x, dim_y);

    generate_background(bkgr, bkgr_noise, qe, exposure_time, dim_x, dim_y);

    generate_starmask(starmask, *targets, exposure_time, dim_x, dim_y, channel);

    smear_star_image(smeared_mask, starmask, smear_kernel, dim_x, dim_y, XDIM_KERNEL, YDIM_KERNEL);
    
    if(channel == 1){
        generate_star_image(starimage, FGS1_Psf, smeared_mask, dim_x, dim_y, XDIM_PSF, XDIM_PSF);
    }
    else if(channel == 2){
        generate_star_image(starimage, FGS2_Psf, smeared_mask, dim_x, dim_y, XDIM_PSF, XDIM_PSF);
    }
    else{
        std::cerr << "Unknown channel configured!" << std::endl;
    }
    
    for(i = 0; i < dim_x*dim_y; i++)
    {   
        starimage[i] = starimage[i] + bkgr[i]; 
    }
    generate_shot(starimage, shot, dim_x, dim_y);

    if(channel == 1){
        apply_fov_mask(shot, fov_mask_FGS1, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
    }
    else if(channel == 2){
        apply_fov_mask(shot, fov_mask_FGS2, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
    }

    for(i = 0; i < dim_x*dim_y; i++){
        image_ramp[i] = image_ramp[i] + (unsigned int) ((shot[i] * flat[i] * hot_pixels[i] + dark[i]) * gain);
        tmp_image[i] = image_ramp[i] + bias[i] * gain;
        if (tmp_image[i] > 65535){
            tmp_image[i] = 65535;
            sat_counter = sat_counter + 1;
        }
        if(previous_image[i] > tmp_image[i]){
            image[i] = 0;
            previous_image[i] = tmp_image[i];
        }
        else{
            image[i] = tmp_image[i] - previous_image[i];
            previous_image[i] = tmp_image[i];
        }
    }
    return 0;
}

/**
 * @brief applies the FGS algorithms on the current image
 *
 * Function that applies the specified FGS algorithm on the current image buffer
 * of the FGS class.
 *
 * @param measurement
 *
 */

struct coord FGS :: perform_algorithm()
{
    unsigned int min = 0;
    struct coord output;
    unsigned int i;
    output.x = 0.;
    output.y = 0.;

    hotPixelCorrection(image, reference_cal, dim_x, dim_y, 0xFFFF);

    if(strcmp(mode, MODE_ACQU) == 0){
        if (threshold_acq == 1){
            min = GetMin(image, dim_x*dim_y);
            for(i = 0; i < dim_x*dim_y; i++){
                image[i] = image[i] - min;
            }
        }
        else if(threshold_acq == 2){
            std_threshold(image, dim_x, dim_y);
        }
        if (med_acq == 1){
            MedFilter3x3(image, dim_x, dim_y, med_threshold, filtered_image);
            output = SourceDetection(filtered_image, dim_x, dim_y, channel, max_targets, target_signal*exposure_time, tolerance_acq, extension_sigma, iter_acq, fwhm_x, fwhm_y, target_signal*(lower_sig_lim/100)*exposure_time, pearson_limit);
        }
        else{
            output = SourceDetection(image, dim_x, dim_y, channel, max_targets, target_signal*exposure_time, tolerance_acq, extension_sigma, iter_acq, fwhm_x, fwhm_y, target_signal*(lower_sig_lim/100)*exposure_time, pearson_limit);
        }
    }
    else if (strcmp(mode, MODE_TRAC) == 0){
        if (threshold_track == 1){
            min = GetMin(image, dim_x*dim_y);
            for(i = 0; i < dim_x*dim_y; i++){
                image[i] = image[i] - min;
            }
        }
        else if(threshold_track == 2){
            std_threshold(image, dim_x, dim_y);
        }  

        if(med_track == 1){
            MedFilter3x3(image, dim_x, dim_y, med_threshold, filtered_image);
            output = ArielCoG(filtered_image, dim_x, dim_y, iter_track, fwhm_x, fwhm_y, channel, target_signal*(lower_sig_lim/100)*exposure_time, pearson_limit, tolerance_track, target_signal*exposure_time, 0);
        }
        else{
            output = ArielCoG(image, dim_x, dim_y, iter_track, fwhm_x, fwhm_y, channel, target_signal*(lower_sig_lim/100)*exposure_time, pearson_limit, tolerance_track, target_signal*exposure_time, 0);
        }
    }

    return output;

}

/**
 * @brief Setter of the channel attribute
 *
 * @param new_channel: FGS channel to be used, either 1 or 2
 */

int FGS :: set_channel(unsigned int new_channel)
{
    channel = new_channel;
    /*Set quantum efficiency*/
    if(channel == 1){
        qe = qe_fgs1;
        plate_scale = ps_fgs1;
        focal_length = fl_fgs1;
        fwhm_x = fwhm1_x;
        fwhm_y = fwhm1_y;
        offset[0] = offset_FGS1[0];
        offset[1] = offset_FGS1[1];

    }
    else if(channel == 2){
        qe = qe_fgs2;
        plate_scale = ps_fgs2;
        focal_length = fl_fgs2;
        fwhm_x = fwhm2_x;
        fwhm_y = fwhm2_y;
        offset[0] = offset_FGS2[0];
        offset[1] = offset_FGS2[1];
    }

    /*set dimensions for Acquisition, 22 arcsec*/
    if(strcmp(mode, MODE_ACQU) == 0){
        if(channel == 1){
            dim_x = acq_dim_fgs1;
            dim_y = acq_dim_fgs1;
        }
        else if(channel == 2){
            dim_x = acq_dim_fgs2;
            dim_y = acq_dim_fgs2;
        }
    }
    reset_ramp();
    calculate_ramp_length();
    if(channel == 1){
        get_hp(hot_pixels, hot_pixel_master_FGS1, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
        get_calibration(reference_cal, reference_cal_master_FGS1, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
    }
    else{
        get_hp(hot_pixels, hot_pixel_master_FGS2, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
        get_calibration(reference_cal, reference_cal_master_FGS2, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
    }
    return 0;
}

/**
 * @brief Setter for the operational mode attribute
 *
 * @param new_mode: new mode for the simulation. Possible entries:"Acquisition",
 * "Tracking", "Standby"
 */

int FGS :: set_mode(const char* new_mode)
{
    mode = new_mode;
    /*Set image dimensiomean,ns based on FoV*/
    /* Target Acquisition, 22 arcsec */
    if(strcmp(mode, MODE_ACQU) == 0){
        exposure_time = exp_acq;
        delay_mean = delay_mean_acq;
        delay_std = delay_std_acq;
        delay_max = delay_max_acq;
        delay_min = delay_min_acq;
        trf_x = 0;
        trf_y = 0;
        if(channel == 1){
            dim_x = acq_dim_fgs1;
            dim_y = acq_dim_fgs1;
        }
        else if(channel == 2){
            dim_x = acq_dim_fgs2;
            dim_y = acq_dim_fgs2;
        }
    }
    else if(strcmp(mode, MODE_TRAC) == 0){
        exposure_time = exp_track;
        delay_mean = delay_mean_track;
        delay_std = delay_std_track;
        delay_max = delay_max_track;
        delay_min = delay_min_track;
        /*Tracking uses 64x64 for both channels*/
        dim_x = track_dim;
        dim_y = track_dim;
    }
    reset_ramp();
    calculate_ramp_length();
    if(channel == 1){
        get_hp(hot_pixels, hot_pixel_master_FGS1, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
        get_calibration(reference_cal, reference_cal_master_FGS1, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
    }
    else{
        get_hp(hot_pixels, hot_pixel_master_FGS2, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
        get_calibration(reference_cal, reference_cal_master_FGS2, dim_x, dim_y, XDIM, YDIM, trf_x, trf_y);
    }
    return 0;
}

/**
 * @brief Reset for all image buffers
 */

int FGS :: reset_arrays(int reset_all)
{
    unsigned int i;
    //random_normal_trm(bias_sample, 0, read_noise, 128);
    //#pragma omp parallel for
    for(i = 0; i < XDIM_KERNEL*5*YDIM_KERNEL*5; i++){
        smear_kernel_os[i] = 0.;
        if(i < XDIM*YDIM){
            bias[i] = 0.;
            dark[i] = 0.;
            shot[i] = 0.;
            bkgr[i] = 0.;
            starmask[i] = 0.;
            starimage[i] = 0.;
            smeared_mask[i] = 0.;
            image[i] = 0;
            filtered_image[i] = 0;
            if(reset_all != 0){
                flat[i] = 0.;
                hot_pixels[i] = 0.;
                reference_cal[i] = 0;
            }
        }
        if(i < XDIM_KERNEL*YDIM_KERNEL){
            smear_kernel[i] = 0.;
        }
    } 
    return 0;
}

char flat_save[128];
char smear_save[128];
char ramp_save[128];
char img_save[128];
char queue_save[128];

/**
 * @brief Creates an xml file based on the current HFS state
 *
 * The function creates an xml output file that stores all parameters of the HFS
 * so that the current state can be reloaded later
 *
 * @param output_file: name of the target output xml-file
 *
 * @return 0 on success
 */

int FGS :: generate_config(const char* output_file)
{

    std::ofstream flat_of, smear_of, ramp_of, img_of, queue_of;
    unsigned int i;
    std::deque<struct coord> centroid_queue_copy = centroid_queue;


     
    tinyxml2::XMLDocument config;
 
    tinyxml2::XMLElement* cRoot = config.NewElement("HFS_MetaInfo");
    config.InsertFirstChild(cRoot);

    tinyxml2::XMLElement* cTime = config.NewElement("simulation_time");
    cTime->SetText(sim_time);
    cRoot->InsertEndChild(cTime);

    tinyxml2::XMLElement* cIntTime = config.NewElement("internal_time");
    cIntTime->SetText(internal_time);
    cRoot->InsertEndChild(cIntTime);

    tinyxml2::XMLElement* cTimeStep = config.NewElement("timestep");
    cTimeStep->SetText(timestep);
    cRoot->InsertEndChild(cTimeStep);
   
    tinyxml2::XMLElement* cRandSeed = config.NewElement("random_seed");
    cRandSeed->SetText(rand_seed);
    cRoot->InsertEndChild(cRandSeed);

    tinyxml2::XMLElement* cExpStart = config.NewElement("integration_start");
    cExpStart->SetText(start_exposure);
    cRoot->InsertEndChild(cExpStart);

    tinyxml2::XMLElement* cFlatMean = config.NewElement("flat_mean");
    cFlatMean->SetText(flat_mean);
    cRoot->InsertEndChild(cFlatMean);

    tinyxml2::XMLElement* cFlatSigma = config.NewElement("flat_sigma");
    cFlatSigma->SetText(flat_sigma);
    cRoot->InsertEndChild(cFlatSigma);

    tinyxml2::XMLElement* cBias = config.NewElement("bias");
    cBias->SetText(bias_value);
    cRoot->InsertEndChild(cBias);

    tinyxml2::XMLElement* cBkgrNoise = config.NewElement("bkgr_noise");
    cBkgrNoise->SetText(bkgr_noise);
    cRoot->InsertEndChild(cBkgrNoise);

    tinyxml2::XMLElement* cRnNoise = config.NewElement("readout_noise");
    cRnNoise->SetText(read_noise);
    cRoot->InsertEndChild(cRnNoise);

    tinyxml2::XMLElement* cDark = config.NewElement("dark_average");
    cDark->SetText(dark_mean);
    cRoot->InsertEndChild(cDark);

    tinyxml2::XMLElement* cGain = config.NewElement("gain");
    cGain->SetText(gain);
    cRoot->InsertEndChild(cGain);

    tinyxml2::XMLElement* cFWCap = config.NewElement("full_well_capacity");
    cFWCap->SetText(full_well_cap);
    cRoot->InsertEndChild(cFWCap);

    tinyxml2::XMLElement* cMedThresh = config.NewElement("median_filter_threshold");
    cMedThresh->SetText(med_threshold);
    cRoot->InsertEndChild(cMedThresh);

    tinyxml2::XMLElement* cJitter = config.NewElement("jitter_error_std");
    cJitter->SetText(jitter_error);
    cRoot->InsertEndChild(cJitter);
    
    tinyxml2::XMLElement* cResetDuration = config.NewElement("reset_duration");
    cResetDuration->SetText(reset_duration);
    cRoot->InsertEndChild(cResetDuration);
    
    tinyxml2::XMLElement* cResetState = config.NewElement("reset_end");
    cResetState->SetText(reset_end);
    cRoot->InsertEndChild(cResetState);

    tinyxml2::XMLElement* cTimingTol = config.NewElement("timing_tolerance");
    cTimingTol->SetText(timing_tolerance);
    cRoot->InsertEndChild(cTimingTol);
    
    tinyxml2::XMLElement* cTimingJit = config.NewElement("timing_jitter");
    cTimingJit->SetText(timing_jitter);
    cRoot->InsertEndChild(cTimingJit);

    tinyxml2::XMLElement* cTimingDrft = config.NewElement("timing_drift");
    cTimingDrft->SetText(timing_drift);
    cRoot->InsertEndChild(cTimingDrft);

    tinyxml2::XMLElement* cFGSPSF1 = config.NewElement("FGS1_Psf");
    cFGSPSF1->SetText(psf_fgs1.c_str());
    cRoot->InsertEndChild(cFGSPSF1);

    tinyxml2::XMLElement* cFGSPSF2 = config.NewElement("FGS2_Psf");
    cFGSPSF2->SetText(psf_fgs2.c_str());
    cRoot->InsertEndChild(cFGSPSF2);

    tinyxml2::XMLElement* cFGSPS1 = config.NewElement("FGS1_PS");
    cFGSPS1->SetText(ps_fgs1);
    cRoot->InsertEndChild(cFGSPS1);

    tinyxml2::XMLElement* cFGSPS2 = config.NewElement("FGS2_PS");
    cFGSPS2->SetText(ps_fgs2);
    cRoot->InsertEndChild(cFGSPS2);

    tinyxml2::XMLElement* cFGSFL1 = config.NewElement("FGS1_FL");
    cFGSFL1->SetText(fl_fgs1);
    cRoot->InsertEndChild(cFGSFL1);

    tinyxml2::XMLElement* cFGSFL2 = config.NewElement("FGS2_FL");
    cFGSFL2->SetText(fl_fgs2);
    cRoot->InsertEndChild(cFGSFL2);

    tinyxml2::XMLElement* cFGSQE1 = config.NewElement("FGS1_QE");
    cFGSQE1->SetText(qe_fgs1);
    cRoot->InsertEndChild(cFGSQE1);

    tinyxml2::XMLElement* cFGSQE2 = config.NewElement("FGS2_QE");
    cFGSQE2->SetText(qe_fgs2);
    cRoot->InsertEndChild(cFGSQE2);

    tinyxml2::XMLElement* cOffset1X = config.NewElement("FGS1_Offset_X");
    cOffset1X->SetText(offset_FGS1[0]);
    cRoot->InsertEndChild(cOffset1X);

    tinyxml2::XMLElement* cOffset1Y = config.NewElement("FGS1_Offset_Y");
    cOffset1Y->SetText(offset_FGS1[1]);
    cRoot->InsertEndChild(cOffset1Y);

    tinyxml2::XMLElement* cOffset2X = config.NewElement("FGS2_Offset_X");
    cOffset2X->SetText(offset_FGS2[0]);
    cRoot->InsertEndChild(cOffset2X);

    tinyxml2::XMLElement* cOffset2Y = config.NewElement("FGS2_Offset_Y");
    cOffset2Y->SetText(offset_FGS2[1]);
    cRoot->InsertEndChild(cOffset2Y);
    
    tinyxml2::XMLElement* cFGSChannel = config.NewElement("FGS_channel");
    cFGSChannel->SetText(channel);
    cRoot->InsertEndChild(cFGSChannel);

    tinyxml2::XMLElement* cFGSMode = config.NewElement("FGS_mode");
    cFGSMode->SetText(mode);
    cRoot->InsertEndChild(cFGSMode);

    tinyxml2::XMLElement* cFWHM1X = config.NewElement("FWHM_FGS1_x");
    cFWHM1X->SetText(fwhm1_x);
    cRoot->InsertEndChild(cFWHM1X);

    tinyxml2::XMLElement* cFWHM1Y = config.NewElement("FWHM_FGS1_y");
    cFWHM1Y->SetText(fwhm1_y);
    cRoot->InsertEndChild(cFWHM1Y);

    tinyxml2::XMLElement* cFWHM2X = config.NewElement("FWHM_FGS2_x");
    cFWHM2X->SetText(fwhm2_x);
    cRoot->InsertEndChild(cFWHM2X);

    tinyxml2::XMLElement* cFWHM2Y = config.NewElement("FWHM_FGS2_y");
    cFWHM2Y->SetText(fwhm2_y);
    cRoot->InsertEndChild(cFWHM2Y);

    tinyxml2::XMLElement* cTransDelayChan = config.NewElement("transition_delay_channel");
    cTransDelayChan->SetText(transition_delay_channel);
    cRoot->InsertEndChild(cTransDelayChan);

    tinyxml2::XMLElement* cTransDelayMode = config.NewElement("transition_delay_mode");
    cTransDelayMode->SetText(transition_delay_mode);
    cRoot->InsertEndChild(cTransDelayMode);

    tinyxml2::XMLElement* cTransEnd = config.NewElement("transition_end");
    cTransEnd->SetText(transition_end);
    cRoot->InsertEndChild(cTransEnd);

    tinyxml2::XMLElement* cTargetSig = config.NewElement("target_signal");
    cTargetSig->SetText(target_signal);
    cRoot->InsertEndChild(cTargetSig);

    tinyxml2::XMLElement* cLowSigLim = config.NewElement("lower_signal_limit");
    cLowSigLim->SetText(lower_sig_lim);
    cRoot->InsertEndChild(cLowSigLim);

    tinyxml2::XMLElement* cPearson = config.NewElement("validation_pearson_limit");
    cPearson->SetText(pearson_limit);
    cRoot->InsertEndChild(cPearson);

    tinyxml2::XMLElement* cFailed = config.NewElement("failed_status");
    cFailed->SetText(failed);
    cRoot->InsertEndChild(cFailed);

    tinyxml2::XMLElement* cRampLen = config.NewElement("ramp_length");
    cRampLen->SetText(ramp_length);
    cRoot->InsertEndChild(cRampLen);

    tinyxml2::XMLElement* cRampCnt = config.NewElement("ramp_counter");
    cRampCnt->SetText(ramp_counter);
    cRoot->InsertEndChild(cRampCnt);

    tinyxml2::XMLElement* cRampMax = config.NewElement("max_ramp");
    cRampMax->SetText(max_ramp);
    cRoot->InsertEndChild(cRampMax);

    tinyxml2::XMLElement* cSatLim = config.NewElement("saturation_limit");
    cSatLim->SetText(sat_limit);
    cRoot->InsertEndChild(cSatLim);

    tinyxml2::XMLElement* cResetCal1 = config.NewElement("reset_calibration_FGS1");
    cResetCal1->SetText(reference_data_fgs1.c_str());
    cRoot->InsertEndChild(cResetCal1);

    tinyxml2::XMLElement* cResetCal2 = config.NewElement("reset_calibration_FGS2");
    cResetCal2->SetText(reference_data_fgs2.c_str());
    cRoot->InsertEndChild(cResetCal2);

    tinyxml2::XMLElement* cHpMap1 = config.NewElement("hp_map_FGS1");
    cHpMap1->SetText(hp_map_fgs1.c_str());
    cRoot->InsertEndChild(cHpMap1);

    tinyxml2::XMLElement* cHpMap2 = config.NewElement("hp_map_FGS2");
    cHpMap2->SetText(hp_map_fgs2.c_str());
    cRoot->InsertEndChild(cHpMap2);

    tinyxml2::XMLElement* cResetNoise = config.NewElement("reset_noise");
    cResetNoise->SetText(reset_noise);
    cRoot->InsertEndChild(cResetNoise);

    tinyxml2::XMLElement* cCircularFov = config.NewElement("circular_fov_radius");
    cCircularFov->SetText(fov_radius);
    cRoot->InsertEndChild(cCircularFov);

    tinyxml2::XMLElement* cTracking = config.NewElement("Tracking");
    cRoot->InsertEndChild(cTracking);

    tinyxml2::XMLElement* cTrackIter = config.NewElement("iterations");
    cTrackIter->SetText(iter_track);
    cTracking->InsertEndChild(cTrackIter);

    tinyxml2::XMLElement* cTrackMed = config.NewElement("median_filter");
    cTrackMed->SetText(med_track);
    cTracking->InsertEndChild(cTrackMed);

    tinyxml2::XMLElement* cTrackThresh = config.NewElement("thresholding");
    cTrackThresh->SetText(threshold_track);
    cTracking->InsertEndChild(cTrackThresh);

    tinyxml2::XMLElement* cTrackDelay = config.NewElement("delay_mean");
    cTrackDelay->SetText(delay_mean_track);
    cTracking->InsertEndChild(cTrackDelay);

    tinyxml2::XMLElement* cTrackDelayStd = config.NewElement("delay_std");
    cTrackDelayStd->SetText(delay_std_track);
    cTracking->InsertEndChild(cTrackDelayStd);

    tinyxml2::XMLElement* cTrackDelayMax = config.NewElement("delay_max");
    cTrackDelayMax->SetText(delay_max_track);
    cTracking->InsertEndChild(cTrackDelayMax);

    tinyxml2::XMLElement* cTrackDelayMin = config.NewElement("delay_min");
    cTrackDelayMin->SetText(delay_min_track);
    cTracking->InsertEndChild(cTrackDelayMin);

    tinyxml2::XMLElement* cTrackExp = config.NewElement("exposure_time");
    cTrackExp->SetText(exp_track);
    cTracking->InsertEndChild(cTrackExp);

    tinyxml2::XMLElement* cTrackTol = config.NewElement("tolerance");
    cTrackTol->SetText(tolerance_track);
    cTracking->InsertEndChild(cTrackTol);
    
    tinyxml2::XMLElement* cTrackDim = config.NewElement("dim");
    cTrackDim->SetText(track_dim);
    cTracking->InsertEndChild(cTrackDim);

    tinyxml2::XMLElement* cAcquisition = config.NewElement("Acquisition");
    cRoot->InsertEndChild(cAcquisition);

    tinyxml2::XMLElement* cAcqIter = config.NewElement("iterations");
    cAcqIter->SetText(iter_acq);
    cAcquisition->InsertEndChild(cAcqIter);

    tinyxml2::XMLElement* cAcqMed = config.NewElement("median_filter");
    cAcqMed->SetText(med_acq);
    cAcquisition->InsertEndChild(cAcqMed);

    tinyxml2::XMLElement* cAcqThresh = config.NewElement("thresholding");
    cAcqThresh->SetText(threshold_acq);
    cAcquisition->InsertEndChild(cAcqThresh);

    tinyxml2::XMLElement* cAcqDelay = config.NewElement("delay_mean");
    cAcqDelay->SetText(delay_mean_acq);
    cAcquisition->InsertEndChild(cAcqDelay);
    
    tinyxml2::XMLElement* cAcqDelayStd = config.NewElement("delay_std");
    cAcqDelayStd->SetText(delay_std_acq);
    cAcquisition->InsertEndChild(cAcqDelayStd);

    tinyxml2::XMLElement* cAcqDelayMax = config.NewElement("delay_max");
    cAcqDelayMax->SetText(delay_max_acq);
    cAcquisition->InsertEndChild(cAcqDelayMax);

    tinyxml2::XMLElement* cAcqDelayMin = config.NewElement("delay_min");
    cAcqDelayMin->SetText(delay_min_acq);
    cAcquisition->InsertEndChild(cAcqDelayMin);

    tinyxml2::XMLElement* cAcqExp = config.NewElement("exposure_time");
    cAcqExp->SetText(exp_acq);
    cAcquisition->InsertEndChild(cAcqExp);

    tinyxml2::XMLElement* cAcqTol = config.NewElement("tolerance");
    cAcqTol->SetText(tolerance_acq);
    cAcquisition->InsertEndChild(cAcqTol);

    tinyxml2::XMLElement* cAcqDim1 = config.NewElement("FGS1_dim");
    cAcqDim1->SetText(acq_dim_fgs1);
    cAcquisition->InsertEndChild(cAcqDim1);

    tinyxml2::XMLElement* cAcqDim2 = config.NewElement("FGS2_dim");
    cAcqDim2->SetText(acq_dim_fgs2);
    cAcquisition->InsertEndChild(cAcqDim2);

    tinyxml2::XMLElement* cAcqTargetLim = config.NewElement("target_limit");
    cAcqTargetLim->SetText(max_targets);
    cAcquisition->InsertEndChild(cAcqTargetLim);

    tinyxml2::XMLElement* cAcqExtSig = config.NewElement("extension_sigma");
    cAcqExtSig->SetText(extension_sigma);
    cAcquisition->InsertEndChild(cAcqExtSig);

    tinyxml2::XMLElement* ccatalogue = config.NewElement("Star_catalogue");
    ccatalogue->SetText(catalogue.c_str());
    cRoot->InsertEndChild(ccatalogue);

    tinyxml2::XMLElement* cSave = config.NewElement("use_saved_files");
    cSave->SetText(1);
    cRoot->InsertEndChild(cSave);
 
    snprintf(flat_save, sizeof(flat_save), "./Flat_%.0f.txt", sim_time); 
    
    snprintf(smear_save, sizeof(smear_save), "./Smear_%.0f.txt", sim_time);

    snprintf(ramp_save, sizeof(ramp_save), "./Ramp_input_%.0f.txt", sim_time);

    snprintf(img_save, sizeof(img_save), "./Previous_image_%.0f.txt", sim_time);

    snprintf(queue_save, sizeof(queue_save), "./Centroid_queue_%.0f.txt", sim_time);

    tinyxml2::XMLElement* cFlat = config.NewElement("flat");
    cFlat->SetText(flat_save);
    cRoot->InsertEndChild(cFlat);

    tinyxml2::XMLElement* cSmear = config.NewElement("smear");
    cSmear->SetText(smear_save);
    cRoot->InsertEndChild(cSmear);

    tinyxml2::XMLElement* cRamp = config.NewElement("ramp");
    cRamp->SetText(ramp_save);
    cRoot->InsertEndChild(cRamp);

    tinyxml2::XMLElement* cImage = config.NewElement("previous_image");
    cImage->SetText(img_save);
    cRoot->InsertEndChild(cImage);

    tinyxml2::XMLElement* cQueue = config.NewElement("centroid_queue");
    cQueue->SetText(queue_save);
    cRoot->InsertEndChild(cQueue);

    tinyxml2::XMLElement* cSmearX = config.NewElement("smear_x");
    cSmearX->SetText(x_smear_0);
    cRoot->InsertEndChild(cSmearX);

    tinyxml2::XMLElement* cSmearY = config.NewElement("smear_y");
    cSmearY->SetText(y_smear_0);
    cRoot->InsertEndChild(cSmearY);

    tinyxml2::XMLElement* cTRFX = config.NewElement("trf_x");
    cTRFX->SetText(trf_x);
    cRoot->InsertEndChild(cTRFX);

    tinyxml2::XMLElement* cTRFY = config.NewElement("trf_y");
    cTRFY->SetText(trf_y);
    cRoot->InsertEndChild(cTRFY);

    config.SaveFile(output_file);
    /*Write output files*/
    
    flat_of.open(flat_save, std::ofstream::out);
    for (i = 0; i < XDIM*YDIM; i++){
        flat_of << std::setprecision(std::numeric_limits<double>::max_digits10) << flat[i] << "\n";

    }
    flat_of.close();
 
    smear_of.open(smear_save, std::ofstream::out);
    for (i = 0; i < XDIM_KERNEL*5*YDIM_KERNEL*5; i++){
        smear_of << std::setprecision(std::numeric_limits<double>::max_digits10) << smear_kernel_os[i] << "\n";
    }
    smear_of.close();

    ramp_of.open(ramp_save, std::ofstream::out);
    for (i = 0; i < XDIM*YDIM; i++){
        ramp_of << std::setprecision(std::numeric_limits<double>::max_digits10) << image_ramp[i] << "\n";
    }
    ramp_of.close();

    img_of.open(img_save, std::ofstream::out);
    for (i = 0; i < XDIM*YDIM; i++){
        img_of << std::setprecision(std::numeric_limits<double>::max_digits10) << previous_image[i] << "\n";
    }
    img_of.close();

    queue_of.open(queue_save, std::ofstream::out);
    while(!centroid_queue_copy.empty()){
        queue_of << centroid_queue_copy.front().x << " " << centroid_queue_copy.front().y << " " << centroid_queue_copy.front().time << " " << centroid_queue_copy.front().integration_start << " " << centroid_queue_copy.front().validity.flag << " " << centroid_queue_copy.front().validity.index << " " << centroid_queue_copy.front().validity.magnitude << "\n";
        centroid_queue_copy.pop_front();
    }
    queue_of.close();

    return 0;
}

/**
 * @brief Getter for the HFS state, used for debugging and tests
 *
 * @return hfs_state struct containing all parameters
 */

struct hfs_state FGS :: get_hfs_state()
{
    struct hfs_state output;

    output.bias_value = bias_value;
    output.qe = qe;
    output.read_noise = read_noise;
    output.dark_mean = dark_mean;
    output.bkgr_noise = bkgr_noise;
    output.gain = gain;
    output.exposure_time = exposure_time;
    output.sim_time = sim_time;
    output.internal_time = internal_time;
    output.timestep = timestep;
    output.flat_mean = flat_mean;
    output.flat_sigma = flat_sigma;
    output.plate_scale = plate_scale;
    output.pearson_limit = pearson_limit;
    output.ps_fgs1 = ps_fgs1;
    output.ps_fgs2 = ps_fgs2;
    output.fl_fgs1 = fl_fgs1;
    output.fl_fgs2 = fl_fgs2;
    output.qe_fgs1 = qe_fgs1;
    output.qe_fgs2 = qe_fgs2;
    output.lower_sig_lim = lower_sig_lim;
    output.dim_x = dim_x;
    output.dim_y = dim_y;
    output.start_exposure = start_exposure;
    output.focal_length = focal_length;
    output.delay = delay_mean;
    output.x_smear_0 = x_smear_0;
    output.y_smear_0 = y_smear_0;
    output.jitter_error = jitter_error;
    output.delay_track = delay_mean_track;
    output.delay_acq = delay_mean_acq;
    output.transition_delay_channel = transition_delay_channel;
    output.transition_delay_mode = transition_delay_mode;
    output.transition_end = transition_end;
    output.exp_track = exp_track;
    output.exp_acq = exp_acq;
    output.reset_duration = reset_duration;
    output.reset_end = reset_end;
    output.timing_tolerance = timing_tolerance;
    output.channel = channel;
    output.iter_track = iter_track;
    output.iter_acq = iter_acq;
    output.med_track = med_track;
    output.med_acq = med_acq;
    output.threshold_track = threshold_track;
    output.threshold_acq = threshold_acq;
    output.tolerance_track = tolerance_track;
    output.tolerance_acq = tolerance_acq;
    if(strcmp(mode, MODE_TRAC) == 0){
        output.mode = 2;
    }
    else if(strcmp(mode, MODE_ACQU) == 0){
        output.mode = 1;
    }
    else{
        output.mode = 0;
    }
    output.targets = targets;
    output.target_pos_x = trf_x;
    output.target_pos_y = trf_y;
    output.target_signal = target_signal;
    output.full_well_cap = full_well_cap;
    output.med_threshold = med_threshold;
    output.sync_ctr = sync_ctr;
    output.track_dim = track_dim;
    output.acq_dim_fgs1 = acq_dim_fgs1;
    output.acq_dim_fgs2 = acq_dim_fgs2;
    output.rand_seed = rand_seed;
    output.fwhm1_x = fwhm1_x;
    output.fwhm1_y = fwhm1_y;
    output.fwhm2_x = fwhm2_x;
    output.fwhm2_y = fwhm2_y;
    output.fwhm_x = fwhm_x;
    output.fwhm_y = fwhm_y;
    output.max_targets = max_targets;
    output.extension_sigma = extension_sigma;
    output.ramp_length = ramp_length;
    output.ramp_counter = ramp_counter;
    output.sat_limit = sat_limit;
    output.sat_counter = sat_counter;
    std::copy(offset_FGS1, offset_FGS1+2, output.offset_FGS1);
    std::copy(offset_FGS2, offset_FGS2+2, output.offset_FGS2);
    std::copy(offset, offset+2, output.offset);
    output.psf_fgs1 = psf_fgs1;
    output.psf_fgs2 = psf_fgs2;
    output.input_file = input_file;
    output.catalogue = catalogue;
    output.targets = targets;
    return output;
}

/**
 * @brief transforms vector from spacecraft reference frame to detector
 * reference frame
 *
 * The function takes an input vector and applies the SC-FBT and FBT-DRF
 * rotation matrices. The ouput is only set, if the resulting vector is inside
 * of the field of view of the detector given by the plate scale and the RoI
 * dimensions.
 *
 * @param vec: input vector
 * @param[out] output: output vector
 *
 * @return 0 on success, 1 if the result is outside of the FOV
 */

int FGS :: transform_to_detector(double (&vec)[3], double (&velVec)[3], double* output)
{
    double x_pos, y_pos;

    apply_aberration(vec, velVec);

    if(strcmp(mode, MODE_TRAC) == 0){
        
        x_pos = (vec[0]*(360/TWO_PI)*3600000 + offset[0])/plate_scale - trf_x;
        y_pos = (vec[1]*(360/TWO_PI)*3600000 + offset[1])/plate_scale - trf_y;
    }
    else{
        x_pos = (vec[0]*(360/TWO_PI)*3600000 + offset[0])/plate_scale;
        y_pos = (vec[1]*(360/TWO_PI)*3600000 + offset[1])/plate_scale;
    }
    if(abs(x_pos) < (dim_x/2)){
        if(abs(y_pos) < (dim_y/2)){
            output[0] = x_pos;
            output[1] = y_pos;
            return 0;
        }
    }
    else{
        return -1;
    }
    return -1;
}

double star_quat[4];
double tmp_quat[4];
double sc_quat[4];
double sc_vec[3];
double res[2];

/**
 * @brief transforms star vector from J2000 to DRF
 *
 * The function takes a quaternion and applies both it and the SC-DRF
 * rotation matrices to obtain the vector in DRF. If the resulting vector is
 * inside the FOV, the "visible" flag is set for the star and the target
 * coordinates are set
 *
 * @param quaternion: quaternion for trandformation (in J2000 to SC)
 * @param trf_x: Position of the Tracking reference frame in px
 * @param trf_y: Position of the Tracking reference frame in px
 *
 * @return 0 on success
 */

int FGS :: transform_star_coordinates(double (&quaternion)[4], double (&velocity)[3])
{
    unsigned int i, flag;
    double quaternion_inv[4] = {quaternion[0], -quaternion[1], -quaternion[2], -quaternion[3]};

    for(i = 0; i < targets -> number; i++){
        star_quat[0] = 0.;
        star_quat[1] = cos(targets -> dec[i] * (TWO_PI/360))*cos(targets -> ra[i] * (TWO_PI/360));
        star_quat[2] = cos(targets -> dec[i] * (TWO_PI/360))*sin(targets -> ra[i] * (TWO_PI/360));
        star_quat[3] = sin(targets -> dec[i] * (TWO_PI/360));


        /*apply quaternion to vector*/
        multiply_quaternion(quaternion, star_quat, tmp_quat);
        multiply_quaternion(tmp_quat, quaternion_inv, sc_quat);

        /*extract vector*/
        sc_vec[0] = sc_quat[1];
        sc_vec[1] = sc_quat[2];
        sc_vec[2] = sc_quat[3];

        flag = transform_to_detector(sc_vec, velocity, res);

        if(flag == 0){
            targets -> x[i] = res[0] + dim_x/2;
            targets -> y[i] = res[1] + dim_y/2;
            targets -> visible[i] = 1;
        }
        else{
            targets -> visible[i] = 0;
        }
    }
    return 0;
}


/**
 * @brief function that generates a centroid packet based on the current state of the simulation environment.
 *
 * @param update: latest updated hfs parameters from the simulation environment
 * @param[out] cent_packet: centroid packet struct passed to the simulation environment.
 */

int FGS :: generate_centroid(hfs_parameters update)
{
    std::ofstream outfile;
    struct coord queue_in;
    double delay = 0.;
    rand_seed = rand_seed * 1103515245 + 12345;
    transform_star_coordinates(update.position_quat, update.scVelocity);
    srand(rand_seed);
    random_normal_trm(bias_sample, 0, read_noise, 128);
    generate_image();
    ramp_counter = ramp_counter + 1;
    queue_in = perform_algorithm();

    reset_arrays(0);
    if(ramp_counter >= ramp_length){
        reset_ramp();
        ramp_counter = 0;
    }
    x_smear_0 = 0.;
    y_smear_0 = 0.;

    delay = random_normal_number(delay_mean, delay_std);

    if(delay < delay_min){
        delay = delay_min;
    }
    if(delay > delay_max){
        delay = delay_max;
    }

    queue_in.time = internal_time + delay;
    calc_error(queue_in.x, queue_in.y);
    /* Convert centroid to mas in FBT frame*/
    queue_in.x = (queue_in.x - dim_x/2) * plate_scale;
    queue_in.y = (queue_in.y - dim_y/2) * plate_scale;

    queue_in.x = (queue_in.x + random_normal_number(0, jitter_error) + ((double) update.add_shift_x)) * update.mult_shift_x;
    queue_in.y = (queue_in.y + random_normal_number(0, jitter_error) + ((double) update.add_shift_y)) * update.mult_shift_y;

    if(queue_in.validity.index > 100 && queue_in.validity.index < 106){
        queue_in.x = 0;
        queue_in.y = 0;
    }
    if(sat_counter >= sat_limit && strcmp(mode, MODE_ACQU) != 0){
        queue_in.validity.flag = 109;
        queue_in.validity.index = 0.;
    }
    queue_in.integration_start = start_exposure;
    centroid_queue.push_back(queue_in);
    return 0;
}

/**
 * @brief Function for reseting the array. The previous image used for the CDS is replaced with the reference frame
 * 
 * @return 0 on success
 */
int FGS :: reset_ramp()
{
    unsigned int i, index;
    random_normal_trm(reset_sample, bias_value*gain, reset_noise, 128);
    for(i = 0; i < XDIM*YDIM; i++){
        previous_image[i] = reference_cal[i];
        index = (unsigned int) (random() * (1. / RAND_MAX) * 128);
        image_ramp[i] = (unsigned int) reset_sample[index];
    }

    return 0;
}

/**
 * @brief Function for updating the output centroid packet with the latest centroid measurement
 *
 * @param[out] cent_packet: centroid packet struct that is visible to the Simulink model
 */


int FGS :: send_centroid(hfs_parameters update, outputHfs *outPacket)
{
    struct coord queue_out;

    queue_out = centroid_queue.front();
    centroid_queue.pop_front();

    if(failed == 1){
        outPacket -> failedState = failed;
    }
    else{
        outPacket -> x = queue_out.x;
        outPacket -> y = queue_out.y;
        outPacket -> validity_flag = queue_out.validity.flag;
        outPacket -> validity_index = queue_out.validity.index;
        outPacket -> magnitude = queue_out.validity.magnitude;
        outPacket -> time = queue_out.time;
        outPacket -> integration_start = queue_out.integration_start;
        outPacket -> channel = channel;
        outPacket -> xErr = error.xErr;
        outPacket -> yErr = error.yErr;

        if(strcmp(mode, MODE_TRAC) == 0){
            outPacket -> mode = 2;
        }
        else
        if(strcmp(mode, MODE_ACQU) == 0){
            outPacket -> mode = 1;
        }

        if(update.set_invalid != 0){
            outPacket -> validity_index = 0.;
            outPacket -> validity_flag = 0;
        }
        if(update.set_error != 0){
            outPacket -> validity_flag = update.set_error;
        }
        outPacket -> failedState = failed;
    }
    return 0;
}

int FGS::calc_error(double x, double y)
{
    double absErr = 0.;
    unsigned int i;
    double tmpX, tmpY, tmpAbs;

    for(i = 0; i < targets -> number; i++){
        tmpX = x - targets -> x[i];
        tmpY = y - targets -> y[i];
        tmpAbs = sqrt(pow(tmpX, 2) + pow(tmpY, 2));
        if(absErr == 0){
            absErr = tmpAbs;
            error.xErr = tmpX * plate_scale;
            error.yErr = tmpY * plate_scale;
            }
        else{
            if(tmpAbs < absErr){
                absErr = tmpAbs;
                error.xErr = tmpX * plate_scale;
                error.yErr = tmpY * plate_scale;
            }
        }
    }

    return 0;
}

char string_buffer[64];

/**
 * @brief Main executable of the HFS. Creates images and centroid packets based
 * on the incoming parameters
 *
 * If the centroid flag isn't set, the function will update the smearing kernel
 * in order to simulate the integration period. Once the centroid flag is set,
 * the full image is simulated and the centroid algorithm is performed. If the
 * save flag is set, the function will generate a xml file based on the current
 * state of the simulator.
 *
 * @param update: hfs_parameters struct containing the kinematic data of
 *                the spacecraft and the modes/flags
 * @param[out] outPacket: pointer to the output packet
 *
 */

int FGS :: set_params(hfs_parameters update, outputHfs *outPacket)
{
    const char* update_mode_char;
    
    if(update.save == 1){
        snprintf(string_buffer, sizeof(string_buffer), "HFS_config_%.0f.xml", sim_time);
        generate_config(string_buffer);
    } 
    if(update.time < sim_time){
        return 0;
    }
    if(update.mode == 1)
        update_mode_char = MODE_ACQU;
    else if(update.mode == 2)
        update_mode_char = MODE_TRAC;
    else
        update_mode_char = MODE_STBY;
    
    if (update.time != 0 && update.time != sim_time){
        internal_time = internal_time + timestep + random_normal_number(timing_drift, timing_jitter);
    }
    sim_time = update.time;

    if(update.validation_signal != target_signal){
        target_signal = update.validation_signal;
        calculate_ramp_length();
        reset_ramp();
    }

    if(update.set_error == 111 && failed == 0){
        failed = 1;
        send_centroid(update, outPacket);
        return 0;
    }

    /*Check if centroid shall be generated*/
    if((strcmp(mode, MODE_ACQU) == 0 || strcmp(mode, MODE_TRAC) == 0) && reset_end == 0){
        if((start_exposure + exposure_time - timing_tolerance) < internal_time && transition_end == 0){
            generate_smear(update.ang_rate, timestep);
            generate_centroid(update);
            start_exposure = internal_time;
        }
        else if(transition_end == 0){
            generate_smear(update.ang_rate, timestep);
        }
        if(transition_end == 0){
            if((strcmp(mode, update_mode_char) != 0) && (channel != update.channel)){
                transition_end = internal_time + transition_delay_mode + transition_delay_channel;
            }
            else if(strcmp(mode, update_mode_char) != 0){
                transition_end = internal_time + transition_delay_mode;
            }
            else if(channel != update.channel){
                transition_end = internal_time + transition_delay_channel;
            }
        }
    }
    else{
        if(transition_end == 0){
            if((strcmp(mode, update_mode_char) != 0) && (channel != update.channel)){
                transition_end = internal_time + transition_delay_mode + transition_delay_channel;
            }
            else if(strcmp(mode, update_mode_char) != 0){
                transition_end = internal_time + transition_delay_mode;
            }
            else if(channel != update.channel){
                transition_end = internal_time + transition_delay_channel;
            }
        }
    }
    /* Check if centroid shall be sent*/
    if((centroid_queue.front().time - timing_tolerance) < internal_time && (centroid_queue.front().time - timing_tolerance) > 0 && reset_end == 0 && !centroid_queue.empty()){
        send_centroid(update, outPacket);
    }
  
    if(transition_end != 0 && (transition_end - timing_tolerance) < internal_time && reset_end == 0){
        if((trf_x != update.target_pos_x || trf_y != update.target_pos_y) && (strcmp(update_mode_char, MODE_TRAC) == 0)){
            trf_x = update.target_pos_x;
            trf_y = update.target_pos_y;
        }
        set_mode(update_mode_char);
        set_channel(update.channel);
        reset_arrays(0);
        x_smear_0 = 0.;
        y_smear_0 = 0.;
        transition_end = 0;
        start_exposure = internal_time;
    }
      
    if(update.reset != 0){
        reset_end = internal_time + reset_duration;
    }
    
    if(reset_end != 0 && (reset_end - timing_tolerance) < internal_time){
        reset_fgs(input_file.c_str(), 0);
        reset_end = 0.;  
    }

    if(failed == 1 && update.set_error != 111){
        reset_fgs(input_file.c_str(), 0);
        failed = 0;
    }
   
    return 0;
}
/**
 * @brief utility to calculate the length of the ramp based on the configured star verificaiton signal
 * 
 * The function calculates the length of the ramp based on the full well capacity, the target signal
 * comming from the simulaiton environment, the quantum efficiency and the exposure time.
 * The factor 0.1 is due to the PSF spreding the signal accross multiple pixels. The value is the brightest
 * value inside the PSF.
 * 
 * @return 0 on success
 
 */

int FGS::calculate_ramp_length()
{
    ramp_length = (unsigned int) (full_well_cap * gain / (target_signal * 0.1 * qe * exposure_time));
    if(ramp_length > max_ramp){
        ramp_length = max_ramp;
    }
    if((ramp_length) == 0){
        ramp_length = ramp_length + 1;
    }
    return 0;
}


char line[1024];

/**
 * @brief searches star catalogue for given id and adds it to a stars struct
 *
 * Parser searching the star catalogue for the given target name and adding the
 * cordinates and fluxes to the passed struct. Automatically handels the
 * incrementation of the the target counter.
 *
 * @param sim_stars: pointer to the destination struct
 * @param catalogue: star catalogue used 
 *
 * @return 0 on success, 1 if target cannot be found
 */

int search_star_id(struct stars* sim_stars, const char* catalogue)
{

    FILE* stream = fopen(catalogue, "r");
    char* end;
    const char* tok;
    unsigned int i;


    if(stream == NULL){
        std::cerr << "Star catalogue could not be found!\n";
        return -1;
    }

    while (fgets(line, 1024, stream)){
        tok = strtok(line, ",");
	    if(strcmp(tok, "Name") == 0){
	        continue;
	    }
        strcpy(sim_stars -> id[sim_stars -> number], tok);

        i = 0;
        for(tok = strtok(NULL, ",\n"); tok && *tok; i++, tok = strtok(NULL, ",\n")){
            switch(i){ 
                case 0:
                    sim_stars -> signalFGS1[sim_stars -> number] = strtod(tok, &end);
                    break;
                case 1:
                    sim_stars -> signalFGS2[sim_stars -> number] = strtod(tok, &end);
                    break;
                case 2:
                    sim_stars -> ra[sim_stars -> number] = strtod(tok, &end);
                    break;
                case 3:
                    sim_stars -> dec[sim_stars -> number] = strtod(tok, &end);
                    sim_stars -> number++;
                    break;
            }
        }
    }
    fclose(stream);
    return 0;
}

double cross[3];

/**
 * @brief Multiplies two quaternions
 *
 * The function multiplies two quaternions in order to calculate the
 * tranformation of a vector. Quaternions are in scalar first notation
 *
 * @param quaternion1: first quaternion
 * @param quaternion2: second quaternion
 * @param[out] output: output quaternion
 *
 * @return 0 on success
 */

int multiply_quaternion(double (&quaternion1)[4], double (&quaternion2)[4], double (&output)[4])
{

    double q_vec1[3] = {quaternion1[1], quaternion1[2], quaternion1[3]};
    double q_vec2[3] = {quaternion2[1], quaternion2[2], quaternion2[3]};
    double s1 = quaternion1[0];
    double s2 = quaternion2[0];
    unsigned int i;

    cross3(q_vec1, q_vec2, cross);
    output[0] = s1 * s2 - dot(q_vec1, q_vec2, 3);

    for(i = 0; i < 3; i++){
        output[i+1] = s1 * q_vec2[i] + s2 * q_vec1[i] + cross[i];
    }
    return 0;
}

/**
 * @brief Normalizes a 3D Vector
 *
 * The function normalizes a 3 dimensional vector
 *
 * @param vec: input vector
 */

void normalize(double* vec)
{
    double norm;
    unsigned int i;
    norm = sqrt(dot(vec, vec, 3));
    for (i = 0; i < 3; ++i) {
        vec[i] /= norm;
    }
}

/**
 * @brief Determine relativistic aberration based on velocity vector
 *
 * The function shifts the input vector starVec based on the realtivistic aberration
 * introduced by the velocity vector vel´.
 *
 * @param starVec: stellar input vector
 * @param vel: velocity vector in km/s
 */
void apply_aberration(double* starVec, double *vel)
{
    double n_AX[3], n_AY[3], n_AZ[3], paraCheck[3];
    double cosTheta0, sinTheta0, cosTheta, sinTheta, velOnCNorm;
    unsigned int i;

    cross3(starVec, vel, paraCheck);

    normalize(starVec);

    if (dot(vel, vel, 3) < 1e-20) {
        return; /* neglect tiny correction */
    }

    if(fabs(paraCheck[0]) < 1e-5 && fabs(paraCheck[1]) < 1e-5 && fabs(paraCheck[2]) < 1e-5){
        return; /*check if parallel*/
    }
    n_AX[0] = vel[0];
    n_AX[1] = vel[1];
    n_AX[2] = vel[2];

    normalize(n_AX);

    cross3(n_AX, starVec, n_AZ);

    normalize(n_AZ);

    cross3(n_AZ, n_AX, n_AY);

    cosTheta0 = -1.0 * dot(starVec, n_AX, 3);
    sinTheta0 = -1.0 * dot(starVec, n_AY, 3);

    velOnCNorm = sqrt(dot(vel, vel, 3)) / CVEL;

    cosTheta = (cosTheta0 - velOnCNorm) / (1 - velOnCNorm*cosTheta0);
    sinTheta = (sinTheta0 > 0 ? +1.0 : -1.0) * sqrt(1 - cosTheta*cosTheta);

    for (i = 0; i < 3; ++i){
        starVec[i] = -1.0 * (cosTheta * n_AX[i] + sinTheta * n_AY[i]);
    }
    return;
}

/**
 * @brief Check if files is empty
 * 
 * @param pFile: input file stream
 * @return true if files is empty, false otherwise
 */

bool is_empty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}
