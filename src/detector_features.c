/**
* @file    detector_features.c
* @author  Gerald Mösenlechner (gerald.moesenlechner@univie.ac.at)
* @date    November, 2024
* @version 1.3.3
*
* @brief Functions for the generation of detector features, such as bias, dark and flats.
*
*
* ## Overview
* C library containing functions for the creation of simulated images of detector features like bias, dark and flat.
*
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <string.h>
#include <float.h>
#include <complex.h>
#include <fftw3.h>
#include <stdbool.h>
#include "./detector_features.h"

#define SWAP(T, a, b) do { T tmp = a; a = b; b = tmp; } while (0)
#define SET_PIXEL(img,cols,x,y,val) ( (img)[(x) + (cols) * (y)] = (val) )
#define SET_PIXEL_SMEAR(img,cols,x,y,val) ( (img)[(x) + (cols) * (y)] += (val) )
#define GET_PIXEL(img,cols,x,y) ( (img)[(x) + (cols) * (y)] )
#define TWO_PI 6.28318531f
#define SIGDIVFWHM 0.42466452f
#define XDIM 200
#define YDIM 200

#if 1
#pragma GCC optimize("O3","unroll-loops","omit-frame-pointer","inline")
#pragma GCC target("avx")
#endif

/**
 * @brief generation of the uniform randomly distributed bias.
 *
 * @param[out] bias: output buffer of the bias
 * @param bias_value: mean value of the bias
 * @param readout_noise: readout noise of the detector
 * @param width/height: size of the detector
 * @param os: oversampling of the final image
 *
 */
void generate_bias(double *bias, double *rn_sample, double bias_value, unsigned int sample_length, unsigned int width, unsigned int height)
{
    unsigned int i, index;

    for(i = 0; i<width*height; i++){
        index = (unsigned int) (random() * (1. / RAND_MAX) * sample_length);
        bias[i] = bias_value + rn_sample[index];
    }

}

/**
 * @brief function for the generation of a darkframes.
 *
 * @param[out] darkframe: output buffer of the dark current
 * @param dark_mean: mean value of the dark (e/s)
 * @param exp_time: exposure time in s
 * @param width/height: size of the detector
 * @param os: oversampling of the final image
 *
 */
void generate_dark(double *darkframe, double dark_mean, double exp_time, unsigned int width, unsigned int height)
{
  	unsigned int i;

  	if(dark_mean == 0.){
        for(i = 0; i < width*height; i++){
      	    darkframe[i] = 0.0;
    	}
  	}
  	else{
    	random_poisson_trm(darkframe, dark_mean*exp_time, width*height);
  	}	
}

/**
 * @brief function for the generation of a hot pixel mask.
 *
 * @param[out] hot_pixels: output buffer for hp image
 * @param amount: 0-1, perentage of the detector with hot pixels
 * @param lower: lower boundary for the signal increase
 * @param upper: upper boundary for the signal increase
 * @param width/height: size of the detector
 *
 */
void generate_hotpixels(double *hot_pixels, double amount, double lower, double upper, unsigned int width, unsigned int height)
{
  	double value;
  	unsigned int count, x, y;
  	unsigned int i;

  	count = (unsigned int) ceil(width*height*amount);

  	for(i = 0; i < width*height; i++){
    	hot_pixels[i] = 1.;
  	}

  	for(i = 0; i < count; i++){
   		x = (unsigned int) (random() * (1. / RAND_MAX) * width);
   		y = (unsigned int) (random() * (1. / RAND_MAX) * height);
   		value = lower + (random() * (1. / RAND_MAX) + (upper - lower));
   		SET_PIXEL(hot_pixels,width,x,y,value);
  	}
}


/**
 * @brief function for the generation of a flat.
 *
 * @param[out] flatfield: output buffer for the flat field
 * @param flat_mean: mean of the pixel sensitivity (0-1.0)
 * @param flat_sigma: standard deviation of the pixel sensitivity
 * @param width/height: size of the detector
 *
 */
void generate_flat(double *flatfield, double flat_mean, double flat_sigma, unsigned int width, unsigned int height)
{
  	unsigned int i;

  	random_normal_trm(flatfield, flat_mean, flat_sigma, width*height);

  	for(i = 0; i < width*height; i++){
        if(flatfield[i] > 1.0){
      		flatfield[i] = 1.0;
    	}
    	else if(flatfield[i] < 0.0){
      		flatfield[i] = 0.0;
    	}
  	}
}


/*
 * @brief Function that sets a star at a given position with subpx
 * precision
 *
 * @param image: image of the star mask
 * @param width/height: dimension of the starmask
 * @param x/y: position of the star
 * @param signal: signal of the star
 *
 */
void set_star_in_image(double *image, unsigned int width, unsigned int height, double x, double y, double signal)
{
	double x_orig, y_orig;
	unsigned int i, j;
	int x_pos[2], y_pos[2];
	/*Starmask must be flipped due to origin change from convolution*/
	x_orig = x;
	y_orig = y;

	x = x_orig + 0.5;
	y = y_orig + 0.5;

	x_pos[0] = (int) (x - 0.5);
  	y_pos[0] = (int) (y - 0.5);
  	x_pos[1] = (int) (x + 0.5);
  	y_pos[1] = (int) (y + 0.5);

	for(i = 0; i < 2; i++){
		for(j = 0; j < 2; j++){
			if(x_pos[i] >= 0 && x_pos[i] < width){
				if(y_pos[j] >= 0 && y_pos[j] < height){
					SET_PIXEL_SMEAR(image, height, x_pos[i], y_pos[j], (1.0-fabs((x_orig-x_pos[i]))) * (1.0-fabs((y_orig-y_pos[j])))*signal);
				}
			}
		}
	}
}

/*
 * @brief Function that creates a mask with pointsources containing the sources
 * signal for the image generation.
 *
 * @param[out] mask: buffer of the star mask
 * @param starfield: strcut containing the positions and signals of all stars
 * @param exposure_time: exposure time of the simulated image, needed for signal calculations
 * @param oversampling: oversampling factor of the image (size of subpx flat)
 * @param detector_width/detector_height: Dimension of the detector [px]
 *
 */
void generate_starmask(double *mask, struct stars star_field, double exposure_time, unsigned int detector_width, unsigned int detector_height, unsigned int channel)
{
  	unsigned int i;
  	double signal;
	
  	for(i = 0; i<star_field.number; i++){
	    if(star_field.visible[i] == 1){
		    if(star_field.x[i] > 0){
		        if(star_field.y[i] > 0){
			        if(star_field.x[i] < detector_width){
			            if(star_field.y[i] < detector_height){
				            if(channel == 1){
				                signal = star_field.signalFGS1[i];
				            }
				            else{
				                signal = star_field.signalFGS2[i];
				            }
				            set_star_in_image(mask, detector_width, detector_height, star_field.x[i], star_field.y[i], signal*exposure_time);
			            }
			        }
		        }
		    }
	    }
	}
}
    

/*
 * @brief modified sliding window convolution used for the creation of stellar
 * images
 *
 * @param[out] result: buffer of convolved image
 * @param starmask: array containg the original starmask
 * @param psf: array containing the pointspread function or convolution kernel
 * @param width_star/height_star: dimensions of the starmask
 * @param width_psf/height_psf: dimension of the Psf
 *
 */
void convolve_starmask_fast(double *result, double *starmask, double *psf, int width_star, int height_star, int width_psf, int height_psf)
{
	unsigned int i, j, k, l, i_start, j_start;
	bzero(result, (size_t) width_star * height_star * sizeof(double));
	for(i=0; i<width_star; i++){
		for(j=0; j<height_star; j++){
			if (starmask[i+height_star*j] > 0){
				i_start = i - (width_psf - 1)/2;
				j_start = j - (height_psf - 1)/2;
				for(k=0; k<width_psf; k++){
					for(l=0; l<height_psf; l++){
						if((i_start + k) >= 0){
                            if((j_start + l) >= 0){
                                if((i_start + k) < width_star){
                                    if((j_start + l) < height_star){
							            result[i_start + k + height_star * (j_start + l)] = result[i_start + k + height_star * (j_start + l)] + psf[k + height_psf*l] * starmask[i + height_star*j];
							        }
                                }
                            }
                        }
                    }
		        }
			}
		}   
    }
}

/*
 * @brief function, that takes a starmask and convolves it with the instruments
 * Psf
 *
 * @param[out] star_image: buffer for star image
 * @param psf: array containing the oversampled Psf
 * @param starmask: array containing the star mask
 * @param oversampling: oversampling factor of the Psf
 * @param width/height: dimension of the star mask
 * @param width_psf/height_psf: dimension of the Psf
 *
 */
void generate_star_image(double *star_image, double *psf, double *starmask, int width, int height, int width_psf, int height_psf)
{
	convolve_starmask_fast(star_image, starmask, psf, width, height, width_psf, height_psf);
}

/**
 * @brief generation of the background noise
 *
 * @param[out] bkgrnoise: buffer for the final shot noise image
 * @param psf: Psf of the instrument
 * @param background_signal: Mean signal of the noise distribution
 * @param exposure_time: Exposure time of the simulated image
 * @param oversampling: Oversampling of the image in accordance to the subpx flat
 * @param width_psf: size of the psf in x direction
 * @param height_psf: size of the psf in y direction
 *
 */
void generate_background(double *bkgrnoise, double background_signal, double qe, double exposure_time, int width, int height)
{
    double signal;
    signal = background_signal*exposure_time*qe;
    random_poisson_trm(bkgrnoise, signal, width*height);
}

double round(double x)
{
    return floor(x) + 0.5; 
}

double fractional(double x)
{
    return x - floor(x);
}

double complement(double x)
{
    return 1 - fractional(x);
}

/**
 * @brief function that generates a linear smearing kernel based on the Bresenham line algorithm
 *
 * @param[out] smear_kernel: buffer for the smearing kernel
 * @param x: length of the smear in x direction
 * @param y: length of the smear in y direction
 * @param dim: dimension of the smearing kernel
 * @param oversampling: Oversampling of the image
 *
 */
int generate_linear_smearing_kernel(double *smear_kernel, double x_origin, double y_origin,  double x, double y, unsigned int dim)
{
    double dx, dy, grad, xEnd, yEnd, xGap, x0, x1, y0, y1, inter;
	unsigned int xPoint1, xPoint2, yPoint1, yPoint2, i;
    bool steep;
	int warning = 0;

	x0 = dim/2 + x_origin;
	y0 = dim/2 + y_origin;
	x1 = x0 + x;
	y1 = y0 + y;


    if(fabs(x0-x1) < 1e-7 && fabs(y0 - y1) < 1e-7){
        SET_PIXEL_SMEAR(smear_kernel, dim, (int) x0, (int )y0, 1);
        return warning;
    }

  	if(x0 > (dim-1)){
  	    x0 = dim-1;
			warning = 1;
		}
  	if(x0 < 0){
  	    x0 = 0;
		warning = 1;
  	}
  	if(x1 > (dim-1)){
  	    x1 = dim-1;
		warning = 1;
  	}
  	if(x1 < 0){
  	    x1 = 0;
  	}
  	if(y0 > (dim-1)){
  	    y0 = dim-1;
		warning = 1;
  	}
  	if(y0 < 0){
  	    y0 = 0;
		warning = 1;
  	}
  	if(y1 > (dim-1)){
  	    y1 = dim-1;
		warning = 1;
  	}
  	if(y1 < 0){
  	    y1 = 0;
		warning = 1;
  	}
    
    steep = fabs(y1 - y0) > fabs(x1 - x0);
    if(steep){
        SWAP(double, x0, y0);
        SWAP(double, x1, y1);
    }

    if(x0 > x1){
        SWAP(double, x0, x1);
        SWAP(double, y0, y1);
    }

	dx = x1 - x0;
	dy = y1 - y0;


    if(fabs(dx) < 1e-7){
        grad = 1.0;
    }
    else{
        grad = dy / dx;
    }

    xEnd = round(x0);
    yEnd = y0 + grad * (xEnd - x0);
    xGap = complement(x0 + 0.5);

	if(xEnd < 0)
		xEnd = 0;
	if(yEnd < 0)
		yEnd = 0;
	if(xEnd > dim - 1)
		xEnd = dim - 1;
	if(yEnd > dim - 1)
		yEnd = dim - 1;

    xPoint1 = (int) xEnd;
    yPoint1 = (int) yEnd;

    if(steep){
        SET_PIXEL_SMEAR(smear_kernel, dim, yPoint1, xPoint1, complement(yEnd) * xGap);
        SET_PIXEL_SMEAR(smear_kernel, dim, yPoint1 + 1, xPoint1, fractional(yEnd) * xGap);
    }
    else{
        SET_PIXEL_SMEAR(smear_kernel, dim, xPoint1, yPoint1, complement(yEnd) * xGap);
        SET_PIXEL_SMEAR(smear_kernel, dim, xPoint1, yPoint1 + 1, fractional(yEnd) * xGap);
    }
    inter = yEnd + grad;

    xEnd = round(x1);
    yEnd = y1 + grad * (xEnd - x1);
    xGap = fractional(x1 + 0.5);
	
	if(xEnd < 0)
		xEnd = 0;
	if(yEnd < 0)
		yEnd = 0;
	if(xEnd > dim - 1)
		xEnd = dim - 1;
	if(yEnd > dim - 1)
		yEnd = dim - 1;

    xPoint2 = (int) xEnd;
    yPoint2 = (int) yEnd;

    if(steep){
        SET_PIXEL_SMEAR(smear_kernel, dim, yPoint2, xPoint2, complement(yEnd) * xGap);
        SET_PIXEL_SMEAR(smear_kernel, dim, yPoint2  + 1, xPoint2, fractional(yEnd) * xGap);
    }
    else{
        SET_PIXEL_SMEAR(smear_kernel, dim, xPoint2, yPoint2, complement(yEnd) * xGap);
        SET_PIXEL_SMEAR(smear_kernel, dim, xPoint2, yPoint2 + 1, fractional(yEnd) * xGap);
    }

    if((xPoint2 - xPoint1) <=1 && (yPoint2 - yPoint1) <=1){
        return warning;
    }

    if(steep){
        for(i = xPoint1; i < xPoint2; i++){
            SET_PIXEL_SMEAR(smear_kernel, dim, (int)inter, i, complement(inter));
            SET_PIXEL_SMEAR(smear_kernel, dim, (int)inter + 1, i, fractional(inter));
            inter = inter + grad;
        }
    }
    else{
        for(i = xPoint1; i < xPoint2; i++){
            SET_PIXEL_SMEAR(smear_kernel, dim, i, (int)inter, complement(inter));
            SET_PIXEL_SMEAR(smear_kernel, dim, i, (int)inter + 1, fractional(inter));
            inter = inter + grad;
        }
    }
	return warning;
}

/**
 * @brief function that generates a linear smearing kernel and convolves it with a given star-map
 *
 * @param[out] smeared_image: buffer of the smeared star image
 * @param starmask: input starmask that should be convolved with the smearing kernel
 * @param width: x dimension of the starmask
 * @param height: y dimension of the starmask
 * @param x: length of the smear in x direction
 * @param y: length of the smear in y direction
 * @param oversampling: Oversampling of the image
 *
 */
void smear_star_image(double *smeared_image, double *starmask, double *smear_kernel, unsigned int width, unsigned int height, unsigned int width_kernel, unsigned int height_kernel)
{
	convolve_starmask_fast(smeared_image, starmask, smear_kernel, width, height, width_kernel, height_kernel);
}

/**
 * @brief Function for generating Shot noise. The mean is subtracted in order to keept the signal
 * in the image consistent. 
 *
 * @param signal: combined stellar and bkgr flux of the image
 * @param[out] shot_noise: buffer for the output array
 * @param width: width of the image buffers
 * @param height: height of the image buffers
 *
 *
 */
void generate_shot(double *signal, double *shot_noise, unsigned int width, unsigned int height)
{
    unsigned int i;
    double matrix;

    for(i = 0; i < width*height; i++)
    {
        random_poisson_trm(&matrix, signal[i], 1);
		shot_noise[i] = matrix;
    }
}

double smear_buffer[500*500];

/**
 * @brief Function for inverting and normalizing the smearing kernel so that the end 
 * point is centered note that this function is designed for the downsampled kernel with size 50*50
 *
 * @param[out] smear: smearing kernel
 * @param x_smear: endpoint of the smear in x dim
 * @param y_smear: endpoint of the smaer in y dim
 * @param dim: dimension of the kernel
 *
 */
void shift_smear(double *smear, double x_smear, double y_smear, unsigned int dim)
{
	int x_diff, y_diff;
	unsigned int i, j;
	double sum = 0;

	x_diff = (int) - x_smear;
	y_diff = (int) - y_smear;

	bzero(smear_buffer, 500*500);

	for(i = 0; i < dim; i++){
		for(j = 0; j < dim; j++){
			if((i+x_diff) >= 0 && (i+x_diff) < dim){
				if((j+y_diff) >= 0 && (j+y_diff) < dim){
					SET_PIXEL(smear_buffer, 500, i+x_diff,  j+y_diff, smear[i + dim*j]);
				}
			}
		}
	}

	for(i = 0; i < dim; i++){
		for(j = 0; j < dim; j++){
			smear[i + dim*j] = smear_buffer[i + 500*j];
			sum = sum + smear[i + dim*j];
		}
	}	
}

/**
 * @brief Function for generating a field of view mask
 *
 * @param[out] fov_mask: buffer for the fov mask
 * @param radius: radius of the fov in arcsec
 * @param platescale: platescale of the instrument in arcsec/px
 * @param width: width of the mask
 * @param height: height of the mask
 *
 */

void generate_fov_mask(double *fov_mask, double radius, double platescale, unsigned int width, unsigned int height)
{
	unsigned int i, j;
	double x, y, x_center, y_center;

	x_center = width/2;
	y_center = height/2;

	for(i = 0; i < width; i++){
		for(j = 0; j < height; j++){

			x = (i - x_center) * platescale;
			y = (j - y_center) * platescale;

			if(sqrt(x*x + y*y) < radius){
				SET_PIXEL(fov_mask, width, i, j, 1);
			}
			else{
				SET_PIXEL(fov_mask, width, i, j, 0);
			}
		}
	}
}

/*
 * @brief Function for applying the fov mask to an image
 *
 * @param[out] image: buffer for the image
 * @param fov_mask: buffer for the fov mask
 * @param width_img/height_img: dimension of the image
 * @param width_fov/height_fov: dimension of the fov mask
 * @param x_img/y_img: position of the image in the fov
 * 
*/

void apply_fov_mask(double *image, double *fov_mask, unsigned int width_img, unsigned int height_img, unsigned int width_fov, unsigned int height_fov, int x_img, int y_img)
{
	unsigned int i, j;
	int  x_start, y_start;
	double tmp;
	x_start = (int) width_fov/2 - ((int) width_img/2 - x_img);
	y_start = (int) height_fov/2 - ((int) height_img/2 - y_img);
	for(i = 0; i < width_img; i++){
		for(j = 0; j < height_img; j++){
			if((x_start + i) >= 0 && (x_start + i) < width_fov){
				if((y_start + j) >= 0 && (y_start + j) < height_fov){
					tmp = (GET_PIXEL(image, width_img, i, j)) * (GET_PIXEL(fov_mask, width_fov, (x_start + i), (y_start + j)));
					SET_PIXEL(image, width_img, i, j, tmp);
				}
				else{
					SET_PIXEL(image, width_img, i, j, 0.0);
				}
			}
			else{
				SET_PIXEL(image, width_img, i, j, 0.0);
			}
		}
	}
}

/**
 * @brief Get the hp array of the current line of sight
 * 
 * @param buffer: hot pixel buffer 
 * @param hp_map: hot pixel master map
 * @param width_buffer: width of the FoV
 * @param height_buffer: height of the FoV
 * @param width_hp: width of the master map
 * @param height_hp: height of the master map
 * @param x: x position of the FoV
 * @param y: y position of the FoV
 */

void get_hp(double* buffer, double* hp_map, unsigned int width_buffer, unsigned int height_buffer, unsigned int width_hp, unsigned int height_hp, int x, int y)
{
	unsigned int i, j;
	int x_start, y_start;
	double tmp;

	x_start = (int) width_hp/2 - ((int) width_buffer/2 - x);
	y_start = (int) height_hp/2 - ((int) height_buffer/2 - y);
	for(i = 0; i < width_buffer; i++){
		for(j = 0; j < height_buffer; j++){
			if((x_start + i) >= 0 && (x_start + i) < width_hp){
				if((y_start + j) >= 0 && (y_start + j) < height_hp){
					tmp = (GET_PIXEL(hp_map, width_hp, (x_start + i), (y_start + j)));
					SET_PIXEL(buffer, width_buffer, i, j, tmp);
				}
				else{
					SET_PIXEL(buffer, width_buffer, i, j, 0.0);
				}
			}
			else{
				SET_PIXEL(buffer, width_buffer, i, j, 0.0);
			}
		}
	}
}

/**
 * @brief Get the calibration array of the current line of sight
 * 
 * @param buffer: calibration buffer 
 * @param hp_map: calibration master
 * @param width_buffer: width of the FoV
 * @param height_buffer: height of the FoV
 * @param width_hp: width of the master
 * @param height_hp: height of the master
 * @param x: x position of the FoV
 * @param y: y position of the FoV
 */

void get_calibration(unsigned short* buffer, unsigned short* cal, unsigned int width_buffer, unsigned int height_buffer, unsigned int width_cal, unsigned int height_cal, int x, int y)
{
	unsigned int i, j;
	int x_start, y_start;
	unsigned short tmp;

	x_start = (int) width_cal/2 - ((int) width_buffer/2 - x);
	y_start = (int) height_cal/2 - ((int) height_buffer/2 - y);
	for(i = 0; i < width_buffer; i++){
		for(j = 0; j < height_buffer; j++){
			if((x_start + i) >= 0 && (x_start + i) < width_cal){
				if((y_start + j) >= 0 && (y_start + j) < height_cal){
					tmp = (GET_PIXEL(cal, width_cal, (x_start + i), (y_start + j)));

					SET_PIXEL(buffer, width_buffer, i, j, tmp);
				}
				else{
					SET_PIXEL(buffer, width_buffer, i, j, 0.0);
				}
			}
			else{
				SET_PIXEL(buffer, width_buffer, i, j, 0.0);
			}
		}
	}
}