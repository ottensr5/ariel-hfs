/**
* @file    fcu_algorithms.c
* @author  Gerald Mösenlechner (gerald.moesenlechner@univie.ac.at)
* @date    November, 2024
* @version 1.3.3
*
*@brief Engineering algorithms for the DPU simulation. contains @ref Centroiding and @ref TargetAcquisition.
* They are used in different situations and provide a centroid packet to the spacecraft.
*
*
* ## Overview
* C library containing the methods and algorithms used for the simulation of the DPU
* and for the Target acquisiting and Centroiding for the ARIEL FGS Control Electronics.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include "fcu_algorithms.h"

#define SET_PIXEL(img,cols,x,y,val) { img[x + cols * y] = val; }
#define GET_PIXEL(img,cols,x,y) ( img[x + cols * y] )
#define SIGDIVFWHM 0.42466452f
#define AMPL_PSF 270.0f
#define TWO_PI 6.28318531f
#define XDIM 200
#define YDIM 200
#define BIN 3
#define ROISIZE 20
#define ROISIZE_COG 100
#define REFINEDROISIZE 21
#define VALBINNING 4




/*Reference Data*/

const float FGS1_Y[25] = {1.47167275e-05, 2.38461646e-05, 3.87655628e-05, 6.75726225e-05, 2.11215257e-04, 5.19455207e-04, 1.19928135e-03, 2.63447848e-03, 4.56727639e-03, 1.08543772e-02, 1.72144731e-02, 2.91691617e-02, 2.51506170e-02, 2.74147190e-02, 2.36298995e-02, 1.53269946e-02, 5.16576064e-03, 1.03382773e-03, 2.64965646e-04, 1.78873205e-04, 8.15633061e-05, 4.72219511e-05, 3.34156503e-05, 9.80079452e-06, 1.41377845e-05};

const float FGS1_X[25] = {0.00029489, 0.0003827, 0.0006489,0.00116522, 0.00165326, 0.00183629, 0.00184897, 0.00274072, 0.00757158, 0.02271154, 0.04101486, 0.03054224, 0.02515062, 0.03338782, 0.0334162, 0.02987768, 0.01898888, 0.00904966, 0.00433987, 0.00245042, 0.0014353, 0.0009233, 0.00049994, 0.0003136, 0.00019248};

const float FGS2_Y[25] = {4.58516318e-05, 1.05016227e-04, 2.02699739e-04, 3.81423208e-04, 5.77229311e-04, 1.06740244e-03, 1.73896758e-03, 2.92656460e-03, 6.47117555e-03, 8.77449011e-03, 1.49745506e-02, 1.70449780e-02, 1.65937231e-02, 2.05735867e-02, 1.63470839e-02, 1.33791399e-02, 8.80076927e-03, 3.88463245e-03, 1.35837579e-03, 4.62881416e-04, 1.76952008e-04, 1.00691379e-04, 6.19276252e-05, 5.27374747e-05, 4.55186973e-05};

const float FGS2_X[25] = {0.00050931, 0.00081074, 0.00076338, 0.00113407, 0.0012236, 0.00225054, 0.00288255, 0.00549922, 0.01107709, 0.02122439, 0.02363573, 0.01719913, 0.01659372, 0.02472113, 0.02537891, 0.02027583, 0.01737751, 0.01285062, 0.00836713, 0.00425878, 0.00258723, 0.00135676, 0.00097328, 0.00060778, 0.0004461};

/**
 * @brief Function for finding the minimum inside of a given array
 *
 * @param image: input array
 * @param lenght: lenght of the input array
 *
 * @returns minimum of given array
 */
unsigned int GetMin(unsigned int* image, unsigned int length)
{
    unsigned int minimum, i;
    minimum = image[0];

    for (i=1; i < length; i++){
        if (image[i] < minimum){
            minimum = image[i];
        }
    }
    return minimum;
}


/**
 * @brief Algorithm for source extraction
 *
 * Algorithm for retriving the brightest pixels of an array based on a sorting
 * algorithm.
 *
 * @param buffer: input array
 * @param buflenght: lenght of the input buffer
 * @param listlenght: number of output values
 * @param[out] list: outputlist for the pixel values
 * @param[out] positions: outputlist for the pixel positions
 *
 */
void find_brightest_uint (unsigned int *buffer, unsigned int buflength, unsigned int listlength, unsigned int *list, unsigned int *positions)
{
    unsigned int ctr, lctr;
    unsigned int temp, temp2, tidx, tidx2;

    list[0] = buffer[0];
    positions[0] = 0;

    for (ctr=1; ctr < buflength; ctr++){
        temp = buffer[ctr];
        tidx = ctr;

        if (temp > list[listlength-1]){
	        for (lctr=0; lctr < listlength; lctr++){
	            if (temp > list[lctr]){
		            temp2 = list[lctr];
		            tidx2 = positions[lctr];
		            list[lctr] = temp;
		            positions[lctr] = tidx;
		            temp = temp2;
		            tidx = tidx2;
		        }
	        }
	    }
    }

    return;
}

/**
 * @brief Function for generating a array containing a 2D gaussian function
 *
 * @param[out] img: target buffer for gaussian function
 * @param cols/rows: dimensions of the input buffer
 * @param center_x/center_y: coordinates of center of gaussian
 * @param fwhm_x/fwhm_y: FWHM of the gaussian
 */
void get_2D_gaussian (float *img, unsigned int cols, unsigned int rows, float center_x, float center_y, float fwhm_x, float fwhm_y)
{
    unsigned int x, y;
    float value;
    float xdist, ydist;
    float sigx, sigy;
    

    sigx = fwhm_x * SIGDIVFWHM;
    sigy = fwhm_y * SIGDIVFWHM;

    for (y = 0; y < rows; y++){
        for (x = 0; x < cols; x++){
            xdist = x - center_x;
            ydist = y - center_y;
            value = exp(-((xdist*xdist)/(2*sigx*sigx)+(ydist*ydist)/(2*sigy*sigy)));
	        SET_PIXEL(img, cols, x, y, value);
        }
    }
    return;
}


/**
 * @brief Function for checking the extent of the bright source.
 *
 * Function that checks the neighbouring pixels of the found source and checks
 * their intensity to get the extent of the source. If the source isn't extended,
 * the source is marked as not being a target. If the source is extended, the
 * sum of the pixels above the given threshold are summed in order to obtain a
 * intensity for further matching.
 *
 * @param image: input array
 * @param sum: list of target intensities
 * @param is_target: list for target identification
 * @param x/y: position of the found source
 * @param width/height: dimension of the image
 * @param threshold: threshold criteria for neighbour identification
 *
 */
int check_neighbours (unsigned int *image, unsigned int *sum, int *is_target, unsigned int x, unsigned int y, unsigned int width, unsigned int height, unsigned int threshold) {
    unsigned int val;
    int ctr=0;
    int i, j, x_tmp, y_tmp;

    for(i = -2;i<=2;i++){
        for(j = -2;j<=2;j++){
            x_tmp = x + i;
            y_tmp = y + j;
            if(x_tmp >= 0 && y_tmp >= 0){
                if(x_tmp <= (width-1) && (y_tmp <= (height-1))){
                    val = GET_PIXEL(image, width, (x_tmp), (y_tmp));
                    if (val > threshold){
                        *sum = *sum + val;
                        ctr = ctr + 1;
                    }
                }
            }
        }
    }

    if (ctr<3){
        *is_target = 0;
    }

    return ctr;
}

/**
 * @brief Aggressive thresholding method using mean and 1 sigma
 *
 * This thresholding method determines the mean and standard deviation of the image and
 * subtracts it from every pixel. This only leaves statistical outliers and helps with
 * guiding of fainter stars on the image outside as the background is greatly redused.
 * Note that this can lead to significant changes to the measured singal 
 *
 * @param[out] image: input array to be processed
 * @param dimX: x dimension of the image
 * @param dimY: y dimension of the image
 *
 */

void std_threshold(unsigned int *image, unsigned int dimX, unsigned int dimY){
    unsigned int mean, std, i;
    unsigned long sum, sumSquare;
    if(dimX == 0 || dimY == 0)
        return;
    
    sum = 0;
    sumSquare = 0;
    for(i = 0; i < dimX*dimY; i++){
        sum = sum + image[i];
        sumSquare = sumSquare + (image[i] * image[i]);
    }

    if(sum == 0)
        return;
    
    mean = (unsigned int) (sum / (dimX * dimY));
    std = (unsigned int) sqrt((sumSquare / (dimX * dimY)) - (mean*mean));
    
    for(i = 0; i < dimX*dimY; i++){
        if(image[i] < (mean + std))
            image[i] = 0;
        else
            image[i] = image[i] - (mean + std);
    }
    return;
}

/**
 * @brief Algorithm for target identification
 *
 * Algorithm that checks potential target stars from the source extraction based
 * on their extent by checking if neighbouring pixels are above a given
 * threshold and by comparing their signal against the given target signal
 *
 * @param image: input array
 * @param[out] brightness: outputlist for the pixel values
 * @param[out] is_target: outputlist for target status
 * @param x_dim/y_dim: lenght of the input buffer
 * @param[out] position: outputlist for the pixel positions
 * @param length: length of the output lists
 * @param sigma: threshold for extent check
 * @param target_brightness: signal of the target star in ADU
 *
 */
int identify_star (unsigned int *image, unsigned int *brightness, int *is_target,  unsigned int x_dim, unsigned int y_dim, unsigned int *positions, int *ext_ctr, unsigned int length, unsigned int sigma, unsigned int target_brightness)
{
    unsigned int pos_x, pos_y, tmp_x, tmp_y;
    unsigned int i, j, ctr = 0;
    unsigned int brightness_diff[length];
    unsigned int loc, minimum;

    if (x_dim == 0)
        return -1;

    if (y_dim == 0)
        return -1;

    for(i = 0; i < length; i++){
        is_target[i] = 1;
        pos_x = (positions[i] % x_dim);
        pos_y = (positions[i] / x_dim);
        for(j = 0; j < i; j++){
            tmp_x = (positions[j] % x_dim);
            tmp_y = (positions[j] / x_dim);
            if((abs(pos_x-tmp_x)+abs(pos_y-tmp_y)) == 1){
                is_target[i] = 0;
            }
        }
    }

    for(i = 0; i < length; i++){
        if(positions[i] > (x_dim * y_dim - 2) || positions[i] < 2){
            is_target[i] = 0;
        }

    }

    for(i = 0; i < length; i++){
        if(is_target[i] != 0){
            pos_x = (positions[i] % x_dim);
            pos_y = (positions[i] / x_dim);
            ext_ctr[i] = check_neighbours(image, &brightness[i], &is_target[i], pos_x, pos_y, x_dim, y_dim, sigma);
        }
    }

    minimum = 0;
    loc = 0;
    for(i = 0; i < length; i++){
        if(is_target[i] != 0){
            brightness_diff[i] = abs(brightness[i]-target_brightness);
            if(ctr == 0){
                minimum = brightness_diff[i];
                loc = i;
                ctr = 1;
            }
            if(brightness_diff[i]<minimum){
                minimum = brightness_diff[i];
                loc = i;
            }
        }
    }

    for(i = 0;i < length; i++){
        if(i != loc){
            is_target[i] = 0;
        }
    }

    return 0;
}


/**
 * @brief Median determination based on sorting the array (destructive) for n = 9
 *
 * @param data: input array
 *
 * @return median of the dataset
 */
unsigned int Med9USpoil (unsigned int *data)
{
    SWAP_SORTU (data[1], data[2]);
    SWAP_SORTU (data[4], data[5]);
    SWAP_SORTU (data[7], data[8]);
    SWAP_SORTU (data[0], data[1]);
    SWAP_SORTU (data[3], data[4]);
    SWAP_SORTU (data[6], data[7]);
    SWAP_SORTU (data[1], data[2]);
    SWAP_SORTU (data[4], data[5]);
    SWAP_SORTU (data[7], data[8]);
    SWAP_SORTU (data[0], data[3]);
    SWAP_SORTU (data[5], data[8]);
    SWAP_SORTU (data[4], data[7]);
    SWAP_SORTU (data[3], data[6]);
    SWAP_SORTU (data[1], data[4]);
    SWAP_SORTU (data[2], data[5]);
    SWAP_SORTU (data[4], data[7]);
    SWAP_SORTU (data[4], data[2]);
    SWAP_SORTU (data[6], data[4]);
    SWAP_SORTU (data[4], data[2]);

    return(data[4]);
}

/**
 * @biref Region of interest extraction for the target acquisition
 *
 * The function takes the original image and the position on the binned imaged
 * from the source extraction and extracts a RoI for further calculations
 *
 * @param image: buffer containing the original image
 * @param width/height: dimensions of the image
 * @param target_x/target_y: upper left corner of the RoI
 * @param[out] RoI: buffer containing the region of interest
 *
 */
void extract_RoI (unsigned int *image, unsigned int width, unsigned int height, unsigned int target_x, unsigned int target_y, unsigned int *RoI)
{
    unsigned int x_ctr, y_ctr, x_start, y_start;

    x_start = target_x * BIN;
    y_start = target_y * BIN;

    for (x_ctr = 0; x_ctr < ROISIZE*BIN; x_ctr++){
        for (y_ctr = 0; y_ctr < ROISIZE*BIN; y_ctr++){
	        RoI[x_ctr + y_ctr*ROISIZE*BIN] = image[x_start+x_ctr + (y_start + y_ctr)*height];
	    }
    }
    return;
}

/**
 * @brief calculates mean and standard deviation for a given dataset
 *
 * @param data: pointer to the input data (integer)
 * @param len: number of values to process
 * @param[out] m: pointer to the mean (float) to store the result
 * @param[out] sig: pointer to the stdev (float) to store the result
 * @note considers Bessel correction
 */
int MeanSigma (unsigned int *data, int len, float *m, float *sig)
{
    int i;
    double sum = 0.;
    double sumq = 0.;
    double mean, var, sigma;

    /* avoid division by zero */
    if (len == 0){
        /* m and sig will be undefined */
        return -1;
    }
    else if (len == 1){
        *m = data[0];
        *sig = 0.0f;
        return -1;
    }

    for (i=0; i<len; i++)
        sum += data[i];

    mean = (double)sum/len;

    for (i=0; i<len; i++)
        sumq += (data[i]-mean) * (data[i]-mean);

    var = 1./(len-1.) * sumq; /* with Bessel corr. */
    sigma = sqrt(var);

    if (mean != 0.0 && (isnan(mean) || isgreater(fabs(mean), FLT_MAX) || isless(fabs(mean), FLT_MIN))) {
        return -1;
    }
    else{
        *m = (float) mean;
    }

    if (sigma != 0.0 && (isnan(sigma) || isgreater(fabs(sigma), FLT_MAX) || isless(fabs(sigma), FLT_MIN))) {
        return -1;
    }
    else{
        *sig = (float) sigma;
    }

    return 0;
}


/**
 * @brief Median calculation using the Algorithm by Torben Mogensen.
 *        Based on a public domain implementation by N. Devillard.
 *
 * @param data: place where the data are stored
 * @param len: number of values to process
 * @returns median of the given values
 * @note for an even number of elements, it returns the smaller one
 * @note The Torben mehtod does not need a separate buffer and it does not mix the input
 */
int Median (unsigned int *data, int len)
{
    int i, less, greater, equal;
    int min, max, guess, maxltguess, mingtguess;

    min = max = data[0] ;

    /* find min and max */
    for (i=1 ; i < len ; i++){
        if (data[i] < min)
            min=data[i];

        if (data[i] > max)
            max=data[i];
    }

    while (1){
        /* update guesses */
        guess = (min + max) / 2;
        less = 0;
        greater = 0;
        equal = 0;
        maxltguess = min;
        mingtguess = max;

        /* find number of smaller and larger elements than guess */
        for (i=0; i < len; i++){
            if (data[i] < guess){
                less++;
                if (data[i] > maxltguess)
                    maxltguess = data[i];
            }
            else if (data[i] > guess){
                greater++;
                if (data[i] < mingtguess)
                    mingtguess = data[i];
            }
            else{
                equal++;
            }
        }

        /* half the elements are less and half are greater, we hav found the median */
        if ((less <= (len+1)>>1) && (greater <= (len+1)/2))
            break;

        else if (less > greater)
            max = maxltguess ;
        else
            min = mingtguess;
    }

    if (less >= (len+1)>>1)
        return maxltguess;
    else if (less+equal >= (len+1)>>1)
        return guess;

    return mingtguess;
}

/**
 * @brief    3x3 median filter with threshold
 *
 * This algorithm takes an image as input and applies 2D median filtering.
 * For each pixel which is not on one of the four borders, it takes a copy of
 * the 3x3 sub-array and calculates the median. If the absolute difference
 * between pixel value and calculated median is larger than the threshold, then
 * the sample is replaced by the median.
 *
 * @param data: array of input samples
 * @param xdim: size of image in x
 * @param ydim: size in y
 * @param threshold: the threshold value to compare against
 * @param[out] filtered: the filtered output image
 *
 * @returns 0 on success
 */
int MedFilter3x3 (unsigned int *data, unsigned int xdim, unsigned int ydim, unsigned int threshold, unsigned int *filtered)
{
    unsigned int x, y, off;
    unsigned int medwin[9];
    unsigned int pixval, median, diff;

    /* we start at 1,1 and run to xdim-1, ydim-1 so that a 1 pixel border is not processed */
    if (xdim < 3)
        return -1;
    if (ydim < 3)
        return -1;

    for (y=1; y < (ydim-1); y++){
        for (x=1; x < (xdim-1); x++){
	        /* first row */
	        off = (y-1)*xdim;
	        medwin[0] = data[off + x-1];
	        medwin[1] = data[off + x];
	        medwin[2] = data[off + x+1];

	        /* last row */
	        off = (y+1)*xdim;
	        medwin[6] = data[off + x-1];
	        medwin[7] = data[off + x];
	        medwin[8] = data[off + x+1];

	        /* middle row */
	        off = y*xdim;
	        medwin[3] = data[off + x-1];
	        pixval = data[off + x];
	        medwin[4] = pixval;
	        medwin[5] = data[off + x+1];

	        median = Med9USpoil(medwin);

	        if (pixval > median){
	            diff = pixval - median;
	        }
	        else{
	            diff = median - pixval;
	        }

	        if (diff > threshold){
	            filtered[off + x] = median; /* reuse off from middle row */
	        }
	        else{
	            filtered[off + x] = pixval;
	        }
	    }
    }

    /* now copy the borders */
    for (x=0; x < xdim; x++){
        filtered[x] = data[x];
    }
    for (x=(ydim-1)*xdim; x < ydim*xdim; x++){
        filtered[x] = data[x];
    }
    for (y=1; y < (ydim-1); y++){
        filtered[y*xdim] = data[y*xdim];
        filtered[y*xdim + (xdim-1)] = data[y*xdim + (xdim-1)];
    }

    return 0;
}

/**
 * @brief get minimum and maximum value of an unsigned int buffer
 *
 * @param data: array of input samples
 * @param len: number of samples
 * @param[out] min: calculated minimum
 * @param[out] max: calculated maximum
 *
 * @note If len is 0 then min = 0 and max = 0xffffffff.
 */
void MinMaxU32 (unsigned int *data, unsigned int len, unsigned int *min, unsigned int *max)
{
    unsigned int i;

    *min = 0xffffffffu;
    *max = 0x0u;

    for (i=0; i < len; i++){
        *min = *min > data[i] ? data[i] : *min;
        *max = *max < data[i] ? data[i] : *max;
    }

    return;
}

/**
 * @brief calculates the pearson r for two given 1-D samples
 *
 * Function used for the calculateion of the the pearson coefficient that is
 * used as the quality metric of the Centroid Validation
 *
 * @param measured_data: array containing the measured data distribution
 * @param reference_data: array containing the reference data
 * @param length_set: length of the given data sets
 *
 * @returns the pearson r of the two given samples
 */
float pearson_r(unsigned int *measured_data, unsigned int *reference_data, unsigned int length_set)
{
    unsigned int i;
    float mean_data, mean_ref, sum1, sum2, sum3, coeff, fraction;

    if (length_set == 0){
        coeff = -2.;
        return coeff;
    }

    sum1 = 0;
    sum2 = 0;
    sum3 = 0;
    mean_data = 0;
    mean_ref = 0;

    for(i = 0; i<length_set; i++){
        mean_data = mean_data + measured_data[i];
        mean_ref = mean_ref + reference_data[i];
    }
    mean_data = mean_data / length_set;
    mean_ref = mean_ref / length_set;

    for(i = 0; i<length_set; i++){
        sum1 = sum1 + (measured_data[i]-mean_data)*(reference_data[i]-mean_ref);
        sum2 = sum2 + (measured_data[i]-mean_data)*(measured_data[i]-mean_data);
        sum3 = sum3 + (reference_data[i]-mean_ref)*(reference_data[i]-mean_ref);
    }
    /*Check to eliminate div by 0*/
    fraction = sqrt(sum2*sum3);
    if(fraction != 0.)
    	coeff = sum1/sqrt(sum2*sum3);
    else
	    coeff = -1.;

    return coeff;
}

/**
 * @brief extract two single pixel rows from a given image in a cross shape
 *
 * Function used for the one-dimensional array extraction needed for the pearson
 * coefficient that is returned as the quality metric of the Centroid Validation
 *
 * @param data: array of input samples
 * @param[out] strip_x/strip_y: Pointers of the target strip arrays
 * @param dim_x/dim_y: size of data in x and y
 * @param length: length of the strips
 * @param x_center: estimated center in x
 * @param y_center: estimated center in y
 *
 */
void extract_strips(unsigned int *data, unsigned int *strip_x, unsigned int *strip_y, unsigned int dim_x, unsigned int dim_y, unsigned int length, float x, float y)
{
    unsigned int i, x_origin, y_origin, pos_x, pos_y;

    x_origin = floor(x)-floor(length/2);
    y_origin = floor(y)-floor(length/2);
    pos_x = x_origin;
    pos_y = y_origin;

    for(i = 0; i<length; i++){
        if((pos_x < 0)||(pos_x > (dim_x-1))){
            strip_x[i] = 0;
        }
        else{
            strip_x[i] = GET_PIXEL(data, dim_x, pos_x, (int)floor(y));
        }

        if((pos_y < 0)||(pos_y > (dim_y-1))){
            strip_y[i] = 0;
        }
        else{
            strip_y[i] = GET_PIXEL(data, dim_x, (int)floor(x), pos_y);
        }
        pos_x = pos_x + 1;
        pos_y = pos_y + 1;
    }
}

unsigned int binned_image[XDIM/VALBINNING*YDIM/VALBINNING];
unsigned int x_strip[25];
unsigned int y_strip[25];
unsigned int ref_x[25];
unsigned int ref_y[25];

/**
 * @brief make checks on the ROI to predict the validity of the centroid
 *
 * Function to analyze an image and return a code carrying the validity of the
 * centroid and the magnitude of the target star that will be calculated from
 * this image. The image is binned to 5x5 pixels, then the following checks are
 * carried out:
 *
 *   1. Check for constant image (is the minumum equal to the maximum?)
 *   2. Check for a star (the mean must be larger than the median)
 *   3. The star must be a distinct feature (the maximum should be larger than
 *      median + 2 sigma)
 *   4. The range of values must be larger than a certain span
 *   5. The sigma must be larger than a certain span
 *   6. The pearson correlation between the image and a reference function at
 *      the center must be larger than the defined value
 *   7. The measured signal of the target must not exeed the specified range
 *
 * The background that is subtracted from the image for the photometry is given
 * by the minimum pixel value inside of the ROI with the exception of known dead
 * pixels. If dead pixels are present, they shall be replaced by the minimum
 * value in prior steps.
 *
 * @param data: array of input samples
 * @param x_dim: size in x
 * @param y_dim: size in y
 * @param CenSignalLimit: threshold for 4
 * @param CenSigmaLimit: threshold for 5
 * @param PearsonLimit: threshold for 6
 * @param fgs: channel for reference function
 * @param x_center: estimated center in x
 * @param y_center: estimated center in y
 * @param signalTolerance: Tolerance range for 7
 * @param targetSignal: Signal for 7
 * @param acq: flag for target acquisition to avoid second signal check
 *
 * @returns struct containing the validity data of the centroid
 */

struct valpack CheckRoiForStar (unsigned int *data, unsigned int x_dim, unsigned int y_dim, unsigned int CenSignalLimit, float PearsonLimit, unsigned int fgs, double x_center, double y_center, unsigned int signalTolerance, unsigned int targetSignal, short acq)
{
    unsigned int i;
    unsigned int median, minimum, maximum;
    unsigned int binnedwidth, binnedheight, binx, biny, x, y, xstart, ystart, rad;
    float mean, sigma, xdist, ydist, pearson_x, pearson_y;
    unsigned int sum;
    int mag;
    struct valpack package;

    if ((x_dim > VALBINNING) && (y_dim > VALBINNING)){
        binnedwidth = x_dim / VALBINNING;
        binnedheight = y_dim / VALBINNING;
    }
    else{
        package.flag = 111;
        package.index = 0.;
        package.magnitude = 0;
        return package;
    }

    if ((binnedwidth == 0) || (binnedheight == 0)){
        package.flag = 111;
        package.index = 0.;
        package.magnitude = 0;
        return package;
    }

    for (i=0; i < (binnedwidth*binnedheight); i++){
        binned_image[i] = 0;
    }

    /* bin by 3x3 */
    for (biny = 0; biny < binnedheight; biny++){
        for (y = 0; y < VALBINNING; y++){
            for (binx = 0; binx < binnedwidth; binx++){
                for (x = 0; x < VALBINNING; x++){
                    xstart = x + binx*VALBINNING;
                    ystart = (y + biny*VALBINNING) * x_dim;
                    binned_image[binx + binnedwidth*biny] = binned_image[binx + binnedwidth*biny] + data[xstart + ystart];
                }
            }
        }
    }
    
    if (fgs == 1){
      rad = 7;
    }
    else{
      rad = 9;
    }
    sum = 0;

    
    /* convert the sums to averages */
    for (i=0; i < binnedwidth*binnedheight; i++){
        binned_image[i] = binned_image[i] / (VALBINNING * VALBINNING);
    }

    MeanSigma (binned_image, binnedwidth*binnedheight, &mean, &sigma);
    
    median = Median (binned_image, binnedwidth*binnedheight);

    MinMaxU32 (binned_image, binnedwidth*binnedheight, &minimum, &maximum);

    /* rule 1: the image must not be constant */
    if (minimum == maximum){
        package.flag = 101;
        package.index = 0.;
        package.magnitude = 0;
        return package;
    }

    /* rule 2: there must be a star */
    if (mean < median){
        package.flag = 102;
        package.index = 0.;
        package.magnitude = 0;
        return package;
    }

    /* rule 3: the star must be sharp */
    if ((median + 2*sigma) > maximum){
        package.flag = 103;
        package.index = 0.;
        package.magnitude = 0;
        return package;
    }
    
    /* rule 4: there must be a signal */
    if ((maximum - minimum) < CenSignalLimit){
        package.flag = 104;
        package.index = 0.;
        package.magnitude = 0;
        return package;
    }
 
    /*rule 6: the signal inside of a 5px radius circle around the estimated center must contain a certain percentage of the total signal*/
    for (y = 0; y < y_dim; y++){
        for (x = 0; x < x_dim; x++){
            /* calculate distance square to center */
            xdist = abs(x - x_center);
            ydist = abs(y - y_center);

            /* speed up */
            if ((xdist <= rad) && (ydist <= rad)){
                sum += GET_PIXEL(data, x_dim, x, y);
            }
        }
    }
    extract_strips(data, x_strip, y_strip, x_dim, y_dim, 25, x_center, y_center);

    mag = sum;
    if((abs(targetSignal - mag) > targetSignal*((float)signalTolerance/100)) && acq != 1){    
        package.flag = 107;
        package.index = 0.;
        package.magnitude = mag;
        return package;
    }
    /*Multiply with signal to scale reference to extracted sample*/

    if(fgs == 1){
      for(i = 0; i < 25; i++){
        ref_x[i] = (unsigned int) (FGS1_X[i]*mag);
        ref_y[i] = (unsigned int) (FGS1_Y[i]*mag);
      }
    }
    else{
      for(i = 0; i < 25; i++){
        ref_x[i] = (unsigned int) (FGS2_X[i]*mag);
        ref_y[i] = (unsigned int) (FGS2_Y[i]*mag);
      }
    }

    pearson_x = pearson_r(x_strip, ref_x, 25);
    pearson_y = pearson_r(y_strip, ref_y, 25);
 
    if ((pearson_x*pearson_y) < PearsonLimit || pearson_x <= 0.0 || pearson_y <= 0.0){
        package.flag = 106;
        package.index = 0.;
    }
    else{
        package.index = pearson_x*pearson_y;
        package.flag = 1;
    }
    package.magnitude = mag;
    return package;
}

/**
 * @biref Region of interest refinement for the centroiding algorithm
 *
 * The function takes the original image and extracts a smaller RoI starting
 * from the defined point
 *
 * @param image: buffer containing the original RoI
 * @param[out] region: buffer containing the RoI
 * @param width/height: dimensions of the image
 * @param target_x/target_y: upper left corner of the RoI
 * @param roi_size: size of the refined RoI
 *
 */
void refine_RoI (unsigned int *image, unsigned int *region, unsigned int width, unsigned int height, unsigned int target_x, unsigned int target_y, unsigned int roi_size)
{
    unsigned int x_ctr, y_ctr;

    for(x_ctr = 0; x_ctr < roi_size; x_ctr++){
        for (y_ctr = 0; y_ctr < roi_size; y_ctr++){
	        region[x_ctr + y_ctr*roi_size] = image[target_x+x_ctr + (target_y + y_ctr)*height];
        }
    }

    return;
}

/**
 * @brief Calculates Center of Gravity for a given 1d array in sub-element
 * accuracy
 *
 * @param img: a buffer holding the image
 * @param	rows: the number of rows in the image
 * @param	cols: the number of columns in the image
 * @param[out] x: x position
 * @param[out] y: y position
 *
 */
void CenterOfGravity2D (unsigned int *img, unsigned int rows, unsigned int cols, float *x, float *y)
{
    unsigned int i, j;
    unsigned long int tmp; /* was: double */

    double pos;
    double sum = 0.0;

    /*
       start by iterating columns, i.e. contiguous sections of memory,
       so the cache will be primed at least for small images for
       the random access of rows afterwards.
       Large images should be transposed beforehand, the overhead is a lot
       smaller that a series of cache misses for every single datum.
     */

    /* for the y axis */
    pos = 0.0;
    for (i = 0; i < rows; i++){
        tmp = 0;

        for (j = 0; j < cols; j++)
            tmp += GET_PIXEL(img, cols, j, i);

        pos += tmp * (i + 1);
        sum += tmp;
    }

    if (sum != 0.0)
        (*y) = (float)(pos / sum) - 1.0f;

    /* for the x axis */
    pos = 0.0;
    for (j = 0; j < cols; j++){
        tmp = 0;

        for (i = 0; i < rows; i++)
            tmp += GET_PIXEL(img, cols, j, i);

        pos += tmp * (j + 1);
    }

    if (sum != 0.0)
        (*x) = (float)(pos / sum) - 1.0f;

    return;
}


unsigned int tmpImg[2048*2048];
/**
 * @brief Calculates centroid using Weighted Center of Gravity for a 2d image
 *
 * In this algorithm, the inner product of img and weights goes into the
 * @ref CenterOfGravity2D algorithm. That algorithm sums up the pixel values in
 * an unsigned int, so the data and the dimensions that we pass must account for
 * that. Consequently, the range of values in our data array needs to be reduced
 * if they are too large. Remember, we will find the center of
 * gravity (position) from that data set, so multiplicative factors can be
 * ignored.
 *
 * @param img: a buffer holding the image
 * @param weights: a buffer holding the weights
 * @param	rows: the number of rows in the image
 * @param	cols: the number of columns in the image
 * @param[out] x: x position
 * @param[out] y: y position
 *
 * @note It is fine to work in place, i.e. img and weights can be identical.
 */
void WeightedCenterOfGravity2D (unsigned int *img, float *weights, unsigned int rows, unsigned int cols, float *x, float *y)
{
    unsigned int i;

    for (i = 0; i < rows * cols; i++){
        /* multiply image with weights */
        tmpImg[i] = (unsigned int) (weights[i]*img[i]);
    }

    /* determine size of datatype and shift so that it is back within CogBits (e.g. 16) bits */

    CenterOfGravity2D (tmpImg, rows, cols, x, y);

    return;
}

float weight[ROISIZE_COG*ROISIZE_COG];

/**
 * @brief Calculates centroid using Intensity Weighted Center of Gravity for
 * a 2d image
 *
 * In the IWC algorithm, the image is basically squared, before it goes into the
 * @ref CenterOfGravity2D. This is achieved by calling
 * @ref WeightedCenterOfGravity2D with img as data and weights parameter.
 *
 * @param img: a buffer holding the image
 * @param	rows: the number of rows in the image
 * @param	cols: the number of columns in the image
 * @param[out] x: x position
 * @param[out] y: y position
 *
 */
void IntensityWeightedCenterOfGravity2D (unsigned int *img, unsigned int rows, unsigned int cols, float *x, float *y)
{
    unsigned int i;
    /* the IWC just works on the square of the image */
    for(i = 0; i<rows*cols;i++){
        weight[i] = (float) img[i];
    }
    WeightedCenterOfGravity2D (img, weight, rows, cols, x, y);

    return;
}


float weights_cog[REFINEDROISIZE*REFINEDROISIZE];
unsigned int roi_cog[REFINEDROISIZE*REFINEDROISIZE];

/**
 * @brief Implementation of the ARIEL centroiding method for mode tracking
 *
 * The ARIEL centroiding method initially performs a
 * @ref IntensityWeightedCenterOfGravity2D on the image to obtain an initial
 * estimate for a @ref IterativelyWeightedCenterOfGravity2D with an iteration
 * count of 5. Before the IWCoG is performed, the RoI of the image is refined
 * in order to reduce the comutational cost of the algorithm.
 *
 * @param img: a buffer holding the image
 * @param	rows: the number of rows in the image
 * @param	cols: the number of columns in the image
 * @param iterations: number of iterations
 * @param fwhm_x: FWHM of gaussian weighting function in x
 * @param fwhm_y:  FWHM of gaussian weighting function in y
 * @param fgs:  FGS channel for approximated weighting function
 * @param CenSignalLimit: Signal limit for centroid validation
 * @param PearsonLimit: Pearson Coeff limit for centroid validation
 * @param acq: flag to indicate use in target acquisition
 *
 * @returns Centroid packet in form of coord struct
 */

struct coord ArielCoG (unsigned int *img, unsigned int rows, unsigned int cols, unsigned int iterations, float fwhm_x, float fwhm_y, int fgs, unsigned int CenSignalLimit, float PearsonLimit, unsigned int signalTolerance, unsigned int targetSignal, short acq)
{

    float x_res, y_res;
    float x_roi, y_roi;
    int x_start, y_start, x_shift, y_shift;
    unsigned int i;
    struct coord res;

    memset(&res, 0, sizeof(res));

    x_res = (float) rows / 2;
    y_res = (float) cols / 2;

    IntensityWeightedCenterOfGravity2D (img, rows, cols, &x_res, &y_res);

    x_start = (int) x_res - REFINEDROISIZE/2 + 1; /*FLP34-C violation due to execution time*/
    y_start = (int) y_res - REFINEDROISIZE/2 + 1; /*FLP34-C violation due to execution time*/


    x_roi = x_res - x_start;
    y_roi = y_res - y_start;

    for (i=0; i < iterations; i++){

        if (i != 0){
	        x_shift = (int) x_roi - REFINEDROISIZE/2 + 1;
	        y_shift = (int) y_roi - REFINEDROISIZE/2 + 1;

            x_start =  x_start + x_shift;
            y_start = y_start + y_shift;

	        x_roi = x_roi - x_shift;
	        y_roi = y_roi - y_shift;

        }

        if (x_start < 0){
            x_roi = x_roi + x_start;
            x_start = 0;
        }
        if (y_start < 0){
            y_roi = y_roi + y_start;
            y_start = 0;
        }
        if (x_start > (rows - REFINEDROISIZE)){
            x_roi = x_roi + x_start - (rows - REFINEDROISIZE);
            x_start = rows - REFINEDROISIZE;
        }
        if (y_start > (cols - REFINEDROISIZE)){
            y_roi = y_roi + y_start - (cols - REFINEDROISIZE);
            y_start = cols - REFINEDROISIZE;
        }
        refine_RoI (img, roi_cog, rows, cols, x_start, y_start, REFINEDROISIZE);
        get_2D_gaussian (weights_cog, REFINEDROISIZE, REFINEDROISIZE, x_roi, y_roi, fwhm_x, fwhm_y);
        
        
        WeightedCenterOfGravity2D (roi_cog, weights_cog, REFINEDROISIZE, REFINEDROISIZE, &x_roi, &y_roi);

    }

    res.x = x_start + x_roi;
    res.y = y_start + y_roi;

    
    res.validity = CheckRoiForStar (img, rows, cols, CenSignalLimit, PearsonLimit,fgs , res.x, res.y, signalTolerance, targetSignal, acq);

    return res;
}

unsigned int binned[XDIM/BIN * YDIM/BIN];
unsigned int roi[BIN*BIN*ROISIZE*ROISIZE];

/**
 * @brief source detection based on peak-finding
 *
 * This algorithm takes a given image and searches point sources based on a
 * simple peak-finding algorithm performs the ariel center of gravity algorithm
 * on a region of interest that is cut around the most likely target
 *
 * @param data: array of input samples
 * @param xdim/ydim: size of the image
 * @param fgs: FGS channel. needed for weighting function
 * @param target_number:  number of expected stars inside of the image
 * @param target_brightness: signal of the target star in ADU
 * @param brightness_tolerance: signal tolerance for target determination
 * @param sigma: min sigma value for extension checking
 * @param iter: iteration count of the CoG
 * @param fwhm_x/fwhm_y: size of the analyt. gaussian weighting func.
 * @param CenSignalLimit: Signal limit for centroid validation
 * @param PearsonLimit: Pearson Coeff limit for centroid validation
 *
 * @returns  coordiates of the target star
 */

struct coord SourceDetection(unsigned int *data, unsigned int xdim, unsigned int ydim, int fgs, unsigned int target_number, unsigned int target_brightness, unsigned int brightness_tolerance, unsigned int sigma, unsigned int iter, float fwhm_x, float fwhm_y, unsigned int CenSignalLimit, float PearsonLimit)
{
    unsigned int binnedwidth, binnedheight, binx, biny, x, y, xstart, ystart;
    unsigned int source_val[target_number];
    unsigned int source_pos[target_number];
    unsigned int source_br[target_number];
    int ext_ctr[target_number];
    int target[target_number];
    int pos_x = 0, pos_y = 0, flag;
    unsigned int tmp_br = 0, sum = 0, mean, i; /*tmp_br is a placeholder for the brightnes check*/
    struct coord result;
    unsigned int th = 0;

    flag = 0;

    if ((xdim > BIN) && (ydim > BIN)){
        binnedwidth = xdim / BIN;
        binnedheight = ydim / BIN;
    }
    else{
        result.x = 0;
        result.y = 0;
        result.validity.flag = 111;
        result.validity.index = 0.;
        return result;
    }

    if ((binnedwidth == 0) || (binnedheight == 0)){
        result.x = 0;
        result.y = 0;
        result.validity.flag = 111;
        result.validity.index = 0.;
        return result;
    }

    for (i=0; i < (binnedwidth*binnedheight); i++){
        binned[i] = 0;
    }

    for (i=0; i < target_number; i++){
        source_val[i] = 0;
        source_pos[i] = 0;
        source_br[i] = 0;
        ext_ctr[i] = 0;
        target[i] = 0;
    }

    /* bin by 3x3 */
    for (biny = 0; biny < binnedheight; biny++){
        for (y = 0; y < BIN; y++){
            for (binx = 0; binx < binnedwidth; binx++){
                for (x = 0; x < BIN; x++){
                    xstart = x + binx*BIN;
                    ystart = (y + biny*BIN) * xdim;
                    binned[binx + binnedwidth*biny] = binned[binx + binnedwidth*biny] + data[xstart + ystart];
                }
            }
        }
    }
   
    /* calc mean */
    for (i=0; i < (binnedwidth * binnedheight); i++){
        sum = sum + binned[i];
    }

    mean = (unsigned int) sum / (binnedwidth * binnedheight);

    find_brightest_uint(binned, (binnedwidth * binnedheight), target_number, source_val, source_pos);
    
    identify_star(binned, source_br, target, binnedwidth, binnedheight, source_pos, ext_ctr, target_number, mean + sigma, target_brightness);
    
    for(i=0; i<target_number;i++){
        if (target[i] == 1){
            if (source_br[i] > tmp_br && (abs(source_br[i]-target_brightness) < (int)(target_brightness*brightness_tolerance/100))){
                tmp_br = source_br[i];
                pos_x = (source_pos[i] % binnedwidth);
                pos_y = (source_pos[i] / binnedwidth);
                flag = 1;
            } 
        }
    }
    if(flag == 1){
        pos_x = pos_x - ROISIZE/2;
        pos_y = pos_y - ROISIZE/2;

        if (pos_x < 0){
            pos_x = 0;
        }
        if (pos_y < 0){
            pos_y = 0;
        }
        if (pos_x > (binnedwidth - ROISIZE)){
            pos_x = binnedwidth - ROISIZE;
        }
        if (pos_y > (binnedheight - ROISIZE)){
            pos_y = binnedheight - ROISIZE;
        }

        extract_RoI(data, xdim, ydim, pos_x, pos_y, roi);

        th = GetMin(roi, ROISIZE*BIN*ROISIZE*BIN);

        for (i=0; i < (ROISIZE*BIN*ROISIZE*BIN); i++){
            roi[i] = roi[i] - th;
        }
    
        result = ArielCoG(roi, ROISIZE*BIN, ROISIZE*BIN, iter, fwhm_x, fwhm_y, fgs, CenSignalLimit, PearsonLimit, brightness_tolerance, target_brightness, 1);
        result.x = result.x + (float) (pos_x*BIN);
        result.y = result.y + (float) (pos_y*BIN);
        
    }
    else{        
        for(i=0; i < target_number; i++){
            if(target[i] == 1){
                pos_x = (source_pos[i] % binnedwidth);
                pos_y = (source_pos[i] / binnedwidth);
                result.validity.magnitude = source_br[i];
                if(ext_ctr[i] <= 6){
                    result.validity.flag = 108;
                    result.validity.index = 0.;
                    break;
                }
                else{;
                    result.validity.flag = 107;
                    result.validity.index = 0.;
                    break;
                }
            }
            else{
                result.validity.flag = 104;
                result.validity.index = 0.;
                pos_x = 0;
                pos_y = 0;
                result.validity.magnitude = 0;
            }
        }
        result.x = (float) pos_x * BIN;
        result.y = (float) pos_y * BIN;
    }
    result.time = 0.;
    return result;
}

/**
 * @brief Function for correcting the pixels marked in the hot pixel map
 * 
 * @param image: input image to be corrected
 * @param map: Hot pixel map that shall be used
 * @param width: width of the image
 * @param height: height of the image
 * @param value: value used for marking hot pixels in the map
 * @return int: 0 on success
 */
int hotPixelCorrection (unsigned int *image, unsigned short *map, unsigned int width, unsigned int height, unsigned int value)
{
    unsigned int i, j, tmp;
    unsigned int medwin[9];

    for(i = 0; i < width; i++){
        for(j = 0; j < height; j++){
            if(map[i + width * j] == value){
                if(j == 0){
                    medwin[0] = 0;
                    medwin[1] = 0;
                    medwin[2] = 0;
                }
                else{
                    if(i == 0){
                        medwin[0] = 0;
                    }
                    else{
                        medwin[0] = image[(i-1) + width * (j-1)];
                    }
                    
                    medwin[1] = image[i + width * (j-1)];

                    if((i + 1) == width){
                        medwin[2] = 0;
                    }
                    else{
                        medwin[2] = image[(i+1) + width * (j-1)];
                    }
                }

                if((j + 1) == height){
                    medwin[5] = 0;
                    medwin[6] = 0;
                    medwin[7] = 0;
                }
                else{
                    if(i == 0){
                        medwin[5] = 0;
                    }
                    else{
                        medwin[5] = image[(i-1) + width * (j+1)];
                    }
                    
                    medwin[6] = image[i + width * (j+1)];

                    if((i + 1) == width){
                        medwin[7] = 0;
                    }
                    else{
                        medwin[7] = image[(i+1) + width * (j+1)];
                    }
                }
                if(i == 0){
                    medwin[3] = 0;
                    medwin[4] = image[(i+1) + width * j];
                }
                else if ((i + 1) == width){
                    medwin[3] = image[(i-1) + width * j];
                    medwin[4] = 0;
                }
                else{
                    medwin[3] = image[(i-1) + width * j];
                    medwin[4] = image[(i+1) + width * j];
                }

                medwin[8] = image[i + width * j];

                tmp = Med9USpoil(medwin);
                image[i + width * j] = tmp;
            }
        }
    }
    return 0;
}