/**
* @file     HFS_API.hpp
* @author   Gerald Mösenlechner (gerald.moesenlechner@univie.ac.at)
* @date     November, 2024
* @version  1.3.3
*
*
*
* ## Overview
* Header file for HFS FGS Simulator
*
*/
#include <tinyxml2.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stddef.h>
#include <limits>
#include <queue>
#include "./utilities.h"
#include "./detector_features.h"
#include "./fcu_algorithms.h"

#if 1
#pragma GCC optimize("O3","unroll-loops","omit-frame-pointer","inline")
#pragma GCC target("avx")
#endif


#ifndef HFS_API_H
#define HFS_API_H

#define XDIM 200
#define YDIM 200
#define XDIM_PSF 40
#define YDIM_PSF 40
#define XDIM_KERNEL 51
#define YDIM_KERNEL 51
#define TWO_PI 6.28318531f
#define CVEL 299792.458f //km/s


extern const char* MODE_ACQU;
extern const char* MODE_TRAC;
extern const char* MODE_STBY;


/**
 * @brief Struct used for debugging and testing, result of the HFS state getter
 *
 */
struct hfs_state
{
    double bias_value;
	double qe; 
	double read_noise;
    double dark_mean;
    double bkgr_noise;
    double gain;
    double exposure_time;
    double sim_time;
    double internal_time;
    double timestep;	
    double flat_mean;
    double flat_sigma;
    double plate_scale;
    double pearson_limit;
    double ps_fgs1;
    double ps_fgs2;
	double fl_fgs1;
    double fl_fgs2;
    double qe_fgs1;
    double qe_fgs2;
    double lower_sig_lim;
	double dim_x;
    double dim_y;
    double start_exposure;
    double focal_length;
    double delay;
    double x_smear_0;
    double y_smear_0;
    double jitter_error;
    double delay_track;
    double delay_acq;
    double transition_delay_channel;
    double transition_delay_mode;
    double transition_end;
    double exp_track;
    double exp_acq;
    double reset_duration;
    double reset_end;
    double timing_tolerance;
	unsigned int channel;
    unsigned int iter_track;
    unsigned int iter_acq;
    unsigned int med_track;
    unsigned int tolerance_track;
    unsigned int med_acq;
    unsigned int threshold_track;
	unsigned int threshold_acq;
    unsigned int tolerance_acq;
    unsigned int target_signal;
    unsigned int full_well_cap;
    unsigned int med_threshold;
    unsigned int sync_ctr;
    unsigned int track_dim;
    unsigned int acq_dim_fgs1;
    unsigned int acq_dim_fgs2;
    unsigned int rand_seed;
    unsigned int fwhm1_x;
    unsigned int fwhm1_y;
    unsigned int fwhm2_x;
    unsigned int fwhm2_y;
    unsigned int fwhm_x;
    unsigned int fwhm_y;
    unsigned int max_targets;
    unsigned int extension_sigma;
    unsigned int ramp_length;
    unsigned int ramp_counter;
    unsigned int sat_counter;
    unsigned int sat_limit;
	double offset_FGS1[2];
    double offset_FGS2[2];
	double offset[2];
	int mode;
    int target_pos_x;
    int target_pos_y;
	std::string psf_fgs1;
	std::string psf_fgs2;
	std::string input_file;
	std::string catalogue;
	struct stars* targets = new stars();
};

/**
 * @brief Struct used for the updating of the hfs state
 *
 * @param position_quat: quaternion for the attitude of the spacecraft
 * @param	ang_rate: angular rate of the spacecraft in sc reference frame [arcsec/s]
 * @param   scVelocity: spacecraft velocity vector in SC reference frame [km/s]
 * @param time: current simulation time;
 * @param	sync_flag: synchronisation flag for HFS
 * @param	channel: chosen channel for FGS
 * @param	save: flag for saving the current state of the HFS
 * @param	reset: flag for reseting the HFS
 * @param	set_invalid: flag for setting the validity flag to false
 * @param validation_signal: Target Signal used for validation and identification
 * @param	set_error: Error code that will be set in the centroid, at 0, the HFS will use the code given by the validation
 * @param	mode: operational mode of the FGS 0: Standby, 1: Acquisition, 2: Tracking
 * @note Operational modes are integers as Simulink doesn't support c
 */
typedef struct{
	double position_quat[4];
	double ang_rate[3];
    double scVelocity[3];
	double time;
	unsigned int channel;
	unsigned int save;
	unsigned int reset;
	unsigned int set_invalid;
	unsigned int validation_signal;
	unsigned int set_error;
	unsigned int mode;
	int add_shift_x;
	int add_shift_y;
    int mult_shift_x;
    int mult_shift_y;
    int target_pos_x;
    int target_pos_y;
} hfs_parameters;

/**
 * @brief Struct used as the output of the HFS
 *
 * @param x,y: Position measurement in mas
 * @param	time: time tag of the measurement
 * @param	integration_start: time tag of the beginning of the integration
 * @param	validity_flag: Flag for the validity of the measurement
 * @param	validity_index: quality index of the measurement (-1 to 1 or error code)
 * @param	magnitude: signal of the measured star in ADU
 * @param	channel: configured channel
 * @param	mode: configured operational mode
 */

typedef struct{
    double xErr;
    double yErr;
} centroidError;

typedef struct{
    double x;
    double y;
    double time;
    double integration_start;
    int validity_flag;
    double validity_index;
    unsigned int magnitude;
    unsigned int channel;
    unsigned int mode;
    unsigned int failedState;
    double xErr;
    double yErr;
}outputHfs;

int search_star_id(struct stars*, const char*);

int multiply_quaternion(double (&)[4], double (&)[4], double (&)[4]);

int generate_shotnoise(double*, double*, double*, unsigned int);

void apply_aberration(double* ,double*);
/**
 * /class FGS
 * @brief The FGS class provides the main interface for the HFS.
 *
 * The FGS class funtions as the main interface of the HFS and contains all
 * methos for mode transitions, coordinate transformations, image simulation and
 * execution of FGS algorithms.
 *
 * FGS methods:
 *
 * 		reset_fgs: reset FGS to original state
 * 		generate_smear: update smearing kernel based on angular rate
 * 		generate_image: generate image based on current state
 * 		perform_algorithm: applies configured algorith to current image
 * 		set_params: main interface for HFS
 * 		set_channel: setter for FGS channel
 * 		set_mode: setter for FGS mode
 * 		generate_config: generate xml from state
 * 		get_hfs_state: getter for HFS state
 * 		reset_arrays: reset image buffers
 * 		transform_to_detector: transform vector from SC to DRF
 * 		transform_star_coordinates: transform star coordiantes from J2000 to DRF
 *
 */
class FGS{
    double bias_value;
	double qe; 
	double read_noise;
    double dark_mean;
    double bkgr_noise;
    double gain;
    double exposure_time;
    double sim_time;
    double internal_time;
    double timestep;	
    double flat_mean;
    double flat_sigma;
    double plate_scale;
    double pearson_limit;
    double ps_fgs1;
    double ps_fgs2;
	double fl_fgs1;
    double fl_fgs2;
    double qe_fgs1;
    double qe_fgs2;
    double lower_sig_lim;
	double dim_x;
    double dim_y;
    double start_exposure;
    double focal_length;
    double delay_mean;
    double delay_std;
    double delay_max;
    double delay_min;
    double x_smear_0;
    double y_smear_0;
    double jitter_error;
    double delay_mean_track;
    double delay_std_track;
    double delay_max_track;
    double delay_min_track;
    double delay_mean_acq;
    double delay_std_acq;
    double delay_max_acq;
    double delay_min_acq;
    double transition_delay_mode;
    double transition_delay_channel;
    double transition_end;
    double exp_track;
    double exp_acq;
    double reset_duration;
    double reset_end;
    double timing_tolerance;
    double timing_jitter;
    double timing_drift;
    double reset_noise;
    double fov_radius;
    int trf_x;
    int trf_y;
	unsigned int channel;
    unsigned int iter_track;
    unsigned int iter_acq;
    unsigned int med_track;
    unsigned int tolerance_track;
    unsigned int med_acq;
    unsigned int threshold_track;
	unsigned int threshold_acq;
    unsigned int tolerance_acq;
    unsigned int target_signal;
    unsigned int full_well_cap;
    unsigned int med_threshold;
    unsigned int sync_ctr;
    unsigned int track_dim;
    unsigned int acq_dim_fgs1;
    unsigned int acq_dim_fgs2;
    unsigned int rand_seed;
    unsigned int fwhm1_x;
    unsigned int fwhm1_y;
    unsigned int fwhm2_x;
    unsigned int fwhm2_y;
    unsigned int fwhm_x;
    unsigned int fwhm_y;
    unsigned int max_targets;
    unsigned int extension_sigma;
    unsigned int failed;
    unsigned int ramp_length;
    unsigned int ramp_counter;
    unsigned int max_ramp;
    unsigned int sat_counter;
    unsigned int sat_limit;
	double offset_FGS1[2];
    double offset_FGS2[2];
	double offset[2];
	const char* mode;
	std::string psf_fgs1;
	std::string psf_fgs2;
	std::string input_file;
	std::string catalogue;
    std::string reference_data_fgs1;
    std::string reference_data_fgs2;
    std::string hp_map_fgs1;
    std::string hp_map_fgs2;
	struct stars* targets = new stars();
    std::deque<struct coord> centroid_queue;
    centroidError error;

public:

	FGS(const char* config_file){
		reset_fgs(config_file, 1);

	};

	int reset_fgs(const char*, int);

	int generate_smear(double (&)[3], double);

	int generate_image();

	struct coord perform_algorithm();

	int set_params(hfs_parameters, outputHfs*);

	int set_channel(unsigned int);

	int set_mode(const char*);

	int generate_config(const char*);

	struct hfs_state get_hfs_state();

	int reset_arrays(int);

	int transform_to_detector(double (&)[3], double (&)[3], double *);

	int transform_star_coordinates(double (&)[4], double(&)[3]);

	int generate_centroid(hfs_parameters);

	int send_centroid(hfs_parameters, outputHfs *);

    int calc_error(double, double);

    int reset_ramp();

    int calculate_ramp_length();

    ~FGS(){
        delete targets;
    };
};

extern FGS *fgsSim;

extern void createFGS(const char*);

extern void deleteFGS();

extern void updateFGS(hfs_parameters*, outputHfs*);

bool is_empty(std::ifstream&);
#endif