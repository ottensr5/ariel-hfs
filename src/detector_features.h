#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <string.h>
#include <complex.h>
#include <float.h>
#include <fftw3.h>
#include <stdbool.h>
#include "./utilities.h"

#ifndef DETECTOR_FEATURE_LIB_H
#define DETECTOR_FEATURE_LIB_H
#ifdef __cplusplus
extern "C" {
#endif


struct stars {
  char id[2500][15];
  double signalFGS1[2500];
  double signalFGS2[2500];
  double x[2500];
  double y[2500];
  double ra[2500];
  double dec[2500];
  short visible[2500];
  unsigned int number;
};

void generate_bias(double *, double *, double, unsigned int, unsigned int, unsigned int);

void generate_dark(double *, double, double, unsigned int, unsigned int);

void generate_hotpixels(double *, double, double, double, unsigned int, unsigned int);

void generate_flat(double *, double, double, unsigned int, unsigned int);

void generate_starmask(double *, struct stars, double, unsigned int, unsigned int, unsigned int);

void generate_star_image(double *, double *, double *, int, int, int, int);

void generate_background(double *, double, double, double, int, int);

void smear_star_image(double *, double *, double*, unsigned int, unsigned int, unsigned int, unsigned int);

int generate_linear_smearing_kernel(double *, double, double,  double, double, unsigned int);

void generate_shot(double *, double *, unsigned int, unsigned int);

void shift_smear(double*, double, double, unsigned int);

void generate_fov_mask(double*, double, double, unsigned int, unsigned int);

void apply_fov_mask(double*, double*, unsigned int, unsigned int, unsigned int, unsigned int, int, int);

void get_hp(double*, double*, unsigned int, unsigned int, unsigned int, unsigned int, int, int);

void get_calibration(unsigned short*, unsigned short*, unsigned int, unsigned int, unsigned int, unsigned int, int, int);

#ifdef __cplusplus
}
#endif

#endif //DATASIM_UTILITY_LIB_H
