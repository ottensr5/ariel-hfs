/**
* @file    utilities.c
* @author  Gerald Mösenlechner (gerald.moesenlechner@univie.ac.at)
* @date    November, 2024
* @version 1.3.3
*
* @brief Utility functions for the data simulator for the ARIEL space mission.
*
*
* ## Overview
* C library containing utility functions such as random number generation, image convolution and image transformations
*
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <string.h>
#include <float.h>
#include <complex.h>
#include <fftw3.h>
#include <stdbool.h>
#include <fenv.h>
#include "./utilities.h"

#define SET_PIXEL(img,cols,x,y,val) ( img[x + cols * y] = val )
#define GET_PIXEL(img,cols,x,y) ( img[x + cols * y] )
#define TWO_PI 6.28318531f
#define SIGDIVFWHM 0.42466452f

#if 1
#pragma GCC optimize("O3","unroll-loops","omit-frame-pointer","inline")
#pragma GCC target("avx")
#endif

/**
 * @brief Random poisson number generator based on the Transformed Rejection Method from Wolfgang Hoermann
 *
 * Algorithm for creating an array filled with random numbers following a poisson distribution
 *
 * @param mean: mean value of the distribution
 * @param size: size of the created array
 *
 * @return Array contaning random numbers
 */

void random_poisson_trm(double *matrix, double mean, unsigned int size)
{
    unsigned int i;
    double k;
    double U, V, slam, loglam, a, b, invalpha, vr, us, enlam, prod, X;

    slam = sqrt(mean);
    loglam = log(mean);
    b = 0.931 + 2.53*slam;
    a = -0.059 + 0.02483*b;
    invalpha = 1.1239 + 1.1328/(b-3.4);
    vr = 0.9277 - 3.6224/(b-2);

		if (mean >= 10)
		{
			for (i=0; i<size; )
	    {
	        while (1) {
	            U = (((double) random()) / INT_MAX) - 0.5;
	            V = (((double) random()) / INT_MAX);
	            us = 0.5 - fabs(U);
							if (us == 0.0)
									break;
	            k = (double) ((2 * a / us + b) * U + mean + 0.43);
	            if ((us >= 0.07) && (V <= vr)) {
	                matrix[i] = k;
	                i+=1;
	                break;
	            }
	            if ((k < 0) ||
	                ((us < 0.013) && (V > us))) {
	                continue;
	            }
	            if ((log(V) + log(invalpha) - log(a / (us * us) + b)) <=
	                (-mean + k * loglam - lgamma(k + 1))) {
	                matrix[i] = k;
	                i+=1;
	                break;
	            }
	        }
	    }
		}
		else
		{
			enlam = exp(-mean);
			for (i=0; i<size;)
			{
				X = 0;
				prod = 1.0;
				while (1)
				{
					U = (((double) random()) / INT_MAX);
					prod *= U;
					if (prod > enlam) {
						X += 1;
					}
					else
					{
						matrix[i] = X;
						i+=1;
						break;
					}
				}
				if(i == INT_MAX)
					break;
			}
		}
}


/**
 * @brief Random normal distributed number generator based on the Box-Muller transform
 *
 * Algorithm for creating an array filled with random numbers following a normal distribution
 *
 * @param mean: mean value of the distribution
 * @param	sigma: sigma of the distribution
 * @param size: size of the created array
 *
 * @return Array contaning random numbers
 */

void random_normal_trm(double *matrix, double mean, double sigma, unsigned int size)
{
    unsigned int i;
    double u0, u1;
		bool gen = true;


		for(i = 0; i<size; i++) {
			if(!gen){
				matrix[i] = (sqrt(-2. * log(u0)) * cos(2 * TWO_PI * u1)) * sigma + mean;
			}
			else{
				u0 = (random() + 1.0) / (RAND_MAX + 2.0);
    		u1 =  random()        / (RAND_MAX + 1.0);

				matrix[i] = (sqrt(-2. * log(u0)) * sin(2 * TWO_PI * u1)) * sigma + mean;
			}
			gen = !gen;
		}

}

double random_normal_number(double mean, double sigma)
{
	double z0, u0, u1, val;
	static double z1;
	static bool gen = true;
	const double eps = DBL_MIN;

	if(!gen){
		val = z1 * sigma + mean;
	}
	else{
		do {
			u0 = random() * (1. / RAND_MAX);
			u1 = random() * (1. / RAND_MAX);

		} while (u0 <= eps);

		z0 = sqrt(-2. * log(u0)) * cos(TWO_PI * u1);
		z1 = sqrt(-2. * log(u0)) * sin(TWO_PI * u1);
		val = z0 * sigma + mean;
	}
	gen = !gen;
	return val;
}

/**
 * @brief simple upsampling of an image
 *
 * @param image: Original image
 * @param height/width: dimension of the original image
 * @param os: Upsampling factor
 * @param copy: Flag if the values should be split over the upsampled pixel
 *
 * @returns Upsampled image
 *
 */

int upsample_image(double *image, double *upsampled_image, unsigned int width, unsigned int height, unsigned int width_new, unsigned int height_new, unsigned int os, int copy)
{
	unsigned int x, y, i, j, k, l;
	unsigned int x_tmp, y_tmp;
	double value;
	unsigned int splitter;
	if (os == 0)
		return -1;

	if (width*os > width_new)
		return -1;

	if (height*os > height_new)
		return -1;

	if(copy == 0){
		splitter = os*os;
	}
	else{
		splitter = 1;
	}

	for(i=0; i < width; i++){
		for(j=0; j < height; j++){
			value = image[i+width*j]/splitter;
			x = i*os;
			y = j*os;
			for(k=0; k<os; k++){
				for(l=0; l<os; l++){
					x_tmp = x+k;
					y_tmp = y+l;
					SET_PIXEL(upsampled_image, width * os, x_tmp, y_tmp, value);
				}
			}
		}
	}

	return 0;
}

/**
 * @brief simple downsampling of an image with normalization
 *
 * @param image: Original image
 * @param[out] downsampled_image: buffer of downsampled image
 * @param height/width: dimension of the original image
 * @param os: downsampling factor
 *
 */

int downsample_image(double *image, double *downsampled_image , unsigned int width, unsigned int height, unsigned int os)
{
	unsigned int x, y, i, j, k, l, x_tmp, y_tmp;
	unsigned int width_new, height_new;
	double value, tmp, sum;
	sum = 0;
	if (os == 0)
		return -1;

	width_new = (unsigned int) floor(width/os);
	height_new = (unsigned int) floor(height/os);

	for(i=0; i < width_new; i++){
		for(j=0; j < height_new; j++){
			value = 0;
			x = i*os;
			y = j*os;
			for(k=0; k<os; k++){
				for(l=0; l<os; l++){
					x_tmp = x+k;
					y_tmp = y+l;

					tmp = GET_PIXEL(image, width, x_tmp, y_tmp);
					value = value + tmp;
				}
			}
			downsampled_image[i + width_new*j] = value;
			sum = sum + value;
		}
	}
    if(sum > 0.){
	    for (i = 0; i < width_new * height_new; i++){
		    downsampled_image[i] = downsampled_image[i]/sum;
	    }
    }
	return 0;
}

/**
 * @brief dot product
 *
 * @param vec1/vec2: double vectors
 * @param n: size of the vectors
 * @returns dot product
 *
 */

double dot(double* vec1, double* vec2, unsigned int n)
{
	double res = 0.;
	unsigned int i;

	for(i = 0; i < n; i++)
	{
		res += vec1[i]*vec2[i];
	}

	return res;
}

int cross3(double* vec1, double* vec2, double* res)
{

	res[0] = vec1[1]*vec2[2] - vec1[2]*vec2[1];
	res[1] = vec1[2]*vec2[0] - vec1[0]*vec2[2];
	res[2] = vec1[0]*vec2[1] - vec1[1]*vec2[0];

	return 0;
}


int dmatmult (struct dmatrix a, struct dmatrix b, struct dmatrix *result)
{

    int z, s, i;

    if (a.xdim == b.ydim)
    {
			result->xdim = b.xdim;
			result->ydim = a.ydim;

			for (i=0; i < result->xdim * result->ydim; i++)
	    	result->data[i] = 0;

			for (z=0; z < result->ydim; z++)
				for (s=0; s < result->xdim; s++)
					for (i=0; i < b.ydim; i++)
					{
						result->data[z*result->xdim + s] += a.data[z*a.xdim + i] * b.data[i*b.xdim + s];
					}
            return 0;
    }
    else
    {
			fprintf (stderr, "ERROR: matrices are incompatible!\n");
            return -1;
    }
    return -1;
}
