import numpy as np

def createDefectPixels(xDim, yDim, n, max, min, value, bias):
    hp = np.ones((xDim, yDim))
    ref = np.full_like(hp, bias)
    for i in range(n):
        x = np.random.randint(0, xDim)
        y = np.random.randint(0, yDim)
        hp[x, y] = np.random.uniform(min, max)
        ref[x, y] = value

    return hp, ref

if __name__ == "__main__":
    hotPixels, Calibration = createDefectPixels(200, 200, 37, 100, 0, 0xFFFF, 2000)

    np.savetxt('hotPixels.txt', hotPixels.ravel(), fmt='%f')
    np.savetxt('Calibration.txt', Calibration.ravel(), fmt='%i')
