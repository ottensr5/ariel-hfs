"""
file    calculate_quaternion.py
author  Gerald Mösenlechner (gerald.moesenlechner@univie.ac.at)
date    June, 2022

Copyright
---------
This program is free software; you can redistribute it and/or modify it
under the terms and conditions of the GNU General Public License,
version 2, as published by the Free Software Foundation.

This program is distributed in the hope it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

Brief
-----
Small command line tool for calculation of a quaternion

Overview
--------
This python script takes the star-name as given in the catalouge as a command
line input and calculates the quaternion needed to place the star in the center
of the detector. Requires pandas and numpy
"""

import numpy as np
import pandas as pd
import sys

def dot(vec1, vec2):
    if len(vec1) != len(vec2):
        raise Exception("Vectors must have the same size")
    sum = 0
    for i in range(len(vec1)):
        sum += vec1[i]*vec2[i]

    return sum

def cross3d(vec1, vec2):
    if len(vec1) != len(vec2):
        raise Exception("Vectors must have the same size")

    output = np.zeros(3)
    output[0] = vec1[1]*vec2[2] - vec1[2]*vec2[1]
    output[1] = vec1[2]*vec2[0] - vec1[0]*vec2[2]
    output[2] = vec1[0]*vec2[1] - vec1[1]*vec2[0]

    return output

def norm(vec):

    sum = 0
    for i in range(len(vec)):
        sum += vec[i]**2

    return np.sqrt(sum)


def mult_quaternion(q1, q2):

    q_out = np.zeros(4)

    q_out[0] = q1[0]*q2[0] - dot(q1,q2)
    vec = q1[0]*q2[1:] + q2[0]*q1[1:] + cross3d(q1[1:], q2[1:])

    q_out[1] = vec[0]
    q_out[2] = vec[1]
    q_out[3] = vec[2]

    return q_out


def calc_quaternion(star, sc):

    quaternion_vector = cross3d(sc, star)

    quaternion = np.zeros(4)

    quaternion[0] = norm(sc)*norm(star)+dot(sc, star)
    quaternion[1] = quaternion_vector[0]
    quaternion[2] = quaternion_vector[1]
    quaternion[3] = quaternion_vector[2]

    quaternion = quaternion/norm(quaternion)

    return quaternion

if __name__ == "__main__":
    star_cat = pd.read_csv("./src/Star_catalogue.csv")
    id = sys.argv[1]

    star = star_cat.loc[star_cat['Name'] == id]
    print("Quaternion for Star:", id)
    star.dec = star.dec
    star.ra = star.ra

    star_vec = np.array((np.cos(star.dec*(np.pi/180))*np.cos(star.ra*(np.pi/180)), np.cos(star.dec*(np.pi/180))*np.sin(star.ra*(np.pi/180)), np.sin(star.dec*(np.pi/180))))

    sc_vec = np.array((0, 0, 1)) #target sc-vector

    quat = calc_quaternion(sc_vec, star_vec)

    print("Quaternion:", quat[0], ",",quat[1], ",",quat[2], ",",quat[3] )
    
    print("Expected Signals:")
    print("\tFGS1:")
    print("\t\tTracking:", float(star.FGS1*0.125), "ADU")
    print("\t\tAcquisition:", float(star.FGS1*0.5), "ADU")
    print("\tFGS2:")
    print("\t\tTracking:", float(star.FGS2*0.125), "ADU/s")
    print("\t\tAcquisition:", float(star.FGS2*0.5), "ADU/s")
