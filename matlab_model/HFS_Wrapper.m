%% Example Simulink S-Function block of the HFS using the legacy code tool.
% The function block will have an input and an output bus that are based on the
% hfs_parameters and centroid_packet structs definded in the HFS_API. The buses
% are imported from HFS_bus.mat. The definition of the methos 'createFGS()',
% 'updateFGS' and 'deleteFGS' are given in HFS_Wrapper.cpp and HFS_Wrapper.hpp.
% Note that this example was tested in Matlab Version 2019a under Ubuntu 20.4
% and might need modification of include paths to work.

    
sys = 'FGSBlock';
new_system(sys) % Create the model
open_system(sys) % Open the model

evalin('base','load HFS_bus.mat');
def = legacy_code('initialize');
def.SFunctionName = 'FGS_HFS';
def.StartFcnSpec  = 'createFGS()';
def.OutputFcnSpec = 'void updateFGS(hfs_parameters u1[1], outputHfs y1[1])';
def.TerminateFcnSpec = 'deleteFGS()';
def.HeaderFiles   = {'HFS_Wrapper.hpp'};
def.SourceFiles   = {'HFS_Wrapper.cpp'};
def.IncPaths      = {'/usr/include', '../src'};
def.SrcPaths      = {'../src/'};
def.HostLibFiles  = {'../src/libHFS_API.so'}
def.Options.language = 'C++';
def.Options.useTlcWithAccel = false;

%importInfo = Simulink.importExternalCTypes('../src/HFS_API.hpp', 'Names', {'hfs_parameters','centroid_packet'});

legacy_code('generate_for_sim', def);

legacy_code('rtwmakecfg_generate', def);

legacy_code('slblock_generate', def, sys);

x = 30;
y = 30;
w = 30;
h = 30;
yStart = y;
offset = 40;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/mode'],'Position',pos);
set_param([sys '/mode'], 'OutDataTypeStr','uint32');
set_param([sys '/mode'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/set_invalid'],'Position',pos);
set_param([sys '/set_invalid'], 'OutDataTypeStr','uint32');
set_param([sys '/set_invalid'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/set_error'],'Position',pos);
set_param([sys '/set_error'], 'OutDataTypeStr','uint32');
set_param([sys '/set_error'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/reset'],'Position',pos);
set_param([sys '/reset'], 'OutDataTypeStr','uint32');
set_param([sys '/reset'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/save'],'Position',pos);
set_param([sys '/save'], 'OutDataTypeStr','uint32');
set_param([sys '/save'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/channel'],'Position',pos);
set_param([sys '/channel'], 'OutDataTypeStr','uint32');
set_param([sys '/channel'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/time'],'Position',pos);
set_param([sys '/time'], 'OutDataTypeStr', 'double');
set_param([sys '/time'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/scVelocity'],'Position',pos);
set_param([sys '/scVelocity'], 'OutDataTypeStr','double');
set_param([sys '/scVelocity'], 'PortDimensions', num2str(3))
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
posHFS = [x+100 y+h/4 x+250 y+h*.75];
posOut = [x+300 y+h/4 x+330 y+h*.75];
xOut = x + 300;
yOut = y;
set_param([sys '/FGS_HFS'],'Position',posHFS);
add_block('built-in/Inport',[sys '/ang_rate'],'Position',pos);
set_param([sys '/ang_rate'], 'OutDataTypeStr','double');
set_param([sys '/ang_rate'], 'PortDimensions', num2str(3));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/position_quat'],'Position',pos);
set_param([sys '/position_quat'], 'OutDataTypeStr','double');
set_param([sys '/position_quat'], 'PortDimensions', num2str(4));y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/validation_signal'],'Position',pos);
set_param([sys '/validation_signal'], 'OutDataTypeStr','uint32');
set_param([sys '/validation_signal'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/add_shift_x'],'Position',pos);
set_param([sys '/add_shift_x'], 'OutDataTypeStr','int32');
set_param([sys '/add_shift_x'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/add_shift_y'],'Position',pos);
set_param([sys '/add_shift_y'], 'OutDataTypeStr','int32')
set_param([sys '/add_shift_y'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/mult_shift_x'],'Position',pos);
set_param([sys '/mult_shift_x'], 'OutDataTypeStr','int32')
set_param([sys '/mult_shift_x'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/mult_shift_y'],'Position',pos);
set_param([sys '/mult_shift_y'], 'OutDataTypeStr','int32')
set_param([sys '/mult_shift_y'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/target_pos_x'],'Position',pos);
set_param([sys '/target_pos_x'], 'OutDataTypeStr','int32')
set_param([sys '/target_pos_x'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x y+h/4 x+w y+h*.75];
add_block('built-in/Inport',[sys '/target_pos_y'],'Position',pos);
set_param([sys '/target_pos_y'], 'OutDataTypeStr','int32')
set_param([sys '/target_pos_y'], 'PortDimensions', num2str(1));
y = y + offset;

pos = [x+60 yStart-15 x+65 y+5];
add_block('simulink/Commonly Used Blocks/Bus Creator', [sys '/BC1'],'Position',pos)
set_param([sys '/BC1'],'Inputs','17')
set_param([sys '/BC1'],'OutDataTypeStr','Bus: hfs_parameters');

posOut = [xOut yOut-155+h/4 xOut+5 yOut+155+h*.75];
add_block('simulink/Commonly Used Blocks/Bus Selector', [sys '/BS1'],'Position',posOut)
set_param([sys '/BS1'],'OutputSignals','x,y,time,integration_start,validity_flag,validity_index,magnitude,channel,mode,failedState,xErr,yErr')

xOut = xOut + 100;

posOut = [xOut yOut-155+h/4 xOut+5 yOut+60+h*.75];
add_block('simulink/Commonly Used Blocks/Bus Creator', [sys '/BC2'],'Position',posOut)
set_param([sys '/BC2'],'Inputs','9')
set_param([sys '/BC2'],'OutDataTypeStr','Bus: centroid_packet');

posOut = [xOut yOut+115+h/4 xOut+5 yOut+160+h*.75];
add_block('simulink/Commonly Used Blocks/Bus Creator', [sys '/BC3'],'Position',posOut)
set_param([sys '/BC3'],'Inputs','2')
set_param([sys '/BC3'],'OutDataTypeStr','Bus: centroidError');

xOut = xOut + 100;

posOut = [xOut yOut-45+h/4 xOut+30 yOut-45+h*.75];
add_block('built-in/Outport',[sys '/centroid_packet'], 'Position', posOut);
set_param([sys '/centroid_packet'], 'OutDataTypeStr','Bus: centroid_packet')

posOut = [xOut yOut+90+h/4 xOut+30 yOut+90+h*.75];
add_block('built-in/Outport',[sys '/failedState'], 'Position', posOut);
set_param([sys '/failedState'], 'OutDataTypeStr','uint32')

posOut = [xOut yOut+135+h/4 xOut+30 yOut+135+h*.75];
add_block('built-in/Outport',[sys '/error'], 'Position', posOut);
set_param([sys '/error'], 'OutDataTypeStr','Bus: centroidError')

modeHandle = add_line(sys, 'mode/1', 'BC1/1');
set_param(modeHandle, 'Name', 'mode');
invalidHandle = add_line(sys, 'set_invalid/1', 'BC1/2');
set_param(invalidHandle, 'Name', 'set_invalid');
errorHandle = add_line(sys, 'set_error/1', 'BC1/3');
set_param(errorHandle, 'Name', 'set_error');
resetHandle = add_line(sys, 'reset/1', 'BC1/4');
set_param(resetHandle, 'Name', 'reset');
saveHandle = add_line(sys, 'save/1', 'BC1/5');
set_param(saveHandle, 'Name', 'save');
channelHandle = add_line(sys, 'channel/1', 'BC1/6');
set_param(channelHandle, 'Name', 'channel');
timeHandle = add_line(sys, 'time/1', 'BC1/7');
set_param(timeHandle, 'Name', 'time');
velHandle = add_line(sys, 'scVelocity/1', 'BC1/8');
set_param(velHandle, 'Name', 'scVelocity');
rateHandle = add_line(sys, 'ang_rate/1', 'BC1/9');
set_param(rateHandle, 'Name', 'ang_rate');
quatHandle = add_line(sys, 'position_quat/1', 'BC1/10');
set_param(quatHandle, 'Name', 'position_quat');
valHandle = add_line(sys, 'validation_signal/1', 'BC1/11');
set_param(valHandle, 'Name', 'validation_signal');
addXHandle = add_line(sys, 'add_shift_x/1', 'BC1/12');
set_param(addXHandle, 'Name', 'add_shift_x');
addYHandle = add_line(sys, 'add_shift_y/1', 'BC1/13');
set_param(addYHandle, 'Name', 'add_shift_y');
multXHandle = add_line(sys, 'mult_shift_x/1', 'BC1/14');
set_param(multXHandle, 'Name', 'mult_shift_x');
multYHandle = add_line(sys, 'mult_shift_y/1', 'BC1/15');
set_param(multYHandle, 'Name', 'mult_shift_y');
targetXHandle = add_line(sys, 'target_pos_x/1', 'BC1/16');
set_param(targetXHandle, 'Name', 'target_pos_x');
targetYHandle = add_line(sys, 'target_pos_y/1', 'BC1/17');
set_param(targetYHandle, 'Name', 'target_pos_y');
add_line(sys, 'BC1/1', 'FGS_HFS/1');
add_line(sys, 'FGS_HFS/1', 'BS1/1');

centroidXHandle = add_line(sys, 'BS1/1', 'BC2/1');
centroidYHandle = add_line(sys, 'BS1/2', 'BC2/2');
centroidTimeHandle = add_line(sys, 'BS1/3', 'BC2/3');
centroidIntStartHandle = add_line(sys, 'BS1/4', 'BC2/4')
centroidFlagHandle = add_line(sys, 'BS1/5', 'BC2/5');
centroidIndexHandle = add_line(sys, 'BS1/6', 'BC2/6');
centroidMagHandle = add_line(sys, 'BS1/7', 'BC2/7');
centroidChannelHandle = add_line(sys, 'BS1/8', 'BC2/8');
centroidModeHandle = add_line(sys, 'BS1/9', 'BC2/9');

add_line(sys, 'BC2/1', 'centroid_packet/1');

targetFailedStateHandle = add_line(sys, 'BS1/10', 'failedState/1');

errorXHandle = add_line(sys, 'BS1/11', 'BC3/1')
errorYHandle = add_line(sys, 'BS1/12', 'BC3/2')

targetErrorHandle = add_line(sys, 'BC3/1', 'error/1');

